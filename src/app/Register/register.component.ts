import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthenticationService, UserService } from "../_services";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { first } from "rxjs/operators";
import { User } from "../_models";

declare var $ :any;
declare var jquery:any;
@Component({
    selector:'app-register',
    templateUrl:'register.component.html',
    styleUrls:['register.component.css']
})

export class Register implements OnInit{
    RegisterForm: FormGroup;
    Otpverification: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    fullname:string;
    mobile:string;
    password:string;
    currentUser: User;
    constructor( private formBuilder: FormBuilder,private userService: UserService, private router: Router,private authenticationService: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute,){
      
    }
    ngOnInit(){
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';
        this.RegisterForm = this.formBuilder.group({
            fullname: ['', Validators.required],
            password: ['', Validators.required],
            mobile: ['', Validators.required]
        });

        this.Otpverification = this.formBuilder.group({
            mobile: ['', Validators.required],
            vcode: ['', Validators.required],
          
        });

        


        
    }
    get f() { return this.RegisterForm.controls; }




    onSubmit() {
       
         this.submitted = true;
      
        // // stop here if form is invalid
         if (this.RegisterForm.invalid) {
        //     //console.log(this.loginForm.controls);
             return;
         }

         this.loading = true;
      
        var form = new FormData();
        form.append('fullname', this.RegisterForm.value.fullname);
        form.append('mobile', this.RegisterForm.value.mobile);
        form.append('password', this.RegisterForm.value.password);

        this.userService.registeruser(form)
        .pipe(first())
        .subscribe(
           
                data => {
                    //console.log(data.status);
                    
                    if(data.result == 'true'){
                    
                        //console.log('data');
                        this.toastr.success('success');
                        localStorage.setItem('currentUser', JSON.stringify(data));
                        $('document').ready(function() {
                            $("#ex1").modal();
                          });
                       
                           
                            
                        }
                        else{
                            this.toastr.error(data.message);
                           
                             //console.log('false new')
                            
                        }
                        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
                        //console.log(this.currentUser['user']['mobile'])
                        this.Otpverification.patchValue({
           
                    mobile:this.currentUser['user']['mobile'],
                });
                });
                
              
                
        
    }






    onSubmitotp() {
        
      
         this.submitted = true;
      
        // // stop here if form is invalid
         if (this.Otpverification.invalid) {
        //     //console.log(this.loginForm.controls);
             return;
         }

         this.loading = true;
            
        var formotp = new FormData();
        formotp.append('mobile', this.Otpverification.value.mobile);
        formotp.append('vcode', this.Otpverification.value.vcode);
       
        this.userService.otpverification(formotp)
        .pipe(first())
        .subscribe(
           
                data => {
                    //console.log(data.status);
                    
                    if(data.result == 'true'){
                    
                       
                        this.toastr.success('success');

                        $('document').ready(function() {
                            $("#ex1").modal('hide');
                          });

                        //   this.currentUser = JSON.parse(localStorage.remove('currentUser'));
                          this.router.navigate([this.returnUrl]); 
                            
                        }
                        else{
                            this.toastr.error(data.message);
                           
                             //console.log('false new')
                            
                        }
                  
                });
                
        
    }

   
}