import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { User } from '../_models';
import { Router } from '@angular/router';
import { NavbarService } from '../_services/navbar.service';
declare var $: any;


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})


export class HeaderComponent implements OnInit {
  isLoggedIn:boolean;
  navbarOpen = false;
  navbarOpen1 = false;
  currentUser: User;
  notificationcount:User;
  countnew:any;
  profileimg:any;
  constructor( private authenticationService: AuthenticationService, private router: Router, public nav: NavbarService ) {    this.authenticationService.isLoggedIn.subscribe(res =>this.loggedInEmitterCatch(res));
    this.isLoggedIn=this.authenticationService.getIsLoggedIn();
    console.log(this.isLoggedIn + "user is logged in");
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));  
    this.notificationcount=JSON.parse(localStorage.getItem('notificationCount'));
    
  }

  ngOnInit() {

    
    console.log();
    this.countnew=this.notificationcount['count'];
    this.profileimg=this.currentUser['profile_image'];
   
    // $(document).ready(function(){
    //   $('ul li a').click(function(){
    //     $('li a').removeClass("active");
    //     $(this).addClass("active");
    // });
    // });
    this.nav.show();
  this.nav.doSomethingElseUseful();
   
    $(document).ready(function(){
    $('.small').click(function(){   
      $('#newop').removeClass("show");  
      $('#newop').addClass("hide");

    });
  });

  }

  
  loggedInEmitterCatch(res:string){
    if(res ==="true"){
        this.isLoggedIn = true;
    }else{
        this.isLoggedIn=false;
    }
}

Logout(){
  this.authenticationService.logout();
  localStorage.removeItem('currentUser');
  this.router.navigate(["/login"]);
}
toggleNavbar() {
  this.navbarOpen = !this.navbarOpen;
}
toggleNavbar1() {
  this.navbarOpen1 = !this.navbarOpen1;
  

}



}
