import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { Sellerknowcomponent } from "./seller-knowmore.component";





const routes: Routes = [
    { path: "know-more-seller", component: Sellerknowcomponent},
    { path: "know-more-customer", component: Sellerknowcomponent},
    
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})
export class KnowmoreRoutingModule { }