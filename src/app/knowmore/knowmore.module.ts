import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Sellerknowcomponent } from "./seller-knowmore.component";
import { KnowmoreRoutingModule } from "./knowmore-routing.module";




@NgModule({
  imports: [CommonModule, KnowmoreRoutingModule],
  declarations: [Sellerknowcomponent]
})
export class KnowmoreModule {}