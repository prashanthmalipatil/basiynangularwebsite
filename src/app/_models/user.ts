﻿export class User {
    msg:string;
    id: string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    mobile:string;
    email:string;
    status:string;
    id_city: any;
    city: string;
    nickname:string;
    result:string;
    message:string;
 
}