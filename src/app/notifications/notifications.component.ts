import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { User } from '../_models/user';
import { UserService } from '../_services/user.service';
declare var $ :any;
declare var swal:any;
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  currentUser: User;
    asnotifications: any[] = [];
  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService, ){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  }

  ngOnInit() {
    this.loadnotifications();
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });
  }


   loadnotifications() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.getnotifications(form).pipe(first()).subscribe(result => { 
        this.asnotifications = result['notifications']; 
        
        //console.log(result);
        
    });

}

//mark as read
markread(){
var invite_ids = new Array();
   
    $('.sellerdelete').each(function(){
        if ($(this).prop('checked')) {
          
            var invite_id = $(this).val(); 
            console.log(invite_ids);
            invite_ids.push(invite_id);
          
        }

    });
    if(invite_ids.length!=0){
      var form = new FormData();
      form.append('userid', this.currentUser.id);
      form.append('notification', JSON.stringify(invite_ids));
      form.append('status','3');
  
      this.userService.marknotification(form).pipe(first()).subscribe(result => { 
          
          swal('The Selected Notification/s Marked As Read ....','','success');
          this.ngOnInit();
          console.log(result);
          
      });
    }
    else{
      swal('Please select at least one notification', "", "warning");
    }
}


//redirect userr to diff page based on layout_id
redirectonclick(user){
  console.log(user);
  if(user.layout_id=='9'){
    console.log('9');

  }
  else if(user.layout_id=='5'){
    this.router.navigate(["/seller-invitation"]);
  }
  else if(user.layout_id=='1'){
    console.log('1');

  }
  else if(user.layout_id=='2'){
    console.log('2');
  }
  else if(user.layout_id=='3'){
    console.log('3');
  }
  else{
    console.log('4');
  }
}


}
