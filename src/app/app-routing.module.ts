import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Logincomponent } from './Login/login.component';
import { Sellerknowcomponent } from './knowmore/seller-knowmore.component';
import { Dashboardcomponent } from './dashboard/dashboard.component';
import { Sellerdashboard } from './dashboard/seller-dashboard.component';
import { Buyercomponent } from './dashboard/buyer-dashboard.component';
import { Userprofile } from './dashboard/user-profile.component';
import { Addseller } from './buy/My-Sellers/add-sellers/add-seller.component';
import { Addcustomer } from './sell/My-Customers/add-customers/add-customer.component';
import { Sellerinvitation } from './buy/My-Sellers/seller-invitation/seller-invitation.component';
import { Viewseller } from './buy/My-Sellers/view-my-sellers/view-seller.component';
import { Customerinvitation } from './sell/My-Customers/invitation-from-customers/customer-invitation.component';
import { Viewcustomer } from './sell/My-Customers/view-my-customers/view-customer.component';
import { Register } from './Register/register.component';
import { AuthGuard } from './_guards';
import { Creategroup } from './sell/create-group.component';
import { Invitedcustomer } from './sell/My-Customers/invited-customers/invited-customer.component';
import { Invitedsellerlist } from './buy/My-Sellers/invited-sellers/invited-seller.component';
import { ViewDetailsComponent } from './sell/Seller-Catalog/manage-catalog/view-details/view-details.component';
import { ManageCatalogComponent } from './sell/Seller-Catalog/manage-catalog/manage-catalog.component';
import { ManageMyStoreComponent } from './buy/My-Store/manage-my-store/manage-my-store.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { MyProductCategoriesComponent } from './buy/My-Store/my-product-categories/my-product-categories.component';
import { RequestSellerForCatalogComponent } from './buy/My-Store/request-seller-for-catalog/request-seller-for-catalog.component';
import { AwaitingSellerForAcceptanceComponent } from './buy/My-Store/awaiting-seller-for-acceptance/awaiting-seller-for-acceptance.component';
import { BuyerAwaitingSellerAcceptanceComponent } from './buy/My-Store/awaiting-seller-for-acceptance/buyer-awaiting-seller-acceptance/buyer-awaiting-seller-acceptance.component';
import { SharedBySellerComponent } from './buy/My-Store/add-items/shared-by-seller/shared-by-seller.component';
import { AddSingleItemComponent } from './buy/My-Store/add-items/add-single-item/add-single-item.component';
import { SellerBasiyncatalogComponent } from './sell/Seller-Catalog/From-BASIYN-Catalog/from-basiyn-catalog.component';
import { SharecatalogComponent } from './sell/Seller-Catalog/Sellershare-catalog/seller-share-catalog.component';
import { CustomerCatalogRequestComponent } from './sell/Seller-Catalog/customer-catalog-request/customer-catalog-request.component';
import { SellerSharableProductsComponent } from './sell/Seller-Catalog/seller-sharable-products/seller-sharable-products.component';
import { specificpricComponent } from './sell/Seller-Catalog/Manage-selling-price/customer-specific-pricing/customerspecific-pricing.component';
import { SellingallproductsComponent } from './sell/Seller-Catalog/Manage-selling-price/manage-selling-price-allproducts/selling-allproducts';
import { OutstandingOrdersComponent } from './buy/My-Orders/outstanding-orders/outstanding-orders.component';
import { BuyerOutstandingOrdersProductsComponent } from './buy/My-Orders/outstanding-orders/buyer-outstanding-orders-products/buyer-outstanding-orders-products.component';
import { OrderHistoryComponent } from './buy/My-Orders/order-history/order-history.component';
import { BuyerOrderSummaryProductsComponent } from './buy/My-Orders/order-history/buyer-order-summary-products/buyer-order-summary-products.component';
import { DispatchedBySellerComponent } from './buy/My-Orders/dispatched-by-seller/dispatched-by-seller.component';
import { BuyerDispatchSummaryProductsComponent } from './buy/My-Orders/dispatched-by-seller/buyer-dispatch-summary-products/buyer-dispatch-summary-products.component';
import { CancelledBySellerComponent } from './buy/My-Orders/cancelled-by-seller/cancelled-by-seller.component';
import { BuyerCancelSummaryProductsComponent } from './buy/My-Orders/cancelled-by-seller/buyer-cancel-summary-products/buyer-cancel-summary-products.component';
import { CustomerobjectionComponent } from './sell/Seller-Catalog/Manage-selling-price/seller-manage-sellingpricecustomer/seller-manage-sellingprice.component';
import { HiddenmanagesellingproductsComponent } from './sell/Seller-Catalog/Manage-selling-price/hiddensellingproducts/hiddenmanagesellingproducts.component';
import { AwaitingCustomerAcceptanceComponent } from './sell/Seller-Catalog/awaiting-customer-acceptance/awaiting-customer-acceptance.component';
import { AwCustomerProductsComponent } from './sell/Seller-Catalog/awaiting-customer-acceptance/awcustomerproducts/aw-customer-products.component';
import { AssignSellerComponent } from './sell/Seller-Catalog/assign-seller/assign-seller.component';
import { UpdateTransferAmountComponent } from './buy/Accounts/update-transfer-amount/update-transfer-amount.component';
import {  buyacntrandetailsComponent } from './buy/Accounts/account-details/account-details.component';
import { CartComponent } from './buy/My-Orders/cart/cart.component';
import { ProdVariantsComponent } from './sell/Seller-Catalog/assign-seller/prod-variants/prod-variants.component';
import { AddAndViewpointsComponent } from './sell/Points_bag/add-and-viewpoints/add-and-viewpoints.component';
import { ViewInvoiceComponent } from './sell/Points_bag/view-invoice/view-invoice.component';
import { sellupdamntComponent } from './sell/Account/update-transfer-amount/update-transfer-amount.component';
import { seltranamntComponent } from './sell/Account/seller-accountdetails/seller-accountdetails.component';
import { SellerCancelSummaryGroupComponent } from './sell/Orders/cancelled-order-history/seller-cancel-summary-group/seller-cancel-summary-group.component';
import { SellerDispatchSummaryGroupComponent } from './sell/Orders/dispatched-order-history/seller-dispatch-summary-group/seller-dispatch-summary-group.component';
import { DispatchedOrderHistoryComponent } from './sell/Orders/dispatched-order-history/dispatched-order-history.component';
import { SellerCancelSummaryProductsComponent } from './sell/Orders/cancelled-order-history/seller-cancel-summary-products/seller-cancel-summary-products.component';
import { SellerProposedOrdersGroupComponent } from './sell/Orders/proposed-orders-history/seller-proposed-orders-group/seller-proposed-orders-group.component';
import { SellerProposedOrdersProductsComponent } from './sell/Orders/proposed-orders-history/seller-proposed-orders-products/seller-proposed-orders-products.component';
import { SellerOrderSummaryGroupComponent } from './sell/Orders/received-order-history/seller-order-summary-group/seller-order-summary-group.component';
import { SellerOrderSummaryProductsComponent } from './sell/Orders/received-order-history/seller-order-summary-products/seller-order-summary-products.component';
import { SellerProposeOrderByCustomerComponent } from './sell/Orders/propose-order/seller-propose-order-by-customer/seller-propose-order-by-customer.component';
import { SellerRtDispatchComponent } from './sell/Orders/view-ready-to-dispatch/seller-rt-dispatch/seller-rt-dispatch.component';
import { SellerRtDispatchProductsComponent } from './sell/Orders/view-ready-to-dispatch/seller-rt-dispatch-products/seller-rt-dispatch-products.component';
import { SellerCancelledProductsGroupComponent } from './sell/Orders/view-ready-for-cancellation/seller-cancelled-products-group/seller-cancelled-products-group.component';
import { SellerCancelledProductsComponent } from './sell/Orders/view-ready-for-cancellation/seller-cancelled-products/seller-cancelled-products.component';
import { SellerBlockedOrdersComponent } from './sell/Orders/blocked-orders/seller-blocked-orders/seller-blocked-orders.component';
import { SellerProcessOrdersComponent } from './sell/Orders/process-order/seller-process-orders/seller-process-orders.component';
import { SellerProcessOrderedProductsComponent } from './sell/Orders/process-order/seller-process-ordered-products/seller-process-ordered-products.component';
import { SellerCancelOrdersComponent } from './sell/Orders/cancel-order/seller-cancel-orders/seller-cancel-orders.component';
import { SellerCancelOrderedProductsComponent } from './sell/Orders/cancel-order/seller-cancel-ordered-products/seller-cancel-ordered-products.component';
import { SellerBulkProcessOrderComponent } from './sell/Orders/bulk-process-order/seller-bulk-process-order/seller-bulk-process-order.component';
import { SellertransComponent } from './sell/Account/sellertrans/sellertrans.component';
import { TransdetailComponent } from './buy/Accounts/transdetail/transdetail.component';
import { SellerBuyerProductsVariantsComponent } from './sell/My-Customers/view-my-customers/seller-buyer-products-variants/seller-buyer-products-variants.component';
import { BuyerSellerProductsVariantsComponent } from './buy/My-Sellers/view-my-sellers/buyer-seller-products-variants/buyer-seller-products-variants.component';
import { BuyerPlaceOrderBySellerComponent } from './buy/My-Orders/place-order/by-seller/buyer-place-order-by-seller/buyer-place-order-by-seller.component';
import { BuyerPlaceOrderByProductNewComponent } from './buy/My-Orders/place-order/by-product/buyer-place-order-by-product-new/buyer-place-order-by-product-new.component';
import { BuyerPlaceOrderByProposedOrdersComponent } from './buy/My-Orders/place-order/proposed-order/buyer-place-order-by-proposed-orders/buyer-place-order-by-proposed-orders.component';
import { BuyerPlaceOrderByProposedOrdersDetailsComponent } from './buy/My-Orders/place-order/proposed-order/buyer-place-order-by-proposed-orders-details/buyer-place-order-by-proposed-orders-details.component';
import { BuyerBulkPlaceOrderComponent } from './buy/My-Orders/place-order/bulk-place-order/buyer-bulk-place-order/buyer-bulk-place-order.component';
import { BuyerCartDetailsComponent } from './buy/My-Orders/cart/buyer-cart-details/buyer-cart-details.component';
import { MainplaceorderPageComponent } from './buy/My-Orders/place-order/mainplaceorder-page/mainplaceorder-page.component';
import { BuyerPlaceOrderSingleProductNewComponent } from './buy/My-Orders/place-order/mainplaceorder-page/buyer-place-order-single-product-new/buyer-place-order-single-product-new.component';
import { PricingComponent } from './About-basiyn/pricing/pricing.component';
import { PrivacyPolicyComponent } from './About-basiyn/privacy-policy/privacy-policy.component';
import { TermsConditionsComponent } from './About-basiyn/terms-conditions/terms-conditions.component';
import { BuySettingCatalogComponent } from './settings-sidebar/Buyer/buy-setting-catalog/buy-setting-catalog.component';
import { BuySettingOrderComponent } from './settings-sidebar/Buyer/buy-setting-order/buy-setting-order.component';
import { BuyerVariantSettingComponent } from './settings-sidebar/Buyer/Buyer-variant-settings/buyer-variant-setting/buyer-variant-setting.component';
import { BuyerManageVariantComponent } from './settings-sidebar/Buyer/Buyer-variant-settings/buyer-manage-variant/buyer-manage-variant.component';
import { BuyVariantSettingComponent } from './settings-sidebar/Buyer/Buyer-variant-settings/buy-variant-setting/buy-variant-setting.component';
import { SellSettingCatalogComponent } from './settings-sidebar/Seller/sell-setting-catalog/sell-setting-catalog.component';
import { SellSettingOrderComponent } from './settings-sidebar/Seller/sell-setting-order/sell-setting-order.component';
import { SellerVariantSettingComponent } from './settings-sidebar/Seller/Seller-variant-settings/seller-variant-setting/seller-variant-setting.component';
import { SellerManageVariantComponent } from './settings-sidebar/Seller/Seller-variant-settings/seller-manage-variant/seller-manage-variant.component';
import { SellSettingOtherComponent } from './settings-sidebar/Seller/sell-setting-other/sell-setting-other.component';








const routes: Routes = [
  {
    path: "",
    component: Logincomponent
  },
  {
    path: 'know-more-seller',
    component:Sellerknowcomponent, canActivate: [AuthGuard]
  },
  {
    path: 'know-more-customer',
    component:Sellerknowcomponent, canActivate: [AuthGuard]
  },
  {
    path: 'dashboard',
    component:Dashboardcomponent, canActivate: [AuthGuard]
  },
  {
    path: 'seller-dashboard',
    component:Sellerdashboard, canActivate: [AuthGuard]
  },
  {
    path: 'buyer-dashboard',
    component:Buyercomponent, canActivate: [AuthGuard]
  },
  {
    path:'user-profile',
    component:Userprofile, canActivate: [AuthGuard]
  },

  {
    path:'add-seller',
    component:Addseller, canActivate: [AuthGuard]
  },
  {
    path:'add-customer',
    component:Addcustomer, canActivate: [AuthGuard]
  },
  {
    path:'seller-invitation',
    component:Sellerinvitation, canActivate: [AuthGuard]
  },
  {
    path:'view-seller',
    component:Viewseller, canActivate: [AuthGuard]
  },
  {
    path:'customer-invitation',
    component:Customerinvitation, canActivate: [AuthGuard]
  },
  {
    path:'view-customer',
    component:Viewcustomer, canActivate: [AuthGuard]
  },
  {
    path: 'register',
    component: Register
  },
  
  {
    path: 'register',
    component: Register
  },
  {
    path: 'create-group',
    component: Creategroup
  },
  {
    path: 'invited-customer',
    component: Invitedcustomer
  },
  {
    path:'invited-seller',
    component: Invitedsellerlist
  },
  {
    path:'seller-manage-catalog',
    component:ManageCatalogComponent
  },
  {
    path:'view-details',
    component:ViewDetailsComponent,
  },
  {
    path:'view-details/:id',
    component:ViewDetailsComponent,
  },
  {
    path: 'buyer-manage-catalog-variants',
    component:ManageMyStoreComponent
  },
  {
    path: 'notifications',
    component:NotificationsComponent
  },
  {
    path: 'my-product-categories',
    component:MyProductCategoriesComponent
  },

  {
    path: 'request-seller-for-catalog',
    component:RequestSellerForCatalogComponent
  },
  {
    path: 'buyer-awaiting-seller-acceptance-sellers-list',
    component:AwaitingSellerForAcceptanceComponent
  },
  {
    path: 'buyer-awaiting-seller-acceptance',
    component:BuyerAwaitingSellerAcceptanceComponent
  },
  {
    path:'buyer-awaiting-seller-acceptance/:id',
    component:BuyerAwaitingSellerAcceptanceComponent
  },
  {
    path: 'shared-by-seller',
    component:SharedBySellerComponent
  },
  {
    path: 'add-single-item',
    component:AddSingleItemComponent
  },
  {
    path:'seller-from-basiyn-catalog.php',
    component:SellerBasiyncatalogComponent
  },
  {
    path:'seller-share-catalog',
    component:SharecatalogComponent
  },
  {
    path:'seller-catalog-sharing-requests',
    component:CustomerCatalogRequestComponent
  },
  {
    path:'seller-sharable-products',
    component:SellerSharableProductsComponent
  },
  {
    path:'seller-all-customers-with-connected-products',
    component:specificpricComponent
  },
  {
    path:'seller-manage-selling-price-all-products',
    component:SellingallproductsComponent
  },
  {
    path: 'outstanding-orders',
    component :OutstandingOrdersComponent
  },
  {
    path: 'buyer-outstanding-orders-products',
    component:BuyerOutstandingOrdersProductsComponent
  },
  {
    path:'buyer-outstanding-orders-products/:id',
    component:BuyerOutstandingOrdersProductsComponent
  },
  {
    path:'buyer-order-summary-group',
    component:OrderHistoryComponent
  },
  {
    path:'buyer-order-summary-products',
    component:BuyerOrderSummaryProductsComponent
  },
  {
    path:'buyer-order-summary-products/:id',
    component:BuyerOrderSummaryProductsComponent
  },
  {
    path: 'buyer-dispatch-summary-group',
    component:DispatchedBySellerComponent
  },
  {
    path:'buyer-dispatch-summary-products',
    component:BuyerDispatchSummaryProductsComponent
  },
  {
    path:'buyer-dispatch-summary-products/:id',
    component:BuyerDispatchSummaryProductsComponent
  },
  {
    path: 'buyer-cancel-summary-group',
    component:CancelledBySellerComponent
  },
  {
    path: 'buyer-cancel-summary-products',
    component:BuyerCancelSummaryProductsComponent
  },
  {
    path:'buyer-cancel-summary-products/:id',
    component:BuyerCancelSummaryProductsComponent
  },
  {
    path:'seller-manage-selling-price',
    component:CustomerobjectionComponent
  },
  {
    path:'seller-manage-selling-price-products',
    component:HiddenmanagesellingproductsComponent
  },
  {
    path:'seller-awaiting-customer-acceptance',
    component:AwaitingCustomerAcceptanceComponent
  },
  {
    path:'seller-awaiting-customer-acceptance-products',
    component: AwCustomerProductsComponent
  },
  {
    path:'assign-seller',
    component:AssignSellerComponent
  },

  {
    path:'mini-account-buyer-customer-select',
    component:UpdateTransferAmountComponent
  },
  {
    path:'mini-account-buyer-customer-details',
    component:buyacntrandetailsComponent
  },
{
  path:'buyer-cart',
  component:CartComponent
},
{
  path:'seller-dispatch-summary-group',
  component:DispatchedOrderHistoryComponent
},
{
  path:'seller-dispatch-summary-products',
  component:SellerDispatchSummaryGroupComponent

},
{
  path:'seller-dispatch-summary-products/:id',
  component:SellerDispatchSummaryGroupComponent
},
{
  path:'seller-cancel-summary-group',
  component:SellerCancelSummaryGroupComponent

},
{
  path:'assign-seller-product-variant',
  component:ProdVariantsComponent
},
{
  path:'wallet',
  component:AddAndViewpointsComponent
},
{
  path:'payment',
  component:ViewInvoiceComponent
},
{
  path:'mini-account-seller-customer-select',
  component:sellupdamntComponent
},
{
  path:'mini-account-seller-customer-details',
  component:seltranamntComponent
},
{
  path:'seller-cancel-summary-products',
  component:SellerCancelSummaryProductsComponent

},
{
  path:'seller-cancel-summary-products/:id',
  component:SellerCancelSummaryProductsComponent
},
{
  path:'seller-proposed-orders-group',
  component:SellerProposedOrdersGroupComponent
},
{
  path:'seller-proposed-orders-products',
  component:SellerProposedOrdersProductsComponent
},
{
  path:'seller-proposed-orders-products/:id',
  component:SellerProposedOrdersProductsComponent
},
{
  path:'seller-order-summary-group',
  component:SellerOrderSummaryGroupComponent
},
{
  path:'seller-order-summary-products',
  component:SellerOrderSummaryProductsComponent
},
{
  path:'seller-order-summary-products/:id',
  component:SellerOrderSummaryProductsComponent
},
{
  path:'seller-propose-order-by-customer',
  component:SellerProposeOrderByCustomerComponent
},
{
  path:'seller-rt-dispatch',
  component:SellerRtDispatchComponent
},
{
  path:'seller-rt-dispatch-products',
  component:SellerRtDispatchProductsComponent
},
{
  path:'seller-rt-dispatch-products/:id',
  component:SellerRtDispatchProductsComponent
},
{
  path:'seller-cancelled-products-group',
  component:SellerCancelledProductsGroupComponent
},
{
  path:'seller-cancelled-products',
  component:SellerCancelledProductsComponent
},
{
  path:'seller-blocked-orders',
  component:SellerBlockedOrdersComponent
},
{
  path:'seller-process-orders',
  component:SellerProcessOrdersComponent
},
{
  path:'seller-process-ordered-products',
  component:SellerProcessOrderedProductsComponent
},
{
  path:'seller-cancel-orders',
  component:SellerCancelOrdersComponent
},
{
path:'seller-cancel-ordered-products',
component:SellerCancelOrderedProductsComponent
},
{
  path:'seller-bulk-process-order',
  component:SellerBulkProcessOrderComponent
  },
  {
    path: 'mini-account-seller-transaction-details',
    component: SellertransComponent
  },
  {
    path:'mini-account-buyer-transaction-details',
    component:TransdetailComponent,
  },
  {
    path:'seller-buyer-products-variants',
    component:SellerBuyerProductsVariantsComponent,
  },
  {
    path:'seller-buyer-products-variants/:id',
    component:SellerBuyerProductsVariantsComponent
  },
  {
    path:'buyer-seller-products-variants',
    component:BuyerSellerProductsVariantsComponent
  },
  {
    path:'buyer-place-order-by-seller',
    component:BuyerPlaceOrderBySellerComponent
  },
  {
    path:'buyer-place-order-by-product-new',
    component:BuyerPlaceOrderByProductNewComponent
  },
  {
    path:'buyer-place-order-by-proposed-orders',
    component:BuyerPlaceOrderByProposedOrdersComponent
  },
  {
    path:'buyer-place-order-by-proposed-orders-details',
    component:BuyerPlaceOrderByProposedOrdersDetailsComponent
  },
  {
    path:'buyer-bulk-place-order',
    component:BuyerBulkPlaceOrderComponent
  },
  {
    path:'buyer-cart-details',
    component:BuyerCartDetailsComponent
  },
  {
    path:'buyer-cart-details/:id',
    component:BuyerCartDetailsComponent
  },
  {
    path:'buyer-cart-details/:ids',
    component:BuyerCartDetailsComponent
  },
  {
    path:'buyer-place-order-new',
    component:MainplaceorderPageComponent
  },
  {
    path:'buyer-place-order-single-product-new',
    component:BuyerPlaceOrderSingleProductNewComponent
  },
  {
    path:'buyer-place-order-single-product-new/:id',
    component:BuyerPlaceOrderSingleProductNewComponent
  },
  {
    path:'pricing',
    component:PricingComponent
  },
  {
    path:'privacy-policy',
    component:PrivacyPolicyComponent
  },
  {
    path:'terms-conditions',
    component:TermsConditionsComponent
  },
  {
    path:'buy-setting-catalog',
    component:BuySettingCatalogComponent
  },
  {
    path:'buy-setting-order',
    component:BuySettingOrderComponent
  },
  {
    path:'buyer-variant-setting',
    component:BuyerVariantSettingComponent
  },
  {
    path:'buyer-manage-variant',
    component:BuyerManageVariantComponent
  },
  {
    path:'buy-variant-setting',
    component:BuyVariantSettingComponent
  },
  {
    path:'sell-setting-catalog',
    component:SellSettingCatalogComponent
  },
  {
    path:'sell-setting-order',
    component:SellSettingOrderComponent
  },
  {
    path:'seller-variant-setting',
    component:SellerVariantSettingComponent
  },
  {
    path:'seller-manage-variant',
    component:SellerManageVariantComponent
  },
  {
    path:'sell-setting-other',
    component:SellSettingOtherComponent
  },
  // {
  //   path: "Login",
  //   loadChildren: "../app/Login/login.module#LoginModule"
  // },
  // {
  //   path: "kinterg",
  //   loadChildren: "../app/king/king.module#KingModule"
  // }

  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}

