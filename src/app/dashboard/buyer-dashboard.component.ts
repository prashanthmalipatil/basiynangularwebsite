import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
    templateUrl:'buyer-dashboard.component.html',
    styleUrls:['buyer-dashboard.component.css']
})

export class Buyercomponent implements OnInit{
    options: FormGroup;

    constructor(fb: FormBuilder){this.options = fb.group({
        bottom: 0,
        fixed: false,
        top: 0
      });}

    ngOnInit(){

    }
    
}