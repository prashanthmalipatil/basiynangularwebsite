import { Component, OnInit } from "@angular/core";
import { NavbarService } from "../_services/navbar.service";
import { UserService } from "../_services";
import { User } from "../_models";
import { first } from "rxjs/operators";

@Component({
    templateUrl:'dashboard.component.html',
    styleUrls:['dashboard.component.css']
})

export class Dashboardcomponent implements OnInit{
    currentUser: User;
    noticount:any[]=[];
    constructor(public nav: NavbarService, private userService: UserService,){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    
    ngOnInit(){
        this.nav.show();
        this.notificationcount();
    }
    notificationcount(){
        var form = new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.notificationscount(form).pipe(first()).subscribe(result => { 
        //this.noticount = result['count']; 
        localStorage.setItem('notificationCount', JSON.stringify(result));
        
       // console.log(result);
        
    });
    }
}