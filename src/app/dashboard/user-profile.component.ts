import { Component, OnInit } from "@angular/core";
import { AbstractClassPart } from "@angular/compiler/src/output/output_ast";
import { User } from "../_models";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthenticationService, UserService } from "../_services";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { first } from "rxjs/operators";
import { HttpClient, HttpHeaders } from '@angular/common/http';

declare var $: any;
declare var swal:any; 
@Component({
    templateUrl:'user-profile.component.html',
    styleUrls:['user-profile.component.css']
})

export class Userprofile implements OnInit{
    currentUser: User;
    profileForm: FormGroup;
    Resetpass:FormGroup;
    Updatemobile:FormGroup;
    loading = false;
    submitted = false;
    states = new Array();
    cities= new Array();
    myForm: any;
   // userdetails=new Array();
    returnUrl: string;
    status:boolean;
    message:any;
    userid: any;
    user_details:any={};
    resetpassword: FormGroup;
   smsstatus1:any[]=[];
    constructor(private http: HttpClient,private formBuilder: FormBuilder, private userService: UserService,  private router: Router,private authenticationService: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute,){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
       
        
    }

    
    ngOnInit(){
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/user-profile';
        this.loadstates();
        this.userinfo();
        //this.navigate();
        // $("#myModal").hide();
        this.profileForm = this.formBuilder.group({
            fullname: ['' ],
            mobile: [''],
            email: [''],
            password:[''],
            address_line_1: [''],
            address_line_2: [''],
            about_me:[''],
            id_state: [''],
            id_city: [''],
            pincode: [''],
            id:[''],
            city:[''],
            device_gcm:[''],
            gps_location:[''],
            is_in_basiyn:[''],
            state:[''],
          
        });
        this.Resetpass= this.formBuilder.group({
            resetpassword:['', Validators.required],
            confirmpassword:['', Validators.required]
        })
        this.Updatemobile= this.formBuilder.group({
            updatemobile:['', Validators.required],
            confirmmobile:['', Validators.required]
        })
       
        
        this.profileForm.patchValue({
            id_state:this.currentUser['id_state'],
            id_city:this.currentUser['id_city'],
            id:this.currentUser['id'],
            fullname : this.currentUser['fullname'],
            mobile : this.currentUser['mobile'],
            email:this.currentUser['email' ],
            address_line_1:this.currentUser['address_line_1'],  
            pincode:this.currentUser['pincode'],
            city:this.currentUser['city'],
            state:this.currentUser['state'],
            is_in_basiyn:this.currentUser['is_in_basiyn'],
            gps_location:this.currentUser['gps_location'],
            device_gcm:this.currentUser['device_gcm'],
            address_line_2:this.currentUser['address_line_2'],
        });
        
        
    }
   
    get g() { return this.Resetpass.controls; }
    get k() { return this.Updatemobile.controls;}
   

    onSubmitresetpass() {

        var newpasswd = $("#newpassword1").val();
         var cnfrpasswd = $("#confirmpassword").val();
        
         if(newpasswd!='' && cnfrpasswd!=''){
             
             if(newpasswd!=cnfrpasswd){
               alert(" confirm password is not matching "); 
             }
         }else{
            alert( " Please Enter password ");
         }
        // console.log("came to submit");
        // this.submitted = true;
         
        // // // stop here if form is invalid
        //  if (this.Resetpass.invalid) {
            
        //      console.log('false data');
        //      return;
        //  }
         
        //  this.loading = true;
          
           var form = new FormData();
          
           form.append('userid', this.currentUser.id);
           form.append('password', this.Resetpass.value.resetpassword);
            console.log(form);
           this.userService.resetpasswords(form)
           .pipe(first())
           .subscribe(
            data => {
               
                    console.log(data['msg']);
                    if(data['status']=='true'){
                        swal(data['msg'], '', 'success'); 
                    }
                    else{
                  
                        swal(data['msg'], '', 'success');
                    }
                  });

   
      }

      onSubmitupdatenumber() {
         var newmobile = $("#updatenum").val();
         var cnfrnumber = $("#confirmnumber").val();
        //  var msg = '';
         if(newmobile!='' && cnfrnumber!=''){
             
             if(newmobile!=cnfrnumber){
                alert( " confirm number not matching "); 
             }
         }else{
           alert(" Please Enter mobile number ");
         }

        //  if(msg==''){
            var form = new FormData();
            form.append('userid', this.currentUser.id);
            form.append('mobile', newmobile);
            this.userService.updateusermobilenumber(form)
            .pipe(first())
            .subscribe(
             data => {
                console.log(data['message']);
                if(data['status']=='true'){
                    alert("hi");
                }
                // else{
                //     alert(data['message']);
                // }
                   });
        //  }else{
        //      alert(msg);
        //  }
    
      }

    private loadstates() {
    
        this.userService.getstates().pipe(first()).subscribe(result => { 
             //this.states = result; 
             if(result.length>0){
                
                     this.states=result;
                     console.log(this.states);
                 
             }
             
        });
       // this.navigate();
        

      }



      handleSelectedValue(statesvalue) {
      
          console.log(statesvalue);
          this.cities=[];
          this.currentUser.id_city=""
          this.currentUser.city=""
          this.userService.getcity(statesvalue).pipe(first()).subscribe(result => {
              this.cities= result;
        });
        //this.navigate();
      }

      get f() { return this.profileForm.controls; }

      //get user details
      userinfo(){
      this.userService.getuserinfo(this.currentUser.id).pipe(first()).subscribe(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
     
        
        this.navigate();
        });
        //this.navigate();

}
navigate(){
    //console.log("came to nav")
   this.router.navigate([this.returnUrl]);
   if (window.location.href.indexOf('reload')==-1) {
    window.location.replace(window.location.href+'?reload');
   // location.reload(true);
    
}

}

  
    onSubmit() {
        // this.router.navigate(['/dashboard']);
        // console.log(this.loginForm.value);
        this.submitted = true;
      
        // stop here if form is invalid
       

        this.loading = true;
        console.log(this.profileForm.value);
        this.userid= this.currentUser.id
        this.user_details= JSON.stringify(this.profileForm.value)

        var form = new FormData();
        form.append('userid', this.userid);
        form.append('user_details' ,this.user_details);
      
        
        this.userService.updateuser(form)
            .pipe(first())
            .subscribe(
               
                    data => {
                        //console.log(data.status);
                        
                        if(data.status == 'true'){
                        
                            console.log('data');
                            this.toastr.success('success');
                            this.userinfo();
                             this.navigate();
                               
                                
                            }
                            else{
                                this.toastr.error(data.message);
                                //console.log(data.status);
                                //  this.toastr.success(status);
                                //  this.navigate();
                                //  this.userinfo();
                                //  console.log(status);
                                 console.log('false new')
                                 //this.toastr.error(data.message);
                            }
                      
                    });
                    
                   
                
        
    }
    openupdatemobile(){
        $("#ex1").show();
    }
}
