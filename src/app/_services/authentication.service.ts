﻿import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { map } from 'rxjs/operators';
import { User } from '../_models';


@Injectable()
export class AuthenticationService {
 
    @Output() isLoggedIn: EventEmitter<string> = new EventEmitter();
    loggedIn = false;
    constructor(private http: HttpClient,  ) { }

    login(email: string, password: string) {
        var form = new FormData();
        form.append('email', email);
        form.append('password' ,password);
        return this.http.post<any>('https://dev.basiyn.com/user-login.php',form)
            .map(user => {
                // login successful if there's a jwt token in the response
                // if (user && user.token) {
                //     console.log('cane to session storage of authentication');
                //     // store user details and jwt token in local storage to keep user logged in between page refreshes
                //     localStorage.setItem('currentUser', JSON.stringify(user));
                // }
                //console.log(user.status)
                
                    if(user.status == 'true'){
                      
                       console.log(user.status +"true")
                       localStorage.setItem('currentUser', JSON.stringify(user));
                        
                       this.isLoggedIn.emit("true");
                        return user;
                        
                    }
                    else{
                       // console.log('false');
                        //sconsole.log(status);
                        console.log(user.status +'false')
                        return user;
                    }
                   
               
              
            });
            
            
    }
    getIsLoggedIn(){
        if(localStorage.getItem('currentUser')){
            console.log("User is logged in")
            return true;
        }else{
            return false;
        }

    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.isLoggedIn.emit("false");
        console.log('user logged out');
    }
}