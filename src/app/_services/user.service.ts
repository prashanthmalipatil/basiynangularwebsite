﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models/index';
import { map } from 'rxjs-compat/operator/map';

@Injectable()
export class UserService {
    [x: string]: any;
  
    getsellinvitedcustomers(form: FormData): any {
        throw new Error("Method not implemented.");
    }
    getassellerinvited(): any {
        throw new Error("Method not implemented.");
    }
    
    
    constructor(private http: HttpClient) { }
    baseurl='https://dev.basiyn.com/';
   
    updateuser(form){
        return this.http.post<any>(this.baseurl+'update-user.php',form)
        .map(user => {
           
            
                if(user.status == 'true'){
                  
                    console.log('Success');
                    console.log(user.status);
                    return user;
                    
                }
                else{
                    console.log('failure');
                    return user;
                }
               
           
          
        });
        // return this.http.post<User[]>(this.baseurl+'update-user.php', form);
    }
    
    //get list of states
    getstates(){
        
        return this.http.get<User[]>(this.baseurl+'states.php');
        

        
    }
    // get city bassed on id of state profile page
    getcity(selectedstate){
        return this.http.get<User[]>(this.baseurl+'cities.php?state_id=' + selectedstate);
    }

    //get user info profile page
    getuserinfo(id){
        return this.http.get<User[]>(this.baseurl+'user.php?id=' + id);
    }
    


    registeruser(form){
        console.log(form)
        return this.http.post<any>(this.baseurl+'user-registration.php',form)
        .map(user => {
           
            
                if(user.status == 'true'){
                  
                    console.log('Success');
                    // console.log(user.status);
                    return user;
                    
                }
                else{
                    console.log('failure');
                    return user;
                }
               
           
          
        });

    }

        //registeration 
    otpverification(formotp){
       // console.log(formotp)
        return this.http.post<any>(this.baseurl+'verify-mobile.php',formotp)
        .map(user => {
           
            
                if(user.status == 'true'){
                  
                    console.log('Success');
                    // console.log(user.status);
                    return user;
                    
                }
                else{
                    console.log('failure');
                    return user;
                }
               
           
          
        });

    }

// add seller buy side
    addseller(form){
        // console.log(formotp)
         return this.http.post<any>(this.baseurl+'invite-seller.php',form)
         .map(user => {
            
             
            if(user.status == 'SMS Sent'){
                   
                     console.log('new user sms sent')
                     // console.log(user.status);
                     return user;
                     
                 }
                 else if(user.status=='Invited'){
                    console.log('already invited')
                   return user;
                 }
                 else if(user.status=='failed'){
                     console.log('cant invite yourself')
                   return user;
                 }
                 else{
                     console.log('failure');
                     return user;
                 }
                
            
           
         });
 
     }

     // add customer sell side
     addcustomer(buyer_invites){
        console.log(buyer_invites)
         return this.http.post<any>(this.baseurl+'add-buyer.php',buyer_invites)
         .map(user => {
            
             console.log(user[0].result.message);

            
            if(user[0].result.status == 'Not Sent'){
                   
                     
                     return user;
                     
                 }
                 else if(user[0].result.status=='Invited'){
                   
                   return user;
                 }
               
                
            
           
         });
 
     }
     //get Invited Seller(buy side)
    
    getsellerinvited(form){
        return this.http.post<User[]>(this.baseurl+'seller-invitations.php', form);
    }

    //get Invited customer(sell side)
    getcustomerinvited(form){
        return this.http.post<User[]>(this.baseurl+'buyer-invitations.php', form);
    }

    //get view customers(sell side)
    getviewcustomers(form){

        return this.http.post<User[]>(this.baseurl+'view-buyers.php?page=0&new=true', form)
    }

    //get view seller (buy side)
    getviewsellers(form){
        return this.http.post<User[]>(this.baseurl+'view-sellers.php?page=0', form);
    }



    //Bulk upload add-seller side 
    bulkupload(form){
        console.log('form')
        return this.http.post<User[]>(this.baseurl+'csvreader.php', form)
        
    }

    //Search Seller
    searchseller(form, seller_name){
        return this.http.post<User[]>(this.baseurl+'view-sellers.php?page='+0+ '&cname='+seller_name ,form)
        .map(user => {
            
            

           
           if(status == 'true'){
                  
                    
                    return user;
                    
                }
                else {
                  
                  return user;
                }
              
               
           
          
        });
    }   

    //Search Customer
    searchcustomer(form, customer_name){
        return this.http.post<User[]>(this.baseurl+'view-buyers.php?page='+0+ '&cname='+customer_name+'&new=true' ,form);
    }   


    //get invited customers (sell side)
    getsellinvitedcustomerslist(form){
        return this.http.post<User[]>(this.baseurl+'invited-customer-list.php', form);
    }

    //get invited sellers(buy side)
    getbuyinvitedsellerslist(form){
        return this.http.post<User[]>(this.baseurl+'invited-sellers-list.php', form);
    }

    //forgot pass on entering mobile no
    forgotmobile(form){
        return this.http.post<User[]>(this.baseurl+'forget-password.php', form);
    }

    //otp submit
    Otpsubmit(form){
        return this.http.post<User[]>(this.baseurl+'verify-otp.php', form);
    }

    //reset pass on new pass entered
    resetpass(form){
        return this.http.post<User[]>(this.baseurl+'reset-password.php', form);
    }

    //Seller single delete 
    sellerdelete(form){
        return this.http.post<User[]>(this.baseurl+'delete-customers.php', form);

    }

    //reject seller invit
    rejectnew(form){
        return this.http.post<User[]>(this.baseurl+'update-seller-invitation-status.php', form);
    }

    //load products in manage catalog
    loadproducts(form,page){
        return this.http.post<User[]>(this.baseurl+'sell_manage_catelog.php?catelog_fetch=1&page='+page, form);
    }

    //update sell and buy price
    updateprice(form){
        return this.http.post<User[]>(this.baseurl+'basiyn_n/sell_manage_catelog.php?catelog_price_update=2', form);
    }
    
    //delete Product
    deleteproduct(form){
        //return this.http.post<User[]>(' http://dev.basiyn.com/basiyn_n/sell_manage_catelog.php?catelog_delete', form)
        return this.http.post<User[]>(this.baseurl+'sell_manage_catelog.php?catelog_delete', form)
    }


    //load buyer side manage store products
    loadbuyerproducts(form, page){

        return this.http.post<User[]>(this.baseurl+'buyer-my-products.php?page='+page, form);
    }

    //load data in details page
    loaddetails(form){
        return this.http.post<User[]>(this.baseurl+'sell_manage_catelog_details.php?catelog_detailes=1', form);
    }
    //delete product in manage my store(BUY Side)
    deletebuyproduct(form){
        return this.http.post<User[]>(this.baseurl+'buyer-delete-products.php', form)
        
    }

    //Notifications
    getnotifications(form){
        return this.http.post<User[]>(this.baseurl+'notifications.php', form);
    }

    notificationscount(form){
        return this.http.post<User[]>(this.baseurl+'notification-count.php', form);
    }

    marknotification(form){
        return this.http.post<User[]>(this.baseurl+'update-selected-notification.php', form);
    }
    //Request for seller catalog(Buy Side)
    getreqselforcat(form){
        return this.http.post<User[]>(this.baseurl+'buyer-sellers-list-share-catalog-request.php', form);
    }

    //Awaiting Seller for acceptance
    getawaitingsellerforaccept(form){
        return this.http.post<User[]>(this.baseurl+'buyer-shared-products-sellers-list.php', form);
    }

    //share by seller page api
    sharedbyseller(form){
        return this.http.post<User[]>(this.baseurl+'sellers-with-shared-products.php?value=sellerList', form);
    }
    //Buyer Awaiting seller for accptance(Buy side)
    getbuyerawaitingsellerforaccept(form){
        return this.http.post<User[]>(this.baseurl+'buyer-shared-products-by-seller.php?page=0', form);
    }
    //Outstanding Orders(Buy side)

    getoustanding(form){
        return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=outstandingordersList&page=0', form);
    }

    getbuyeroustanding(form){
        return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=outstandingordersdetail', form);
    }

    //order history(Buy side/my-orders)
    getorderhistory(form){
        return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=orders&page=0', form); 
    }
    //  order-history:Buyer-order-summary-products(Buy side)
    getbuyerordersummary(form){
        return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=orderdetail&page=0', form); 
    }

    //Dispatch by seller(buy side)
    getdispatch(form){
        return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=dispachList&page=0', form); 
    }
    
    //dispatch by seller->buyer-dispatch-summary-products(buy side)

    getbuyedispatchsummaryprod(form){
        return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=dispachDetails&page=0', form); 
    }

    //cancelled-by-seller(buy-side)

    getcancelled(form){
        return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=CancelBySeller&page=0', form); 
    }
    //buyer-cancel-summary-products(buy-side)
    getbuyercancelledsummary(form){
        return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=Canceldetail&page=0', form); 
    }

    

    //buyer awating acceptance user delete
    buyerawwatingacceptance(form){
        return this.http.post<User[]>(this.baseurl+'buyer-shared-products-by-seller.php?action=delete', form);
    }

    frombasiynsellside(form,page){
        return this.http.post<User[]>(this.baseurl+'basiyn-products.php?value=products&page='+page,form)
    }
    sharecatalog(form){
        return this.http.post<User[]>(this.baseurl+'seller-buyers-for-share-catalog.php?page=0',form)
    }

    //share all products in share catalog
    shareallproducts(form){
        return this.http.post<User[]>(this.baseurl+'products.php?request=productsshare',form);
    }


    //customer catalog requests
    sharecatalogrequest(form){
        return this.http.post<User[]>(this.baseurl+'view-buyers-requested-catalog.php?page=0',form);
    }

    shareallcustproducts(form){
        return this.http.post<User[]>(this.baseurl+'products.php?request=productsshare',form);
    }

    //sharable products invisible page
    shareableproducts(form){
        return this.http.post<User[]>(this.baseurl+'products.php?request=shareProducts', form);
    }

    //customer specific pricing in manage selling price
    specificpricinig(form){
        return this.http.post<User[]>(this.baseurl+'seller-buyers-with-connected-products.php',form);
    }

    //load buyer products
    buyershareprodu(form, page){
        return this.http.post<User[]>(this.baseurl+'seller-buyer-all-products.php?new=true&page='+ page,form);
    }


    //revise selling price in seller manage selling price
    revsellingprice(form){
        return this.http.post<User[]>(this.baseurl+'seller-manage-selling-price.php',form);
    }

    //customer objection in price
    custobjection(form){
        return this.http.post<User[]>(this.baseurl+'seller-cost-difference-customers-list.php',form)
    }
    
    //sellingprice diff
    sellingpricobj(form){
        return this.http.post<User[]>(this.baseurl+'seller-cost-difference-products.php?new',form);
    }

    //load awaiting customer acceptance
    loadcustacceptance(form){
        return this.http.post<User[]>(this.baseurl+'seller-shared-products-customers-list.php',form);
    }

    //awaiting customer products
    awaitingcustproducts(form){
        return this.http.post<User[]>(this.baseurl+'seller-shared-products-by-customer.php', form);
    }

    //search awaiting products
    searchawating(form, pname){
        return this.http.post<User[]>(this.baseurl+'seller-shared-products-by-customer.php?page=0&pname='+pname,form);
    }
    deletecustawtproducts(form){
        return this.http.post<User[]>(this.baseurl+'delete-seller-shared-products.php',form);
    }

    //search products in seller-manage seelling price all products page
    seachprod(form){
        return this.http.post<User[]>(this.baseurl+'seller-buyer-all-products.php?new=true',form);
    }

    //
    confsellingprice(form){
        return this.http.post<User[]>(this.baseurl+'seller-manage-selling-price.php',form);
    }

    //search in seller manage selling price all prod
    seachprodcust(form){
        return this.http.post<User[]>(this.baseurl+'seller-cost-difference-products.php?new',form);
    }
    //search for order-history(buy side/my-orders)
    searchsellernames(form, seller_name){
        return this.http.post<User[]>(this.baseurl+'buyer-order.php?page='+0+'&request=orders&sname='+seller_name ,form);
       
    }   

    //search for dispatch order-history(buy side/my-orders)
    searchdispatchsellernames(form, seller_name){
        return this.http.post<User[]>(this.baseurl+'buyer-order.php?page='+0+'&request=dispachList&sname='+seller_name ,form);
       
    } 
 //search for cancelled order-history(buy side/my-orders)
 searchcancelledsellernames(form, seller_name){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?page='+0+'&request=CancelBySeller&sname='+seller_name ,form);
   
} 

//filter for dates order-history(buy side/my-orders)
searchsellernamesbydates(form,from,to ){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?page='+0+'&request=orders&from='+from+'&to='+to ,form);
   
} 

    //load assign supplier
    loadasssupplier(form){
        return this.http.post<User[]>(this.baseurl+'view-sellers.php?page=0',form);
    }
    //prod variants
    prodvariants(form, seller, status){
        return this.http.post<User[]>(this.baseurl+'seller-product-variant.php?page=0&seller='+seller+'&status='+status, form)
    }

    //add and view points
    getpoints(form){
        return this.http.post<User[]>(this.baseurl+'view-transactions.php', form);
    }
    //check available points
    availpoints(form){
        return this.http.post<User[]>(this.baseurl+'available-points.php',form)
    }

    //calculate gst and total amnt
    amntcalc(){
        return this.http.post<User[]>(this.baseurl+'rupee-to-bp.php',45);
    }
    payu(form){
        return this.http.post<User[]>('https://sandboxsecure.payu.in/_payment', form)
    }

    loadinvoices(form){
        return this.http.post<User[]>(this.baseurl+'order-seller.php?request=paymentList', form)
    }

    filterinvoice(form){
        return this.http.post<User[]>(this.baseurl+'order-seller.php?page=0&request=paymentList', form);
    }
    loadcustomer(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/buyer-mini-account-customer-select.php', form);
    }

    updatepriceselleraccount(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-seller-transaction.php', form)
    }

    loadselleraccountdetails(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-seller-customer-details.php', form)
    }
//filter for dates dispatch by sellers(buy side/my-orders)
searchdispatchsellernamesbydates(form,from,to ){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?page='+0+'&request=dispachList&from='+from+'&to='+to ,form);
   
} 

//filter for dates cancelled by seller(buy side/my orders)
searchcancelledsellernamesbydates(form,from,to ){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?page='+0+'&request=CancelBySeller&from='+from+'&to='+to ,form);
   
} 

//dispatch order history(sell side)
getselldispatchhistory(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=DispatchHistroy&page=0', form); 
}
//dispatch order history -detail page(sell side)
getselldispatchsummary(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=dispachDetails&page=0', form); 
}

//search for dispatched order-history(sell side/orders)
searchselldispatchcustomernames(form,seller_name){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?page='+0+'request=DispatchHistroy&sname='+seller_name ,form);
   
} 
//filter for dates dispatch order history(sell side/ orders)
searchselldispatchsellernamesbydates(form,from,to ){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?page='+0+'&request=DispatchHistroy&from='+from+'&to='+to ,form);
   
} 

//cancelled order history -orders(sell side)
getsellcancelledhistory(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=cancelHistory&page=0', form); 
}

//cancelled order detail history -orders(sell side)
getsellcancelleddetailhistory(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=cancelHistoryDetail&page=0', form); 
}

//filter for dates cancel order history(sell side/ orders)
searchsellcancelsellernamesbydates(form,from,to ){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?page='+0+'&request=cancelHistory&from='+from+'&to='+to ,form);
   
} 
//proposed order history(sell side-orders)
getsellproposedhistory(form){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?request=proposeorder-list&page=0', form); 
}

//proposed order detail history(sell side-orders)
getsellproposeddetailhistory(form){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?request=proposeorder-detail&page=0', form); 
}

//filter for dates dispatch order history(sell side/ orders)
searchsellproposedsellernamesbydates(form,from,to ){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=proposeorder-list&from='+from+'&to='+to ,form);
   
} 
//search for proposed order-history(sell side/orders)
searchproposedsellernames(form, seller_name){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=proposeorder-list&sname='+seller_name ,form);
   
}

//search for dispatch order-history(sell side/orders)
searchselldispatchnames(form, seller_name){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=DispatchHistroy&sname='+seller_name ,form);
   
}  

//search for cancelled order-history(sell side/orders)
searchsellcancellednames(form, seller_name){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=cancelHistory&sname='+seller_name ,form);
   
}  
//received order  history(sell side-orders)
getsellreceivedhistory(form){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?request=recivedorder&page=0', form); 
}
//received order detail history(sell side)
getsellreceiveddetailhistory(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=recivedorderdetails&page=0', form); 
}
//filter for dates received order history(sell side/ orders)
searchsellreceivedsellernamesbydates(form,from,to ){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=recivedorder&from='+from+'&to='+to ,form);
   
} 
//search for received order-history(sell side/orders)
searchreceivedsellernames(form, seller_name){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=recivedorder&sname='+seller_name ,form);
   
}
//proposed connected customers(sell side-orders)
getsellproposedconnected(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=proposeorder-customer&page=0', form); 
}  

//proposed non connected customers(sell side-orders)
getsellproposednonconnected(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=proposeorder-noncustomer&page=0', form); 
}  
//search for proposed connected customer(sell side/orders)
searchproposedconnectednames(form, seller_name){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=proposeorder-customer&sname='+seller_name ,form);
   
}
//View RTD-ready to dispatch(sell side-orders)
getsellviewrtd(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=rtdlist', form); 
}  
//View RTD detail page-ready to dispatch(sell side-orders)
getsellviewrtddetail(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=rtd_detail', form); 
}  
//View RTC-ready for cancellation(sell side-orders)
getsellviewrtc(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=rfclist', form); 
}  

//View RTD detail page-ready to dispatch(sell side-orders)
getsellviewrtcdetail(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=rfcdetail', form); 
} 

    //close objection
    closeobj(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/close-transaction-objection.php', form)
    }
        //edit amount 

        editamnt(form){
            return this.http.post<User[]>(this.baseurl+'mini-account/update-transaction-amount-buyer.php', form);
        }

    //search customer in seller view transdetails
    searchcust(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-seller-customer-details.php', form);
    }

//Blocked Orders(sell side-orders)
getblockedorders(form){
    return this.http.post<User[]>(this.baseurl+'seller-blocked-orders.php', form); 
}  
//Process Orders(sell side-orders)
getprocessorders(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=processOrder&page=0', form); 
}  
//Process Orders detail page(sell side-orders)
getprocessordersdetail(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=processodersdetail', form); 
} 
//Cancel Orders(sell side-orders)
getcancelledorders(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=processOrder&page=0', form); 
}   
//Cancel Orders detail page(sell side-orders)
getcancelsordersdetail(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=processodersdetail', form); 
} 

    //search seller in customer vieww transactions
    searchsell(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-buyer-customer-details.php', form);
    }

    //filter by dates in seller trans details page
    sellertransdet(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-seller-transaction-details.php?', form);
    }


    //export filtered data in seller side vide trans details
    exportfilterdata(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-seller-customer-details-export.php', form);
    }


    //customerr trans details filter date
    customertransdet(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-buyer-transaction-details.php?', form);
    }

    //export filtered data in seller side vide trans details
    exportfilterdatabuyside(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-buyer-customer-details-export.php', form);
    }


    //bulk process order download products
    exportorder(form){
        return this.http.post<User[]>(this.baseurl+'seller-outstanding-orders-export.php', form);
    }

    bulkuploadnew(form){
        return this.http.post<User[]>(this.baseurl+'csvreaderbulk.php', form);
    }

    loadtranssell(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-seller-transaction-details.php?', form);
    }
    
    //load data in chatbox
    chatbox(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/seller-objection-comments.php', form);
    }
    
    //submit txt when useer send msg
    userchatsub(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/seller-objection-details.php', form);
    }
    loadcustaccountdetails(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-buyer-customer-details.php', form);
    }
      
    loadbuysidedetails(form){
        return this.http.post<User[]>(this.baseurl+'mini-account/buyer-mini-account-customer-select.php', form);
    }
    
    //buyer side update amount
updatepricebuyeraccount(form){
    return this.http.post<User[]>(this.baseurl+'mini-account/mini-account-buyer-transaction.php', form)
}

//buyer side trans details
loadtransbuy(form, page){
    return this.http.post<User>(this.baseurl+'mini-account/mini-account-buyer-transaction-details.php?page='+page, form)
}

//upload csv data in bulk upload
uploadconcsvdata(form){
    return this.http.post<User[]>(this.baseurl+'seller-mark-rtd-bulk.php', form)
}

           
    //search for proposed connected customer(sell side/orders)
    searchsellproposedconnectednames(form, seller_name){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=proposeorder-customer&sname='+seller_name ,form);
   
}
//search for proposed non-connected customer(sell side/orders)
searchsellproposednonconnectednames(form, seller_names){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=proposeorder-noncustomer&sname='+seller_names ,form);
   
}
 //search for buyer oustanding order detail page(buy side/orders)
 searchoutstandingproductnames(form, product_name){
    return this.http.post<User[]>('https://dev.basiyn.com/buyer-order.php?page='+0+'&request=outstandingordersdetail&pname='+product_name ,form);
   
}

//search for process order detail page(sell side/orders)
searchprocessorderdetailpagenames(form, product_name){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=request=processodersdetail&pname='+product_name ,form);
   
}
//search for process order  page(sell side/orders)
searchprocessorderproductnames(form, product_name){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=processOrder&pname='+product_name ,form);
   
}
//search for process order detailed page(sell side/orders)
searchcancelorderproductnames(form, product_name){
    return this.http.post<User[]>('https://dev.basiyn.com/order-seller.php?page='+0+'&request=processOrder&pname='+product_name ,form);

}
//search for waiting seller acceptance detail page(bue side/my stores)
searchbuyerawaitingselleracceptanceproductnames(form, product_name){
    return this.http.post<User[]>('https://dev.basiyn.com/buyer-shared-products-by-seller.php?page='+0+'&pname='+product_name ,form);
   
}

//view customers detail page(sell side/customers)
getsellbuyerprodvarients(form,id){
    return this.http.post<User[]>('https://dev.basiyn.com/seller-buyer-products-variants.php?new=true&page='+0+'&buyer_id='+id, form)
}
//search for waiting seller acceptance detail page(bue side/my stores)
searchviewcustomerproddetailpage(form,id, product_name){
    return this.http.post<User[]>('https://dev.basiyn.com/seller-buyer-products-variants.php?page='+0+'&new='+true+'&pname='+product_name+'&buyer_id='+id ,form);
   
}
//view seller detail page(sell side/customers)
getviewsellerprodvarients(form,id){
    return this.http.post<User[]>('https://dev.basiyn.com/buyer-my-products.php?page='+0+'&seller='+id, form)
}

//search for waiting seller acceptance detail page(bue side/my stores)
searchviewsellerproddetailpage(form,id, product_name){
    return this.http.post<User[]>('https://dev.basiyn.com/buyer-my-products.php?page='+0+'&seller='+id+'&pname='+product_name,form);
   
}



//seller process order page on dropdown change
outstndorddrop( form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?page=0&request=processOrder', form);
}
// place order by seller(buy side-my orders)
getplaceorderbyseller( form){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?page=0&request=getsellers', form);
} 
//Search for place order by seller(buy side-my orders)
searchplaceorderbysellernames( form){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?page=0&request=getsellers', form);
    } 
// place order by product(buy side-my orders)
getplaceorderbyproducts( form){
    return this.http.post<User[]>(this.baseurl+'buyer-products-variants-order-page-filters.php', form);
}
// place order proposed order(buy side-my orders)
getplaceorderproposedorder( form){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=proposeorder-list&page=0', form);
}
// place order proposed order detail page(buy side-my orders)
getbuyplaceorderbuproposedorderdetails( form){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=proposeorder-detail&page=0', form);
}
// Cart(buy side-my orders)
getbuyercart( form){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=cartsummary&page=0', form);
}
// Cart-detail page(buy side-my orders)
getbuyercartdetail( form){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=getcart&page=0', form);
}
// Cart detail page(buy side-my orders)
deleterowcartdetailpage( form){
    return this.http.post<User[]>(this.baseurl+'delete-cart-product.php', form);
}

viewrtdcondispatch(form){
    return this.http.post<User[]>(this.baseurl+'confirm-rtd.php', form);
}

resetpasswords(form){
    return this.http.post<User[]>(this.baseurl+'profiles.php?request=updatepassword', form);
}
updateusermobilenumber(form){
    return this.http.post<User[]>(this.baseurl+'update_mobile.php', form);
}
//request for catalog buy side
reqcatalogbuyside(form){
    return this.http.post<User[]>(this.baseurl+'request-catalog-buyer.php', form);
}

//export outstanding orders
exportoutorder(form){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=outstandingordersdetail', form);
}

//export buyer side order summary
buyerordersumm(form){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=orders&export=yes', form);
}

//buyer dispatch summary
buyerdispsumm(form){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=dispachList&export=yess', form);
}

//seller dispach order summary exportcsv
sellerdispsummary(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=DispatchHistroy&export=yes', form);
}

//sell side received order history
receivedordhistory(form){
    return this.http.post<User[]>(this.baseurl+'order-seller.php?request=recivedorderexport&nopagination=no', form);
}


//cancel order detail page
markcancel(form){
    return this.http.post<User[]>(this.baseurl+'seller-cancel-process-order.php', form);
}

markreadytodisp(form){
    return this.http.post<User[]>(this.baseurl+'seller-mark-rtd.php', form);
}

loadproductsbuyside(form, page){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?page='+page+'&request=getproducts', form);
}

//sett bookmark in buy side product page
bookmarkbuyside(form){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?request=setfavor',form);
}

//buyerside add to cart
addtocart(form){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?request=addtocart', form);
}
//place-order ->detail page(BUY sise-Orders)
getplaceorderdetailpage(form){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?page=0&request=getproducts', form);
}

//place-order get varients-> detail page(BUY sise-Orders)
getplaceorderdetailpagevarients(form){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?request=getvariantsLevels', form);
}
//call api on change
changevalues(id, value, userid, var2){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?page=0&userid='+userid+'&mid='+id+'&variant_1='+value+'&variant_2='+ var2+'&request=getvariants','');
}

//buyseerside product search in placeorder page
searchbuypro(form){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?page=0&request=getproducts',form);
}

//load categories in buyer side place order page
loadcategoriesbuy(form){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?page=0&request=filters', form);
}
//catfilter
catfilt(form){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?page=0&request=getproducts', form);
}

//catfilter buy side place order
mycatfilter(form){
    return this.http.post<User[]>(this.baseurl+'placeorder.php?page=0&request=getproducts', form);
}

//quickchecckout get product details
quickgetdetails(form){
    return this.http.post<User[]>(this.baseurl+'buyer-order.php?request=getcart&page=0', form);
}


checkoutwithcardids(form){
    return this.http.post<User[]>(this.baseurl+'buyer-place-order.php',form);
}

//update cart page
updatecart(form){
    return this.http.post<User[]>(this.baseurl+'update-cart.php', form);
}

//get address in cart page buy side
getaddress(form){
    return this.http.post<User[]>(this.baseurl+'get-user-address.php', form);
}

//sett default address in cart
setdefaultadadress(form){
    return this.http.post<User[]>(this.baseurl+'update-cart-delivery-address.php', form);
}

//delete address
deleteadd(form){
    return this.http.post<User[]>(this.baseurl+'delete-user-address.php', form);
}
//get cities
getcityincart(statevalue){
    return this.http.get<User[]>(this.baseurl+'cities.php?state+id='+statevalue);
   
}

//add new adderss
addnewadd(form){
    return this.http.post<User[]>(this.baseurl+'add-user-address.php', form);
}

//placeorder buy side
placeorderbuyside(form){
    return this.http.post<User[]>(this.baseurl+'buyer-place-order.php', form);
}
}






