import * as _ from 'underscore';

export class PagerService {
    getPager(total: number, current_page: number = 1, per_page: number) {
        // calculate total pages
        let totalPages = Math.ceil(total / per_page);
       // console.log('t.....')
        console.log(totalPages)

        let startPage: number, endPage: number;
        //console.log(total);
        
        if (totalPages <= 5) {
            startPage = 1;
            endPage = totalPages;
        } else {
            if (current_page <= 3) {
                startPage = 1;
                endPage = 5;
            } else if (current_page + 1 >= totalPages) {
                startPage = totalPages - 4;
                endPage = totalPages;
            } else {
                startPage = current_page - 2;
                endPage = current_page+2;
            }
        }

        // calculate start and end item indexes
        let startIndex = (current_page - 1) * per_page;
        let endIndex = Math.min(startIndex + per_page - 1, total - 1);

        // create an array of pages to ng-repeat in the pager control
        let pages = _.range(startPage, endPage + 1);

        // return object with all pager properties required by the view
        return {
            total: total,
            current_page: current_page,
            per_page: per_page,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }
}