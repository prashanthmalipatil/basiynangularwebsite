import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/_services';
import { first } from 'rxjs/operators';
  declare var swal:any;

@Component({
  selector: 'app-update-transfer-amount',
  templateUrl: './update-transfer-amount.component.html',
  styleUrls: ['./update-transfer-amount.component.css']
})
export class UpdateTransferAmountComponent implements OnInit {
  currentUser:User;
  customers:any;
  updateprice:FormGroup;
  submitted=false;
  loading=false;
  show=false;
  constructor(private formBuilder:FormBuilder, private userService:UserService) {this.currentUser=JSON.parse(localStorage.getItem('currentUser')) }

  ngOnInit() {
    this.loadcustomer();
    this.updateprice = this.formBuilder.group({
      match_id: ['', Validators.required],
      category: ['', Validators.required],
      mydate: ['', Validators.required],
      myamount: ['', Validators.required],
      remarks: [''],
  });
  }


  loadcustomer(){
    var form=new FormData()
    form.append('userid', this.currentUser.id);
    this.userService.loadbuysidedetails(form).pipe(first()).subscribe(
      data =>{
          this.customers=data;
      }
    );
  }


  ChangingValue(event){

    console.log(event);
  }
  seleccat(){
    this.show=true;
  }


  get f() { return this.updateprice.controls; }


  onSubmit(){

    this.submitted = true;
      
        // stop here if form is invalid
        if (this.updateprice.invalid) {
            //console.log(this.loginForm.controls);
            return;
        }

        this.loading = true;
    console.log(this.updateprice.value);

    var form=new FormData();
    form.append('userid', this.currentUser.id);
    form.append('user_match_id', this.f.match_id.value);
    form.append('category', this.f.category.value);
    form.append('myamount', this.f.myamount.value);
    form.append('remarks', this.f.remarks.value);
    form.append('mydate', this.f.mydate.value);

    this.userService.updatepricebuyeraccount(form).pipe(first()).subscribe(data=>{
      console.log(data);
      if(data['status']=='success'){
        swal('Amount is inserted', "", 'success');
          this.updateprice.reset();
          this.submitted=false;
      }
      else{
        swal('Sorry price could not be updated', '', 'error');
      }
    });
  }
}
