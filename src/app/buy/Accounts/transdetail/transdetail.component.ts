import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { UserService, PagerService } from 'src/app/_services';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
declare var $:any;
declare var swal:any;
@Component({
  selector: 'app-transdetail',
  templateUrl: './transdetail.component.html',
  styleUrls: ['./transdetail.component.css']
})
export class TransdetailComponent implements OnInit {
  currentuser:User
  sellerid:any;
  transdetail:any;
  loggedinid:any;
  loadingnew=true;
  total: number;
  p: number = 1;
  objectionid:any;
  chattxt:any[]=[];
  chattext:any;
  ediamount:any;
  userid:any;
  id:any;
  nickname:any;
  
  user_amount:any;
  invoice:any;
  exportdata:any[]=[];
  csvdata:any[]=[];
  Filterbydates:FormGroup;
  editamountform:FormGroup;
  csvdateform:FormGroup;
  from:any;
  to:any;
  replyMessage = "";
  per_page:any;
  constructor(private userService:UserService,private formBuilder:FormBuilder, private pagerService: PagerService, private route:ActivatedRoute, private router: Router) { 
    this.currentuser=JSON.parse(localStorage.getItem('currentUser'));

    this.route.queryParams.subscribe(params => {
    
      this.sellerid=params['id']
      });
      console.log(this.sellerid)

      this.loggedinid=this.currentuser.id;
  }


  //Pagination
  allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];



  ngOnInit() {
    this.loadtransdetbuy();
    this.setPage(1);
    this.Filterbydates = this.formBuilder.group({
      fromdate: ['', Validators.required],
      todate:['',Validators.required]});

      this.editamountform = this.formBuilder.group({
        userid: ['', Validators.required],
        id:['',Validators.required],
        editamount:['',Validators.required],
        newamount:['', Validators.required]
      });

      this.csvdateform = this.formBuilder.group({
        fromDate: ['', Validators.required],
        toDate:['', Validators.required]
      });
    
  }

  loadtransdetbuy(page: number = 1){
    console.log(this.sellerid)
    var form=new FormData();
    form.append('userid', this.currentuser.id);
    form.append('sellerId', this.sellerid);

    this.userService.loadtransbuy(form, page).pipe(first()).subscribe(
      data =>
      {
        this.loadingnew=false;
        this.transdetail= data['transactions'];
        this.nickname=data['transactions'][0]['seller_nickname'];
        console.log(this.nickname);
        this.user_amount=data['user_amount'];
        this.total = data['count'];
        this.per_page=20;
        this.pager = this.pagerService.getPager(this.total, page, this.per_page);
      }
    )
  }
  setPage(page: number) {

    if (page < 1 || page > this.pager.total) {
        return;
    }
  
    this.loadtransdetbuy(page);
  
  }



  chatboxbuy(id){
    $("#chatbuy").modal();
    console.log(id);
    this.objectionid=id;

    var form=new FormData();
    form.append('obection_id', this.objectionid);
    this.userService.chatbox(form).pipe(first()).subscribe(
      data=>{
        console.log(data);
        this.chattxt=data['comments'];
      }
    )
  }


  submit(ngForm) {
    console.log(ngForm);
    this.chattext=ngForm.replyMessage
    this.chattxt.push({
      "text":this.replyMessage,
      "self":true
    })
    this.replyMessage = "";
    var form=new FormData();
    form.append('userid', this.currentuser.id);
    form.append('transaction_id', this.objectionid);
    form.append('transaction_comment', this.chattext);
    this.userService.userchatsub(form).pipe(first()).subscribe(
      data=>{
        console.log('success');
        this.chatboxbuy(this.objectionid);
       
      }
    )
   
  }


  //close objection

  closeobjection(id){
    console.log(id);

    swal({
      
      buttons: {
       
        roll: {
          text: "Yes Close it",
          value: "true",
        },
        roll1: {
          text: "No Cancel it",
          value: "false1",
        },
      
      },
          }).then((value) => {
            //console.log(value);
            if (value=='true') {
                var form=new FormData();
                form.append('transaction_id', id);
                 this.userService.closeobj(form).pipe(first()).subscribe(
                   data =>{
                     console.log(data);
                     this.loadtransdetbuy();
                   }
                 )
              
            }
            else if(value=='false1'){
           
  }
  else{

  }
});
  }



  //edit amount
  editamnt(transactions){
    $("#amount").modal();
    console.log(transactions);
    this.ediamount=transactions.amount;
    this.userid=transactions.user_id;
    this.id=transactions.id

  }

  get f(){return this.editamountform.controls;}
  //edit amount submit button
  amntsubmit(){
   

    var formeditamnt= this.f.newamount.value;
    var form=new FormData();
    form.append('userid', this.userid);
    form.append('transaction_id', this.id);
    form.append('amount',formeditamnt);

    this.userService.editamnt(form).pipe(first()).subscribe(
      data=>{
        if(data['status']='true'){
        swal('price updated Succesfully', '', 'success');
        this.loadtransdetbuy();
        $("#amount").modal('hide');
     
      }
      else{
        swal('Sorry price coudnt be updated', '', 'error');
         $("#amount").modal('hide');
      }
    }
    
    )
  }



  //filter dates and export code starts here

  get g(){return this.Filterbydates.controls;}



  onSubmit(){
    console.log(this.Filterbydates.value);
    console.log(this.g.fromdate.value);
    this.from=this.g.fromdate.value;
    this.to=this.g.todate.value;
    if($("#new1").val()=='' || $("#new2").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new1").val()>$("#new2").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new1").val()<=$("#new2").val()){

      $("#refresh").show();
      var form= new FormData()
  form.append('userid', this.currentuser.id);
  form.append('fromDate', this.from);
  form.append('toDate', this.to);
  form.append('sellerId', this.sellerid )
  this.userService.customertransdet(form).pipe(first()).subscribe(
    data => {
      this.transdetail=data['transactions'];
    }
  );
      
  }
  }

  //export csv file
  export(){
      $('#export').modal();
  }


  get h(){return this.csvdateform.controls;}


  //export trans details on click
  exportdatadate(){
    if($("#fromd").val()=='' || $("#tod").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#fromd").val()>$("#tod").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#fromd").val()<=$("#tod").val()){


      var formfrom= this.h.fromDate.value;
      var formto= this.h.toDate.value;
      var form=new FormData();
      form.append('userid', this.currentuser.id);
      form.append('fromDate', formfrom);
      form.append('toDate', formto);
      form.append('sellerId', this.sellerid);

      this.userService.exportfilterdatabuyside(form).pipe(first()).subscribe(
        data=>{
          $('#export').modal('hide');
          console.log(data);
          this.invoice=data['transactions'];
          var exportdata1=this.invoice.length;
          for (let i=0; i<exportdata1; i++){
            var datajs={"Date": this.invoice[i]['Date'], "Seller Name": this.invoice[i]['Seller Name'], "Category": this.invoice[i]['Category'], "Amount":this.invoice[i]['amount'],  "Updated By":'Seller', "Remarks":this.invoice[i]['Remarks'] }
              this.csvdata.push(datajs);
             }
              console.log(this.csvdata);

              var options = {
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalseparator: '.',
                showLabels: true,
                showTitle: true,
                useBom: true,
                noDownload: false,
                headers: ["Date", "Seller name", "Category", "Amount", "Updated By", "Remarks"]
              };
            
            new Angular5Csv(this.csvdata, 'payment_list', options);

        }
      )
  }
}



cleardates(){
  $("#refresh").hide();
 this.Filterbydates.reset();
this.loadtransdetbuy();
 }
}
