import { Component, OnInit, Pipe } from '@angular/core';
import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
declare var $:any;
@Pipe({name: 'negativeNumber'})
@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.css']
})
export class buyacntrandetailsComponent implements OnInit {
  currentUser:User;
  loadcustomerdetails:any;
  loadingnew=true;
  transform(value: number): number { 
    return Math.abs(value)*(-1);
  }
  constructor(private userService:UserService, private route:ActivatedRoute, private router: Router) { this.currentUser=JSON.parse(localStorage.getItem('currentUser'))}

  ngOnInit() {
   this.loadselleraccount();
   $(".clear").hide();
  }


  loadselleraccount(){
    var form=new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.loadcustaccountdetails(form).pipe(first()).subscribe(
      data=>{
        this.loadcustomerdetails=data['customers'];
        this.loadingnew=false;
      }
    )
  }


  transdet(match_id){
    console.log(match_id);

    let navigationExtras: NavigationExtras = {
      queryParams: {
      "id":match_id,
     
    
      }
      };
      this.router.navigate(['/mini-account-buyer-transaction-details/'],navigationExtras);
  }



  submit(ngForm){
    $(".clear").show();
    console.log(ngForm);
    var form=new FormData();
    form.append('cname', ngForm.cname);
    form.append('userid', this.currentUser.id);
    this.userService.searchsell(form).pipe(first()).subscribe(
      data=>{
          this.loadcustomerdetails=data['customers'];

      }
    )
  }

  clearsearchcustomer(){
    $(".clear").hide();
    this.loadselleraccount();
  }


}
