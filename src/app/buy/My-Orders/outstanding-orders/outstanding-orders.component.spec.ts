import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutstandingOrdersComponent } from './outstanding-orders.component';

describe('OutstandingOrdersComponent', () => {
  let component: OutstandingOrdersComponent;
  let fixture: ComponentFixture<OutstandingOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutstandingOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutstandingOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
