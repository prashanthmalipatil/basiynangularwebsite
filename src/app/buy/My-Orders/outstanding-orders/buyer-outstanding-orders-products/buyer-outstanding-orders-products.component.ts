import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $ :any;

@Component({
  selector: 'app-buyer-outstanding-orders-products',
  templateUrl: './buyer-outstanding-orders-products.component.html',
  styleUrls: ['./buyer-outstanding-orders-products.component.css']
})
export class BuyerOutstandingOrdersProductsComponent implements OnInit {

  currentUser: User;
  asbuyeroutstanding: any[] = [];
  loadingnew=true;
  sellerid:any;
  Searchseller:FormGroup;
  loading=true;
  submitted=false;

  constructor(private userService: UserService, private formBuilder:FormBuilder, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.queryParams.subscribe(params => {
      this.sellerid=params['seller_id']
      
      
      });
      console.log(this.sellerid);
  }

  ngOnInit() {

    $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
 $(".clear").hide();
 this.Searchseller = this.formBuilder.group({
  product_name: ['', Validators.required],
  
});
    this.loadbuyeroutstandingordresproducts();
  }
  get f() { return this.Searchseller.controls; }

  onSubmitsearch() {
    // console.log("came to submit");
    this.submitted = true;
     
    // // stop here if form is invalid
     if (this.Searchseller.invalid) {
        
         console.log('false data');
         return;
     }
     this.loading = true;
      
       var form = new FormData();
      
       form.append('userid', this.currentUser.id);
       form.append('pname', this.Searchseller.value.product_name);
        console.log(form);
       this.userService.searchoutstandingproductnames(form, this.Searchseller.value.product_name)
       .pipe(first())
       .subscribe(
        data => {
                  
          this.asbuyeroutstanding=data['order'];
       
          });

  }
  clearsearch(){
    $(".clear").hide();
    this.asbuyeroutstanding=[];
   this.Searchseller.reset();
    this.loadbuyeroutstandingordresproducts();
   }

  loadbuyeroutstandingordresproducts() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('value',this.sellerid)

    this.userService.getbuyeroustanding(form).pipe(first()).subscribe(result => { 
      
        this.asbuyeroutstanding= result['order']; 
        this.loadingnew=false;

        //console.log(result);
        
    });
  }

}
