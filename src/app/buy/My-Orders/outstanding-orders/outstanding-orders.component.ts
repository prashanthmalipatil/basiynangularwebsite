import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

declare var $ :any;

@Component({
  selector: 'app-outstanding-orders',
  templateUrl: './outstanding-orders.component.html',
  styleUrls: ['./outstanding-orders.component.css']
})
export class OutstandingOrdersComponent implements OnInit {

  currentUser: User;
  asoutstanding: any[] = [];
  outstandingorderquantity: any[]=[];
  outstandingordervalue: any[]=[];
  
  loadingnew=true;
  newsellerid: any[]=[];
  custname=true;
  selectedvalue:any;
  exportdatacon:any;
  csvdata:any[]=[];
  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
    this.loadoutstandingordres();
  }
  loadoutstandingordres() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);

    if(this.selectedvalue!=null){
      form.append('order_by', this.selectedvalue);
    }

    this.userService.getoustanding(form).pipe(first()).subscribe(result => { 
      this.outstandingorderquantity= result['TotaloutStandingQty'];
      this.outstandingordervalue=result['Totaloutstanding_order_amount'];
        this.asoutstanding= result['order']; 
        this.loadingnew=false;

        //console.log(result);
        
    });
  }
  
  sellerid(user){
    console.log(user);
    this.newsellerid=user.seller_id;
    console.log(this.newsellerid);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
      "seller_id":user.seller_id
    
      }
      };
      this.router.navigate(['/buyer-outstanding-orders-products/'],navigationExtras);
  }


  handleSelectedValue(value){
    this.custname=false;
    this.selectedvalue=$( "#group" ).val();
    this.loadoutstandingordres();
    
  }

  exportdata(){
    var form=new FormData();
    form.append('userid', this.currentUser.id);
    this.userService.exportoutorder(form).pipe(first()).subscribe(
      data=>{
        this.exportdatacon=data['order'];
        var exportdata1=this.exportdatacon.length;
        for (let i=0; i<exportdata1; i++){
          var datajs={"Order No": this.exportdatacon[i]['order_no'], "Product Id": this.exportdatacon[i]['product_id'], "Product Name": this.exportdatacon[i]['product_name'], "Seller Id":this.exportdatacon[i]['seller_id'],  "Seller Name":this.exportdatacon[i]['seller_nickname'] , "Cost":this.exportdatacon[i]['sales_mrp'], "order Date": this.exportdatacon[i]['order_date'], "Deliver by Date": this.exportdatacon[i]['deliver_by_date'], "Time to fulfil(days)":this.exportdatacon[i]['sales_time_to_fulfill'], 'Outstanding Quantity':this.exportdatacon[i]['outstanding_qty']  }
            this.csvdata.push(datajs);
           }

        var options = {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: true,
          useBom: true,
          noDownload: false,
          headers: ["Order No", "Product Id", "Product Name", "Seller Id", "Seller Name", "Cost","order Date", "Deliver by Date", "Time to fulfil(days)", "Outstanding Quantity"]
        };
      
      new Angular5Csv(this.csvdata, 'Buyer-Outstanding-Orders-Sanjay-2018-12-17', options);
      }
    )
  }
 
}
