import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $ :any;
declare var swal:any; 
@Component({
  selector: 'app-cancelled-by-seller',
  templateUrl: './cancelled-by-seller.component.html',
  styleUrls: ['./cancelled-by-seller.component.css']
})
export class CancelledBySellerComponent implements OnInit {
  currentUser: User;
  ascancelled: any[] = [];
  loadingnew=true;
  cancel_summary_id:any[] = [];
  Searchseller: FormGroup;
  Filterbydates:FormGroup;
    from:any;
    to:any;
    submitted = false;
    loading=false;
    searchseller:any[]=[];
    ids:any[]=[];
    searchdata:any;
  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); }

  ngOnInit() {
    $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
 $(".clear").hide();
 $("#refresh").hide();
  
    this.checkvalue();
  }


  // loadcancelled() {
    
  //   var form = new FormData();
  //   form.append('userid', this.currentUser.id);

  //   this.userService.getcancelled(form).pipe(first()).subscribe(result => { 
     
  //       this.ascancelled= result['order']; 
        
  //       this.loadingnew=false;
  //       //console.log(result);
        
  //   });
  // }
  cancelorderbyseller(user){
    console.log(user);
    this.cancel_summary_id=user.cancel_summary_id;
    console.log(this.cancel_summary_id);
    
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
      "cancel_summary_id":user.cancel_summary_id,
      
    
      }
      };
      this.router.navigate(['/buyer-cancel-summary-products/'],navigationExtras);
}

checkvalue(page: number = 1){
  //this.selectedvalue=$( "#group" ).val();
  //this.filterbydd=$("#group-by").val();
  this.from=$("#new1").val();
  this.to=$("#new2").val();
  this.searchdata=$("#mysearch").val();
  
  var form=new FormData();
  
  

  if(this.from!=null && this.to!=null){
    
   
    form.append('from', this.from);
    form.append('to', this.to);
  }

  

  if(this.searchdata!=null){
    form.append('sname', this.searchdata);
  }
  form.append('userid', this.currentUser.id);
  this.userService.getcancelled(form).pipe(first()).subscribe(
    data=>{
      //this.asprocessorder= data['order']; 
      this.ascancelled= data['order']; 

      this.loadingnew=false;
    
    }
  )
}


Searchbar(){
  this.checkvalue();
  
}
searchbydates(){
  

  if($("#new1").val()=='' || $("#new2").val()==''){
    
    swal("please select a from and to dates", '', 'info');
  
  }
  if($("#new1").val()>$("#new2").val()){
    swal("from date should be less than to date", '', 'error');
  }
  if($("#new1").val()<=$("#new2").val()){
    this.checkvalue();
  $("#refresh").show();
  }

  
  //$('#refresh').css("display", "block");
}




clearsearch(){
  $(".clear").hide();
  $("#mysearch").val('');
  this.checkvalue();
 }

 cleardates(){
   $("#refresh").hide();
   $("#new1").val('');
   $("#new2").val('');
   //this.from=[];
   //this.to=[];
   this.checkvalue();
  
 }

}
