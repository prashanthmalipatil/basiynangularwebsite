import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerCancelSummaryProductsComponent } from './buyer-cancel-summary-products.component';

describe('BuyerCancelSummaryProductsComponent', () => {
  let component: BuyerCancelSummaryProductsComponent;
  let fixture: ComponentFixture<BuyerCancelSummaryProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerCancelSummaryProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerCancelSummaryProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
