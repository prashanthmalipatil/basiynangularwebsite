import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';

declare var $ :any;

@Component({
  selector: 'app-buyer-cancel-summary-products',
  templateUrl: './buyer-cancel-summary-products.component.html',
  styleUrls: ['./buyer-cancel-summary-products.component.css']
})
export class BuyerCancelSummaryProductsComponent implements OnInit {
  currentUser: User;
  ascancelledsummary: any[] = [];
  ascancelleddetails: any[]=[];
  loadingnew=true;
  idcancelsummary:any;
  

  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.queryParams.subscribe(params => {
      this.idcancelsummary=params['cancel_summary_id']
     
      
      });
      console.log(this.idcancelsummary);
     
  }

  ngOnInit() {
    this.loadbuyercancelledsummary();
  }
  loadbuyercancelledsummary() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('cancel_summary_id',this.idcancelsummary);
    this.userService.getbuyercancelledsummary(form).pipe(first()).subscribe(result => { 
        this.ascancelleddetails= result['cancel_details'];
        this.ascancelledsummary= result['order']; 
        this.loadingnew=false;
        
        //console.log(result);
        
    });
  }

}
