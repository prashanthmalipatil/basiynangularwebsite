import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelledBySellerComponent } from './cancelled-by-seller.component';

describe('CancelledBySellerComponent', () => {
  let component: CancelledBySellerComponent;
  let fixture: ComponentFixture<CancelledBySellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelledBySellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelledBySellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
