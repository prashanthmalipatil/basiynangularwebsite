import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';

declare var $ :any;

@Component({
  selector: 'app-buyer-dispatch-summary-products',
  templateUrl: './buyer-dispatch-summary-products.component.html',
  styleUrls: ['./buyer-dispatch-summary-products.component.css']
})
export class BuyerDispatchSummaryProductsComponent implements OnInit {
  currentUser: User;
  asdispatchsummary: any[] = [];
  asattachments:any[]=[];
  asorders:any[]=[];
  loadingnew=true;

   iddispatch:any;
   idorder:any;
  

  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.queryParams.subscribe(params => {
      this.iddispatch=params['id_dispatch'],
      this.idorder=params['order_no']
      
      });
      console.log(this.iddispatch);
      console.log(this.idorder);
  }

  ngOnInit() {
    this.loadbuyerdispatchsummary();
  }

  loadbuyerdispatchsummary() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('dispatch_id',this.iddispatch);
    form.append('order_no_id',this.idorder);
    this.userService.getbuyedispatchsummaryprod(form).pipe(first()).subscribe(result => { 
      this.asattachments= result['attachments'];
        this.asdispatchsummary= result['dispatch_details']; 
        this.asorders= result['order'];
        this.loadingnew=false;
        
        //console.log(result);
        
    });
  }

  downloadatt(){
    
    $('#export').show();
   
  }
  closemod(){
    $('#export').hide();
  }
}
