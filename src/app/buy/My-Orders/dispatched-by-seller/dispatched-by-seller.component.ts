import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

declare var swal:any; 
declare var $ :any;
@Component({
  selector: 'app-dispatched-by-seller',
  templateUrl: './dispatched-by-seller.component.html',
  styleUrls: ['./dispatched-by-seller.component.css']
})
export class DispatchedBySellerComponent implements OnInit {
  currentUser: User;
  asdispatch: any[] = [];
  loadingnew=true;
  dispatchid: any[] =[];
  orderid: any[]=[];
  Searchseller: FormGroup;
  Filterbydates:FormGroup;
    from:any;
    to:any;
    submitted = false;
    loading=false;
    searchseller:any[]=[];
    ids:any[]=[];
    searchdata:any;
    csvdata:any[]=[];
    exportdatacon:any[]=[];
    filename:any;
  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); }

  ngOnInit() {
    $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
 $(".clear").hide();
 $("#refresh").hide();
  


  //   this.Searchseller = this.formBuilder.group({
  //     seller_name: ['', Validators.required],
      
  // });

  // this.Filterbydates = this.formBuilder.group({
  //   fromdate: ['', Validators.required],
  //   todate:['',Validators.required]});

  this.checkvalue();
  }

  checkvalue(page: number = 1){
    //this.selectedvalue=$( "#group" ).val();
    //this.filterbydd=$("#group-by").val();
    this.from=$("#new1").val();
    this.to=$("#new2").val();
    this.searchdata=$("#mysearch").val();
    console.log(this.from);
    console.log(this.to);
    var form=new FormData();
    
    
  
    if(this.from!=null && this.to!=null){
      
     
      form.append('from', this.from);
      form.append('to', this.to);
    }
  
    
  
    if(this.searchdata!=null){
      form.append('sname', this.searchdata);
    }
    form.append('userid', this.currentUser.id);
    this.userService.getdispatch(form).pipe(first()).subscribe(
      data=>{
        //this.asprocessorder= data['order']; 
        this.asdispatch= data['order']; 

        this.loadingnew=false;
      
      }
    )
  }

  
  Searchbar(){
    this.checkvalue();
    
  }
  searchbydates(){
    

    if($("#new1").val()=='' || $("#new2").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new1").val()>$("#new2").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new1").val()<=$("#new2").val()){
      this.checkvalue();
    $("#refresh").show();
    }

    
    //$('#refresh').css("display", "block");
  }
  
  
  
  
  clearsearch(){
    $(".clear").hide();
    $("#mysearch").val('');
    this.checkvalue();
   }
  
   cleardates(){
     $("#refresh").hide();
     $("#new1").val('');
     $("#new2").val('');
     //this.from=[];
     //this.to=[];
     this.checkvalue();
    
   }

  orderdispatch(user){
    console.log(user);
    this.dispatchid=user.id_dispatch;
    console.log(this.dispatchid);
    this.orderid=user.order_no;
    console.log(this.orderid);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
      "id_dispatch":user.id_dispatch,
      "order_no":user.order_no
    
      }
      };
      this.router.navigate(['/buyer-dispatch-summary-products/'],navigationExtras);
  }



  closemod(){
    $('#export').hide();
  }

  exportorders(){
    $('#export').show();

  }
  datesub(ngForm){
    if($("#new3").val()=='' || $("#new4").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new3").val()>$("#new4").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new3").val()<=$("#new4").val()){

      var form=new FormData();
      form.append('from', ngForm.from);
      form.append('to', ngForm.to);
      form.append('userid', this.currentUser.id)
      this.userService.buyerdispsumm(form).pipe(first()).subscribe(
        data=>{
          this.exportdatacon=data['order'];
          this.filename=data['filename'];
          console.log(this.filename);
        var exportdata1=this.exportdatacon.length;
        for (let i=0; i<exportdata1; i++){
          var datajs={"Order No": this.exportdatacon[i]['order_no'],"Dispatch No": this.exportdatacon[i]['dispatch_no'], "Product Id": this.exportdatacon[i]['product_id'], "Product Name": this.exportdatacon[i]['product_name'], "Seller Id":this.exportdatacon[i]['seller_id'],  "Seller Name":this.exportdatacon[i]['seller_nickname'] , "Cost":this.exportdatacon[i]['rate'], "order Date": this.exportdatacon[i]['order_date'], "Deliver by Date": this.exportdatacon[i]['deliver_by_date'], 'Order Quantity':this.exportdatacon[i]['product_level_quantity']  }
            this.csvdata.push(datajs);
           }

        var options = {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: true,
          useBom: true,
          noDownload: false,
          headers: ["Order No","Dispatch No", "Product Id", "Product Name", "Seller Id", "Seller Name", "Cost","order Date", "Dispatch Date", "Quantity"]
        };
      
      new Angular5Csv(this.csvdata, this.filename, options);
      }
        
      )
    console.log(ngForm);
  }
}
  
}
