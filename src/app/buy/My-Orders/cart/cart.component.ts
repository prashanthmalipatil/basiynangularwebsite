import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $ :any;
declare var swal:any;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  currentUser: User;
  asbuyercart: any[] = [];
  loadingnew=true;
  loading=false;
  sellerid:any[]=[];
  matchmasterid:any[]=[];
  buyerpickup:any[]=[];
  addressid:any[]=[];
  cartid:any[]=[];
  dispselleradd=true;
  dispcustadd=false;
  optvalue:any;
  cartsids:any[]=[];
  collectalldata:any;
  cartids:any[]=[];
  address_id:any;
  constructor(private userService: UserService, private formBuilder:FormBuilder, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); }

  ngOnInit() {
    this.loadpbuyercart();
    this.optvalue=1;
  }

  loadpbuyercart() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    //form.append('value',this.sellerid)

    this.userService.getbuyercart(form).pipe(first()).subscribe(result => { 
        
        this.asbuyercart=result['order'];
        
        this.loadingnew=false;

        //console.log(result);
        
    });
  }

  cartdetail(user){
    console.log(user);
    this.sellerid=user.seller_id;
    console.log(this.sellerid);
    this.matchmasterid=user.match_master_id;
    console.log(this.matchmasterid);
    console.log(user);
    this.buyerpickup=user.buyer_pickup;
    console.log(this.buyerpickup);
    this.addressid=user.address_id;
    console.log(this.addressid);
    this.cartid=user.cart_id;
    console.log(this.cartid);
//this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
let navigationExtras: NavigationExtras = {
   queryParams: {
   'seller_id':user.seller_id,
   'match_master_id':user.match_master_id,
   'buyer_pickup':user.buyer_pickup,
   'address_id':user.address_id,
   'did':this.optvalue,
   'hd':user.s_home_delivery_status,

 
   }
   };
this.router.navigate(['/buyer-cart-details/'],navigationExtras);
 }

 onFilterChange(value, id){
  this.dispselleradd=true;
  this.dispcustadd=false;
  this.optvalue=$('.optyes').val()
  console.log(this.optvalue);
 }

 onFilterChangeno(){
   
   this.optvalue=$('.optno').val();
  console.log(this.optvalue);
  this.dispselleradd=false;
  this.dispcustadd=true;
 }


 //Quick checkut button
 quickcheckout(){
   var checckstatus=0;
  var form=new FormData();
  var anyBoxesChecked = false;
  form.append('userid', this.currentUser.id);
   console.log($("#checkAll").val());
   $('.checkeAll').each(function(){
    if ($(this).prop('checked')==true) {
      anyBoxesChecked = true;
      console.log(anyBoxesChecked);
      checckstatus=1;
      form.append('seller_id', $(this).val());
      form.append('deliver_by_date', $(this).data('ddate'));
      form.append('buyer_pickup', $(this).data('bpickup'));

    
    }
  
  
  })

  if (anyBoxesChecked == false) {
    //Do something
    swal('please select a row','','error');
  } 
  
  if (checckstatus==1) {
  this.userService.quickgetdetails(form).pipe(first()).subscribe(
    data=>{
      console.log(data);
      this.cartsids=data['order'];  
      console.log(this.cartsids);
      this.collectalldata=data['summary'];
      this.checckoutnew();
    }
    
  )
  }
 
    
    // console.log(obj);
  
}

checckoutnew(){

  for(var i=0; i<this.cartsids.length; i++){
   console.log(this.cartsids[i]['cart_id']);
   this.cartids.push(this.cartsids[i]['cart_id']);
  }
 
  this.address_id=this.collectalldata['address_id'];
var obj={'cart_ids':this.cartids,'buyer_pickup_flag': JSON.stringify(this.optvalue), 'address_id': parseInt(this.address_id), 'order_date':[this.collectalldata['deliver_by_date']], 'total_qty':this.collectalldata['quantity'],'total_amount':this.collectalldata['selling_price'], 'b_team_member_id':this.collectalldata['b_team_member_id'],'s_team_member_id':this.collectalldata['s_team_member_id']}
  var order=new Array();
  order.push(obj);
console.log(order);
var form=new FormData();
  form.append('userid', this.currentUser.id);
  form.append('order_data',JSON.stringify(obj));
  this.userService.checkoutwithcardids(form).pipe(first()).subscribe(
    data=>{
      if(data['status']=='true'){
      swal(data['msg'],'','success');
      this.loadpbuyercart();

      }
      else{
        swal(data['msg'],'','error');
      }
    }
  )
}

}
