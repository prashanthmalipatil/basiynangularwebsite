import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerCartDetailsComponent } from './buyer-cart-details.component';

describe('BuyerCartDetailsComponent', () => {
  let component: BuyerCartDetailsComponent;
  let fixture: ComponentFixture<BuyerCartDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerCartDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerCartDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
