import { Component, OnInit, PipeTransform, Pipe  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { result } from 'underscore';

declare var $ :any;
declare var swal:any;
@Component({
  selector: 'app-buyer-cart-details',
  templateUrl: './buyer-cart-details.component.html',
  styleUrls: ['./buyer-cart-details.component.css']
})
@Pipe({
  name: 'dateFormatPipe',
})
export class BuyerCartDetailsComponent implements OnInit {
  currentUser: User;
  ascartordersummary: any[] = [];
  ascartsummary: any[]=[];
  loadingnew=true;
  loading=false;
  idseller:any;
  idmatchmaster:any;
  idbuyerpickup:any;
  idaddress:any;
  deliverystatus:any;
  idcart=new Array();
  addZero:any;
  today: number = Date.now();
  homedelivery:any;
  truck=false;
  value:any;
  changeadd=false;
  checckedstatus:boolean;
  getstates:any;
  getcity:any;
  useraddresses:any;
addressform=false;
updaatecartdetails:any;
returnUrl:string;
pickupflag:any;
  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.queryParams.subscribe(params => {
      this.idseller=params['seller_id'],
      this.idmatchmaster=params['match_master_id'],
      this.idbuyerpickup=params['buyer_pickup'],
      this.idaddress=params['address_id']
      this.homedelivery=params['hd'];
      this.deliverystatus=params['did']
      });
      console.log(this.idseller);
      console.log(this.idmatchmaster);
      console.log(this.idbuyerpickup);
      console.log(this.homedelivery);

    
    }

  ngOnInit() {
    this.value=this.deliverystatus;
    this.loadbuyercartdetail();
    this.clicked(this.value, event);
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'buyer-place-order-new';
  }
  
 loadbuyercartdetail() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('seller_id',this.idseller);
    form.append('match_master_id',this.idmatchmaster);
    form.append('buyer_pickup',this.idbuyerpickup);
    form.append('address_id',this.idaddress);
  
    this.userService.getbuyercartdetail(form).pipe(first()).subscribe(result => { 
        this.ascartsummary=result['summary'];
        this.ascartordersummary= result['order']; 
        this.loadingnew=false;
        
        //console.log(result);
        
    });
  }
  
   
  

  deleterow(user) {

      console.log(user);
      this.idcart=user.cart_id;
       var form = new FormData();
      var ids=new Array();
      ids.push(this.idcart); 
      
      // var test = ids;
      //   console.log(test);
        
       form.append('userid', this.currentUser.id);
       form.append('cart_ids',JSON.stringify(ids));
       
        console.log(form);
       this.userService.deleterowcartdetailpage(form)
       .pipe(first())
       .subscribe(
        data => {
          // this.ascartsummary=result['summary'];
          //  this.ascartordersummary=result['order'];
           //
         
         this.loadbuyercartdetail();
         ids=[];
          });

  }

  //Cart update
  updatecart(details){
    this.updaatecartdetails=details;
    var parinp=new Array();
    var partdate=new Array();
    var partcartid=new Array();
   var cartdata=new Array();
console.log(this.updaatecartdetails)

    partcartid=this.updaatecartdetails;
    console.log(partcartid)


    $('.quantityval').each(function(){
    
      parinp.push($(this).val());
    
  })
  console.log(parinp);


  $('.cartdate').each(function(){
    
    partdate.push($(this).val());
   
  
})
console.log(partdate);



for(var i=0; i<this.ascartordersummary.length; i++){
  console.log(partcartid[i]['cart_id']);
  var obj={"cart_id":partcartid[i]['cart_id'], 'qty': parinp[i],'deliver_by_date':partdate[i], 'buyer_pickup_flag':this.ascartsummary['buyer_pickup_flag']}
  cartdata.push(obj);
}
 console.log(cartdata);
var newcartdata=new Array();
newcartdata=cartdata;
 var form=new FormData();
 form.append('userid', this.currentUser.id);
 form.append('cart_data', JSON.stringify(newcartdata));

 this.userService.updatecart(form).pipe(first()).subscribe(
   data=>{
    this.loadbuyercartdetail();
   }
 )
}

clicked(value: any, event: boolean | Event){
 // console.log(this.truck)
  //console.log(event);
  console.log(value);

  
  if(value==true){
    this.truck=false;
   this.changeadd=false;
    this.checckedstatus=true;
    this.pickupflag=1;
  }
  else if(value==false){
    this.truck=true;
    this.changeadd=true;
    this.pickupflag=0;
  }
  else if(this.deliverystatus==0){
    this.truck=true;
    this.changeadd=true;
    this.checckedstatus=false;
    this.pickupflag=0;
  }
  else if(this.deliverystatus==1){
    this.truck=false;
    this.changeadd=false;
    this.checckedstatus=true;
    this.pickupflag=1;
  }
  
}


//change address popup get addresses
changeaddress(){
  $('#changeadd').show();
  var form=new FormData();
  form.append('userid', this.currentUser.id);
  this.userService.getaddress(form).pipe(first()).subscribe(
    data=>{
      console.log(data);
      this.useraddresses=data['user_address'];
    }
  )
  this.userService.getstates().pipe(first()).subscribe(
    data=>{
      this.getstates=data;
    }
  )
}

//set default address on click of set defalt butt
setdefault(){
  $('#changeadd').hide();
  var cartids=new Array();
  var cartdata=new Array();


  for(var i=0; i<this.ascartordersummary.length; i++){
    
    var obj={"cart_id":this.ascartordersummary[i]['cart_id'], 'address_id':this.ascartsummary['address_id']} 
    cartids.push(obj);
}

//checck checckbox checcked or not
$('.addresschecck').each(function(){
  if ($(this).prop('checked')==true) {
    cartdata=$(this).val();
  }
})

console.log(cartdata.length);
//if checcked call api
if(cartdata.length!=0){
  $('#changeadd').hide();
var form=new FormData();
form.append('userid', this.currentUser.id);
form.append('card_data', JSON.stringify(cartids));
this.userService.setdefaultadadress(form).pipe(first()).subscribe(
  data=>{
    if(data['status']=='true'){
    swal(data['message'],'','success');
    this.changeaddress();
   
    }
    else{
      swal(data['message'],'','error');
    }
  }
)
}

else{
  $('#changeadd').show();
  swal('Please select atleast one address','','error');
}

}


//delete address
deleteadadress(){
  var cartdata=new Array();
  $('.addresschecck').each(function(){
    if ($(this).prop('checked')==true) {
      cartdata=$(this).val();
    }
  })
  console.log(cartdata);
  if(cartdata.length!=0){
    var form=new FormData();
    form.append('userid', this.currentUser.id);
    form.append('address_id', cartdata.toString());
    this.userService.deleteadd(form).pipe(first()).subscribe(
      data=>{
        if(data['status']=='true'){
          $('#changeadd').hide();
        swal(data['message'],'','success');
        this.changeaddress();
      }
      else{
        $('#changeadd').show();
        swal(data['message'],'','error');
      }
    }
    )
  }
  else{
    swal('Please select address to delete','','error');
  }
}


//on select of state get cities
selectstates(statesvalue){
console.log(statesvalue);
// var form=new FormData();

// form.append('state_id', value);
this.userService.getcityincart(statesvalue).pipe(first()).subscribe(
  data=>{
    this.getcity=data;
  }
)
}

submit(ngForm){
  console.log(ngForm);
  var obj={'userid':this.currentUser.id, 'mobile':ngForm.mobile,'address_line_1':ngForm.address,'city_id':ngForm.city,'state_id':ngForm.state,'userFullname':ngForm.fname,'pincode':ngForm.pincode, 'is_default':'1'}
  var form=new FormData();
  form.append('userid', this.currentUser.id);
  form.append('address', JSON.stringify(obj));
  this.userService.addnewadd(form).pipe(first()).subscribe(
    data=>{
      if(data['status']=='true'){
        swal(data['message'],'','success');
        this.changeaddress();
      }
      else{
        swal(data['message'],'','error');
      }
      
    }
  )
}


//place ordedr
placeorder(values){
  var cartids=new Array();
  this.updatecart(values);
  //this.loadbuyercartdetail();
  for(var i=0; i<values.length; i++){
    cartids.push(values[i]['cart_id']);
  }
  console.log(cartids);
        var obj={'cart_ids':cartids,'buyer_pickup_flag':this.pickupflag,'address_id':this.ascartsummary['address_id'],'order_date':this.ascartsummary['deliver_by_date'],'total_qty':this.ascartsummary['quantity'],'total_amount':this.ascartsummary['selling_price'],'b_team_member_id':this.ascartsummary['b_team_member_id'],'s_team_member_id':this.ascartsummary['s_team_member_id']}

        var form=new FormData();
        form.append('userid', this.currentUser.id);
        form.append('order_data', JSON.stringify(obj));

        
        if(this.pickupflag==1){
        // swal({
        //   title: "Are you sure?",
        //   text: "You are picking up the order from seller shop",
        //   icon: "warning",
        //   buttons: [
        //     'No, cancel it!',
        //     'Yes, I am sure!'
        //   ],
        //   dangerMode: true,
        // }).then(function(isConfirm) {
        //   if (isConfirm) {
            
        //       this.userService.placeorderbuyside(form).pipe(first()).subscribe(
        //         data=>{
        //           console.log(data);
        //           if(data['status']=='true'){
        //           swal(data['message'],'','success')
        //         }
        //         else{
        //           swal(data['message'],'','error');
        //         }
        //       }
        //       )
            
        //   } else {
        //     swal("Cancelled", "Your imaginary file is safe :)", "error");
        //   }
        // });

        swal({
          title: "Are you sure?",
            text: "You are picking up the order from seller shop",
            icon: "warning",
            buttons: ["No, cancel it", "Yes, I am sure!"],
         
           
            roll: {
              text: "Yes, I am sure!",
              value: "true",
            },
            roll1: {
              text: "No, cancel it",
              value: "false",
            },
            
          
              }).then((value) => {
                //console.log(value);
                if (value=='true') {
                  this.userService.placeorderbuyside(form).pipe(first()).subscribe(
                            data=>{
                              console.log(data);
                              if(data['status']=='true'){
                              swal(data['message'],'','success');
                              this.router.navigate([this.returnUrl]); 
                            }
                            else{
                              swal(data['message'],'','error');
                            }
                          }
                          )
                  
                }
   
      else{
        swal("Cancelled", "", "error");
      }
    });
      }

      if(this.pickupflag==0){
        swal({
          title: "Are you sure?",
            text: "Seller is delivering the order",
            icon: "warning",
          buttons: {
           
            roll: {
              text: "Yes, I am sure!",
              value: "true",
            },
            roll1: {
              text: "No Cancel it",
              value: "false1",
            },
          
          },
              }).then((value) => {
                //console.log(value);
                if (value=='true') {
                  this.userService.placeorderbuyside(form).pipe(first()).subscribe(
                            data=>{
                              console.log(data);
                              if(data['status']=='true'){
                              swal(data['message'],'','success');
                              this.router.navigate([this.returnUrl]); 
                            }
                            else{
                              swal(data['message'],'','error');
                            }
                          }
                          )
                  
                }
   
      else{
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
      }

  

  
}



closemod(){
  $('#changeadd').hide();
}
addnewaddress(){
  this.addressform=true;

}
}
