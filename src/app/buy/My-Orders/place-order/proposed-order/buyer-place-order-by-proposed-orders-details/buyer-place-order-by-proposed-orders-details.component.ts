import { Component, OnInit,PipeTransform, Pipe   } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';
declare var $ :any;
declare var swal:any; 
@Component({
  selector: 'app-buyer-place-order-by-proposed-orders-details',
  templateUrl: './buyer-place-order-by-proposed-orders-details.component.html',
  styleUrls: ['./buyer-place-order-by-proposed-orders-details.component.css']
})
@Pipe({
  name: 'dateFormatPipe',
})
export class BuyerPlaceOrderByProposedOrdersDetailsComponent implements OnInit {
  currentUser: User;
  asproposedorderdetail: any[] = [];
  loadingnew=true;
  loading=false;
  idproposedorder:any;
  today: number = Date.now();
  constructor(private userService: UserService,private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.queryParams.subscribe(params => {
      this.idproposedorder=params['user']
      });
      console.log(this.idproposedorder); }

  ngOnInit() {
    this.loadbuyplaceorderproposedorder();
  }

  loadbuyplaceorderproposedorder() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('proposed_order_id',this.idproposedorder);
    this.userService.getbuyplaceorderbuproposedorderdetails(form).pipe(first()).subscribe(result => { 
     
        this.asproposedorderdetail= result['order']; 
        this.loadingnew=false;

        //console.log(result);
        
    });
  }

}
