import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerPlaceOrderByProposedOrdersDetailsComponent } from './buyer-place-order-by-proposed-orders-details.component';

describe('BuyerPlaceOrderByProposedOrdersDetailsComponent', () => {
  let component: BuyerPlaceOrderByProposedOrdersDetailsComponent;
  let fixture: ComponentFixture<BuyerPlaceOrderByProposedOrdersDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerPlaceOrderByProposedOrdersDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerPlaceOrderByProposedOrdersDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
