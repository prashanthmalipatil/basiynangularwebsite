import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerPlaceOrderByProposedOrdersComponent } from './buyer-place-order-by-proposed-orders.component';

describe('BuyerPlaceOrderByProposedOrdersComponent', () => {
  let component: BuyerPlaceOrderByProposedOrdersComponent;
  let fixture: ComponentFixture<BuyerPlaceOrderByProposedOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerPlaceOrderByProposedOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerPlaceOrderByProposedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
