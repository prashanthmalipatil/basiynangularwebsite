import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $ :any;
@Component({
  selector: 'app-buyer-place-order-by-proposed-orders',
  templateUrl: './buyer-place-order-by-proposed-orders.component.html',
  styleUrls: ['./buyer-place-order-by-proposed-orders.component.css']
})
export class BuyerPlaceOrderByProposedOrdersComponent implements OnInit {
  currentUser: User;
  asproposedorder: any[] = [];
  loadingnew=true;
  loading=false;
  
  constructor(private userService: UserService, private formBuilder:FormBuilder, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); }

  ngOnInit() {
    this.loadplaceorderproposedorder();
  }

  loadplaceorderproposedorder() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    //form.append('value',this.sellerid)

    this.userService.getplaceorderproposedorder(form).pipe(first()).subscribe(result => { 
        
        this.asproposedorder=result['order'];
        
        this.loadingnew=false;

        //console.log(result);
        
    });
  }
  buyplaceorderproposedorder(user){
    console.log(user);
//this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
let navigationExtras: NavigationExtras = {
   queryParams: {
   'user':user
 
   }
   };
this.router.navigate(['/buyer-place-order-by-proposed-orders-details/'],navigationExtras);
 }
}
