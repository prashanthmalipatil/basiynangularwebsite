import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerBulkPlaceOrderComponent } from './buyer-bulk-place-order.component';

describe('BuyerBulkPlaceOrderComponent', () => {
  let component: BuyerBulkPlaceOrderComponent;
  let fixture: ComponentFixture<BuyerBulkPlaceOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerBulkPlaceOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerBulkPlaceOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
