import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $ :any;
@Component({
  selector: 'app-buyer-place-order-by-seller',
  templateUrl: './buyer-place-order-by-seller.component.html',
  styleUrls: ['./buyer-place-order-by-seller.component.css']
})
export class BuyerPlaceOrderBySellerComponent implements OnInit {
  currentUser: User;
  asbyseller: any[] = [];
  loadingnew=true;
  sellerid:any;
  Searchseller:FormGroup;
  loading=true;
  submitted=false;

  constructor(private userService: UserService, private formBuilder:FormBuilder, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));}

  ngOnInit() {
    $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
 $(".clear").hide();
 this.Searchseller = this.formBuilder.group({
  seller_name: ['', Validators.required],
  
});
this.loadplaceorderbyseller();
  }
  get f() { return this.Searchseller.controls; }

  onSubmitsearch() {
    // console.log("came to submit");
    this.submitted = true;
     
    // // stop here if form is invalid
     if (this.Searchseller.invalid) {
        
         console.log('false data');
         return;
     }
     this.loading = true;
      
       var form = new FormData();
      
       form.append('userid', this.currentUser.id);
       form.append('sellername', this.Searchseller.value.seller_name);
        console.log(form);
       this.userService.searchplaceorderbysellernames(form)
       .pipe(first())
       .subscribe(
        data => {
                  
          this.asbyseller=data['sellers'];
       
          });

  }
  clearsearch(){
    $(".clear").hide();
   
   this.Searchseller.reset();
    this.loadplaceorderbyseller();
   }
   loadplaceorderbyseller() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    //form.append('value',this.sellerid)

    this.userService.getplaceorderbyseller(form).pipe(first()).subscribe(result => { 
      
        this.asbyseller= result['sellers']; 
        this.loadingnew=false;

        //console.log(result);
        
    });
  }
}
