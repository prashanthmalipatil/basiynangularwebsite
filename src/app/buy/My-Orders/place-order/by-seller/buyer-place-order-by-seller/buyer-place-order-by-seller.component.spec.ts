import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerPlaceOrderBySellerComponent } from './buyer-place-order-by-seller.component';

describe('BuyerPlaceOrderBySellerComponent', () => {
  let component: BuyerPlaceOrderBySellerComponent;
  let fixture: ComponentFixture<BuyerPlaceOrderBySellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerPlaceOrderBySellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerPlaceOrderBySellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
