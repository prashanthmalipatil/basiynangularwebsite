import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainplaceorderPageComponent } from './mainplaceorder-page.component';

describe('MainplaceorderPageComponent', () => {
  let component: MainplaceorderPageComponent;
  let fixture: ComponentFixture<MainplaceorderPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainplaceorderPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainplaceorderPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
