import { Component, OnInit, PipeTransform, Pipe  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { result } from 'underscore';
import { DateAdapter } from '@angular/material';

declare var $ :any;
declare var swal:any;
@Component({
  selector: 'app-buyer-place-order-single-product-new',
  templateUrl: './buyer-place-order-single-product-new.component.html',
  styleUrls: ['./buyer-place-order-single-product-new.component.css']
})
export class BuyerPlaceOrderSingleProductNewComponent implements OnInit {
  currentUser: User;
  asorderdetail: any[] = [];
  asordervarientsdetail:any[]=[];
  asaddressid:any[]=[];
  asbuyerpickupflag:any[]=[];
  idproductmatch:any;
  businesslayout:any;
  loadingnew=true;
  loading=false;
  var1value:any;
var2value:any;
varmatchid:any[]=[];
selectmatchid:any[]=[];
catdetails:any;
addid:any;
selectedValue:any;
  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService, private dateAdapter: DateAdapter<Date>){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.queryParams.subscribe(params => {
      this.idproductmatch=params['master_product_id']
     
      });
    
      dateAdapter.setLocale('en-in'); // DD/MM/YYYY
  }

  ngOnInit() {
    
    this.loadplaceorderdetailpage();
    this.loadplaceorderdetailpagevarients();
    this.var1value=null;

  
  }

  loadplaceorderdetailpage() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('master_product_id',this.idproductmatch);
   

    this.userService.getplaceorderdetailpage(form).pipe(first()).subscribe(result => { 
        this.asaddressid=result['address_id'];
        this.asbuyerpickupflag=result['buyer_pickup_flag'];
      this.asorderdetail=result['products'];

        
        this.loadingnew=false;
        
        //console.log(result);
        
    });
  }

  loadplaceorderdetailpagevarients() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('master_product_id',this.idproductmatch);
   

    this.userService.getplaceorderdetailpagevarients(form).pipe(first()).subscribe(result => { 
        this.asordervarientsdetail=result['variant_1'];
        this.selectedValue=result['variant_1'][0]['id']
        
        this.loadingnew=false;
        
        //console.log(result);
        
    });
  }


   //userlayout on selecct dropdown call api
   handleSelectedValue(user, value){ 
    this.var1value=$('#selvalue').val();
    this.varmatchid=user.product_id;
    this.callapi();
  }

  handleSelectedValue2(user, value){
    this.var2value=value;
    this.callapi();
  }

  //onchange call api
  callapi(){
     if(this.var2value==null){
      this.var2value='';
    }
    if(this.var1value==null){
      this.var1value=''
    }
    this.userService.changevalues(this.idproductmatch, this.selectedValue, this.currentUser.id, this.var2value).pipe(first()).subscribe(
      data=>{
        console.log(data);
        if(data['variant'].length!=0){
        this.selectmatchid=data['variant'][0]['match_master_id'];
      }
    }
    )
  }



   //add to cart
   addtocart(user){
    console.log(user);
     //date conversion
 function convert(str) {
   var date = new Date(str),
       mnth = ("0" + (date.getMonth()+1)).slice(-2),
       day  = ("0" + date.getDate()).slice(-2);
   return [ day, mnth, date.getFullYear() ].join("-");
 }
 var newdate= $("#newdate").val();
 var convdate=convert(newdate);
 
 console.log(convdate);
  

//console.log(typeof convdate);
 if(typeof convdate=='string'){
console.log('if')
 var setdate=convdate;
 console.log(setdate);
 }
 else{
  console.log('else');
   setdate=newdate;
   console.log(setdate);
 }
 
 
     var proddetails=new Array();
    if(user.cartaction==1){
     $('.hiddenvariant').each(function(){
       if ($(this).val()!=null) {  
         this.variantvalue=$(this).attr('value');
         this.varid=$(this).attr('id');  
         if(this.variantvalue!=''){
         var obj={"match_master_id":this.varid, "qty": this.variantvalue,'date':setdate}
           proddetails.push(obj);
         }
       }
       //console.log(proddetails)
       
     });
   }

   if(user.cartaction==0){
     console.log(this.var1value);
     var matchids=new Array();
   
     if(user.variant_1.length==0){
     
       matchids=user.match_master_id;
       console.log(matchids +'match ids')
     }
       else if(this.var1value==null){
        matchids=user.match_master_id;
       }
      else{
         matchids=this.selectmatchid;
         console.log(matchids);
       }
    //  if(this.var1value.length==0){
    //    swal('please select variants', '', 'error')
    //  }
    
   
     $('.qtyinput').each(function(){
 
       this.qtyvalue=$(this).attr('value');
 
       if(matchids==null){
         swal('Please select variants', '', 'error');
         console.log(this.qtyvalue);
       }
       
       else if(this.qtyvalue==''){
         swal('Quantity cannot be empty', '', 'error');
       }
       else{
 
         var obj={"match_master_id":matchids, "qty": this.qtyvalue,'deliver_date':setdate}
         proddetails.push(obj);
         
       }
     
      
     });
   }
 //end add to cart
 
 
 
 
 
 if(proddetails.length!=0){
 var form=new FormData();
 form.append('userid', this.currentUser.id);
 form.append('buyer_pickup_flag', this.asbuyerpickupflag.toString());
 form.append('address_id', this.asaddressid.toString());
 form.append('cartdetail', JSON.stringify(proddetails));
 
 
 this.userService.addtocart(form).pipe(first()).subscribe(
   data=>{
     if(data['status']==true){
     swal(data['msg'], '', 'success');
     this.loadplaceorderdetailpagevarients();
   }
 
 else{
   swal(data['msg'], '', 'error');
 }
   }
 )
 }
   }
}
