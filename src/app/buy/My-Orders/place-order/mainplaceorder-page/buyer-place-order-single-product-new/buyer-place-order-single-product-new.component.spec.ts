import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerPlaceOrderSingleProductNewComponent } from './buyer-place-order-single-product-new.component';

describe('BuyerPlaceOrderSingleProductNewComponent', () => {
  let component: BuyerPlaceOrderSingleProductNewComponent;
  let fixture: ComponentFixture<BuyerPlaceOrderSingleProductNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerPlaceOrderSingleProductNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerPlaceOrderSingleProductNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
