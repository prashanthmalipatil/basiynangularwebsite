import { Component, OnInit, NgModule, Pipe } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { UserService, PagerService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { first, min } from 'rxjs/operators';
import { MAT_DATE_LOCALE, DateAdapter } from '@angular/material';
import { DatePipe } from '@angular/common';

declare var $:any;
declare var swal:any;
@Component({
  selector: 'app-mainplaceorder-page',
  templateUrl: './mainplaceorder-page.component.html',
  styleUrls: ['./mainplaceorder-page.component.css', './slidercss.component.css']
})
@Pipe({
  name: 'dateFormat'
})
export class MainplaceorderPageComponent implements OnInit {
  level:any;
  category_id:any;
  currentUser:User;
  productdata:any;
  booknotseltext:any;
  bookselected:any;
  variantvalue:any[]=[];
  varid:any[]=[];
  catdetails:any;
  addid:any;
  masterproductid:any[]=[];
  businesslayout:any[]=[]; 
  selectmatchid:any;
  var1value:any;
  var2value:any;
  varmatchid:any;
  qtyvalue:any;
  loadingnew=true;
  showtxt=false;
  total: number;
  p: number = 1;
  per_page:any;
  l1categories:any;
  level1clickdata:any=[];
  level2clickdata:any=[];
  level3clickdata:any=[];
  mycat:any[]=[];
  checkedid:any=[];
  clearcategorytxt=false;
  mycatfiltertxt=false;
  clrsell=false;
  sellerdata:any[]=[];
  sellercheckid:any[]=[];
  pricedata:any[]=[];
  minValue:any=[];
  maxValue:any=[];
  newslidval:any[]=[];
  range1:any[]=[];
  range2:any[]=[];
  range:any[]=[];
  clrprice=false;
  favour:any[]=[];
  favdata:any[]=[];
  clrfav=false;
  brandsdata:any[]=[];
  brandchecckvalue:any[]=[];
  clrfavbrand=false;
  Searchvalue:any[]=[];
  constructor(private route: ActivatedRoute,private pagerService: PagerService, private router: Router, private userService: UserService, private dateAdapter: DateAdapter<Date>) { 
    this.route.queryParams.subscribe(params => {
      this.level=params['level']
      this.category_id=params['catlevel']

      
     });
     dateAdapter.setLocale('en-in'); // DD/MM/YYYY
    
     this.currentUser=JSON.parse(localStorage.getItem('currentUser'));
     console.log(this.pricedata['min']);
     console.log(this.pricedata['max']) 
     
  }


  //Pagination
  allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];


  ngOnInit() {
    
    $(".clear").hide();
    this.loadbuysideproducts();
    this.Getcategories();
    this.setPage(0);

 
   
    
  }

//on page load

  loadbuysideproducts(page: number = 0){
    var form=new FormData();
    form.append('userid', this.currentUser.id);
    if(this.level!=null && this.category_id!=null){
    form.append('level', this.level);
    form.append('category', this.category_id);
    }
    this.userService.loadproductsbuyside(form, page).pipe(first()).subscribe(
      data=>{
        this.loadingnew=false;
        
        this.productdata=data['products'];
        this.catdetails=data['buyer_pickup_flag'];
        this.addid=data['address_id'];
        this.total = data['nopages']*12;
        this.per_page=12;
        this.pager = this.pagerService.getPager(this.total, page,this.per_page);
       
      }
    )
  }

  //pagination
  setPage(page: number) {

    if (page < 1 || page > this.pager.total) {
        return;
    }
    this.loadbuysideproducts(page);
  
  }

  productover(product){
   // console.log('hello');
    var id = product
    //console.log(id);
    $(".productdiv").each(function(){
       $(this).removeClass('animate');
    })
     $("#product-card"+id).addClass('animate');
  }

  //bokmark code 
  //not selected
  booknotsel(id){
    this.booknotseltext=[];
    this.bookselected=[];
    this.booknotseltext=id;
   
    this.bookmark();
  }

  //selected
  booksel(id){
    this.booknotseltext=[];
    this.bookselected=[];
    this.bookselected=id;
   
    this.bookmark();
  }


  //bookmark api call
  bookmark(){ 
    var form= new FormData();
    form.append('userid', this.currentUser.id);
    
    if( this.booknotseltext!=''){
      form.append('master_product_id', this.booknotseltext);
      form.append('set', '1');
    }

    if(this.bookselected!=''){
      form.append('master_product_id', this.bookselected);
      form.append('set', '0');
    }
    this.userService.bookmarkbuyside(form).pipe(first()).subscribe(
      data=>{
          //console.log(data);
          this.loadbuysideproducts();
      }
    )
  }






  //add to cart
  addtocart(user){
   console.log(user);
    //date conversion
function convert(str) {
  var date = new Date(str),
      mnth = ("0" + (date.getMonth()+1)).slice(-2),
      day  = ("0" + date.getDate()).slice(-2);
  return [ day, mnth, date.getFullYear() ].join("-");
}
var newdate= $("#newdate"+user.master_product_id).val();;
var convdate=convert(newdate);


// if(convdate=='aN-aN-NaN'){
// //console.log(newdate);
// var setdate=newdate;
// }
// else{
 
//   setdate=convdate;
// }
if(typeof convdate=='string'){
  console.log('if')
   var setdate=convdate;
   console.log(setdate);
   }
   else{
    console.log('else');
     setdate=newdate;
     console.log(setdate);
   }


    var proddetails=new Array();
   if(user.cartaction==1){
    $('.hiddenvariant'+user.master_product_id).each(function(){
      if ($(this).val()!=null) {  
        this.variantvalue=$(this).attr('value');
        this.varid=$(this).attr('id');  
        if(this.variantvalue!=''){
        var obj={"match_master_id":this.varid, "qty": this.variantvalue,'date':setdate}
          proddetails.push(obj);
        }
      }
      //console.log(proddetails)
      
    });
  }
 // console.log('.qty'+user.master_product_id);
  if(user.cartaction==0){
   
    console.log(matchids);
    console.log(user.variant_1);
    var matchids=new Array();
    

    if(user.variant_1.length==0){
    console.log('came to if');
      matchids=user.match_master_id;
    }
   
    else{
   
      matchids=this.selectmatchid;
    }
   
  
    $('.qty'+user.master_product_id).each(function(){
      this.qtyvalue=$(this).attr('value');

      if(matchids==null){
        swal('Please select variants', '', 'error');
        console.log(this.qtyvalue);
        matchids=user.match_master_id;
      }
      
      if(this.qtyvalue==''){
        swal('Quantity cannot be empty', '', 'error');
      }
      else{
        console.log(matchids);
        var obj={"match_master_id":matchids, "qty": this.qtyvalue,'deliver_date':setdate}
        proddetails.push(obj);
        
      }
    console.log(obj);
     
    });
  }
//end add to cart





if(proddetails.length!=0 || this.varid!=null){
var form=new FormData();
form.append('userid', this.currentUser.id);
form.append('buyer_pickup_flag', this.catdetails);
form.append('address_id', this.addid);
form.append('cartdetail', JSON.stringify(proddetails));


this.userService.addtocart(form).pipe(first()).subscribe(
  data=>{
    if(data['status']==true){
    swal(data['msg'], '', 'success');
    this.loadbuysideproducts();
  }

else{
  swal(data['msg'], '', 'error');
}
  }
)
}
  }


  //userlayout on selecct dropdown call api
  handleSelectedValue(user, value){ 
    this.var1value=value;
    this.varmatchid=user.product_id;
    this.callapi();
  }

  handleSelectedValue2(user, value){
    this.var2value=value;
    this.callapi();
  }

  //onchange call api
  callapi(){
     if(this.var2value==null){
      this.var2value='';
    }
    if(this.var1value==null){
      this.var1value=''
    }
    this.userService.changevalues(this.varmatchid, this.var1value, this.currentUser.id, this.var2value).pipe(first()).subscribe(
      data=>{
        console.log(data);
        if(data['variant'].length!=0){
        this.selectmatchid=data['variant'][0]['match_master_id'];
      }
    }
    )
  }

  orderdetailpage(product){
    console.log(product);
    this.masterproductid=product.master_product_id;
    console.log(this.masterproductid);
   this.businesslayout=product.business_layout;
   console.log(this.businesslayout);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        'master_product_id':product.master_product_id,
        
  
      }
      };
      this.router.navigate(['/buyer-place-order-single-product-new/'],navigationExtras);
  }

  //Search
  searchForm(ngForm){
    console.log(ngForm.search);
    $(".clear").show();

    this.Searchvalue=ngForm.search;
    this.Filtercategories();
    this.Getcategories();
    // var form=new FormData();
    
    // form.append('type','filterList');
    // form.append('pname', ngForm.search);
    // form.append('userid', this.currentUser.id);

    // this.userService.searchbuypro(form).pipe(first()).subscribe(
    //   data=>{
    //     this.productdata=data['products'];
    //     if(data['status']='false'){
    //       this.showtxt=true;
    //      // console.log('true');
    //     }
    //     else{
    //       this.showtxt=false;
    //       //console.log('false')
    //     }
       
    //   }
    // )
  }

  clearsearch(){
    $(".clear").hide();
    $("#mysearch").val('');
    // this.loadbuysideproducts();
    this.Filtercategories();
    this.Getcategories();
   }




   //Call get catagories to fetech all categories

   Getcategories(){
    this.minValue=[];
    this.maxValue=[];
     var form=new FormData();
     if(this.level1clickdata.length!= 0 ){
      //console.log('came to first')
     form.append('category_name', this.level1clickdata);
     form.append('level','1')
   }
   if(this.level2clickdata.length!= 0){
     //console.log('came to second')
     form.append('category_name', this.level2clickdata);
     form.append('level','2')
   }
   if(this.level3clickdata.length!= 0){
    // console.log('came to 3rd')
     form.append('category_name', this.level3clickdata);
     form.append('level','3')
   }
   if(this.checkedid.length!=0){
     form.append('mycat', this.checkedid);
   }
   if(this.sellercheckid.length!=0){
    form.append('seller', this.sellercheckid.toString());
  }

  if(this.range.length!=0){
    form.append('price', this.range.toString());
  }
  if(this.favdata.length!=0){
    form.append('IsFavorite', this.favdata.toString());
  }
  if(this.brandchecckvalue.length!=0){
    form.append('brands', this.brandchecckvalue.toString());
  }

  if(this.Searchvalue.length!=0){
    form.append('pname', this.Searchvalue.toString());
  }
     form.append('userid', this.currentUser.id);
     this.userService.loadcategoriesbuy(form).pipe(first()).subscribe(
       data=>{
        // console.log(data);
         this.l1categories=data['categories'];
         this.mycat=data['mycategory'];
         this.sellerdata=data['sellers'];
         this.pricedata=data['price'];
         this.newslidval=data['price'];
         this.favour=data['favor'];
         this.brandsdata=data['brands'];

       //  console.log(this.brandsdata);
      

        this.minValue=parseInt(this.newslidval['min']);
        this.maxValue=parseInt(this.newslidval['max']);
        console.log(this.minValue);
        console.log(this.maxValue);
        this.slider();
         //console.log(this.pricedata['min']);
       }
     )
   }

//filter categores based on option selected
   Filtercategories(){
     var form=new FormData();

     if(this.level1clickdata.length!= 0 ){
           // console.log('came to first')
            form.append('category_name', this.level1clickdata);
            form.append('level','1')
          }
          if(this.level2clickdata.length!= 0){
            //console.log('came to second')
            form.append('category_name', this.level2clickdata);
            form.append('level','2')
          }
          if(this.level3clickdata.length!= 0){
           // console.log('came to 3rd')
            form.append('category_name', this.level3clickdata);
            form.append('level','3')
          }
          if(this.checkedid.length!=0){
            form.append('mycat', this.checkedid);
          }
          if(this.sellercheckid.length!=0){
            form.append('seller', this.sellercheckid.toString());
          }
          if(this.range.length!=0){
            form.append('price', this.range.toString());
          }
          if(this.favdata.length!=0){
            form.append('IsFavorite', this.favdata.toString());
          }

          if(this.brandchecckvalue.length!=0){
            form.append('brands', this.brandchecckvalue.toString());
          }

          if(this.Searchvalue.length!=0){
            form.append('pname', this.Searchvalue.toString());
          }

     form.append('userid', this.currentUser.id);
     this.userService.catfilt(form).pipe(first()).subscribe(
       data=>{
        this.productdata=data['products'];
        this.clearcategorytxt=true;
       }
     )
   }



      //on catclick
   level1click(user){
     //console.log(user);
     this.level2clickdata=[];
     this.level3clickdata=[];
     this.level1clickdata=user.name;
     this.Filtercategories();
     this.Getcategories();
     this.mycatfiltertxt=true;

   }
   level2click(user1){
    this.level1clickdata=[];
    this.level3clickdata=[];
    this.level2clickdata=user1.name;
    this.Getcategories();
    this.Filtercategories();
    this.mycatfiltertxt=true;

  }
  level3click(user2){
    
    this.level1clickdata=[];
    this.level2clickdata=[];
    //console.log(user);
    this.level3clickdata=user2.name;
    this.mycatfiltertxt=true;
    this.Filtercategories();
    this.Getcategories();
  }

//Clear category
clearcategory(){
  this.clearcategorytxt=true;
  this.level1clickdata=[];
  this.level2clickdata=[];
  this.level3clickdata=[];
  this.Filtercategories();
  this.Getcategories();
  
}


//my ctegory filters

mycatfilter(){
  var invite_ids=new Array();
      $('.catcheck').each(function(){
          if ($(this).prop('checked')) {
            //console.log($(this).val());
            var newdata=$(this).val();
            // this.checkedid=$(this).val();
            invite_ids.push(newdata);
            
            
       }
      })
      this.mycatfiltertxt=true;
     
    this.checkedid=invite_ids;
    this.Filtercategories();
    this.Getcategories();
}


//clear my category
clearmycategory(){
  this.checkedid=[];
  this.mycatfiltertxt=false;
  this.Getcategories();
  this.Filtercategories();
}


//seller filter
sellerfilter(){
  $("#sellers").hide();
  var invite_ids = new Array();
    
    $('.sellercheck').each(function(){
      if ($(this).prop('checked')) {
        //console.log($(this).val());
        var newdata=$(this).val();
        // this.checkedid=$(this).val();
        invite_ids.push(newdata);
        
        
   }
  })
  this.sellercheckid=invite_ids;
  this.Filtercategories();
    this.Getcategories();
    this.clrsell=true;
}

clearseller(){
  this.sellercheckid=[];
this.clrsell=false;
this.Filtercategories();
    this.Getcategories();
}


//price range filter

pricefilter(){

this.range1= $("#low").attr('value');
this.range2=$("#high").attr('value');
this.range.push(this.range1, this.range2);
this.Filtercategories();
this.Getcategories();
$("#price").hide();
 
}

clearprice(){
  this.clrprice=false;
  this.range=[];
  this.Filtercategories();
    this.Getcategories();
}


favclick(fav){
//console.log(fav);
this.favdata=fav;
this.Filtercategories();
this.Getcategories();
this.clrfav=true;
}


//clear fav
clearfav(){
  this.clrfav=false;
  this.favdata=[];
  this.Filtercategories();
this.Getcategories();
}


//brand filter
brandfilter(brand){
 // console.log(brand);
$("#brand").show();
var brandvalue=new Array();
$('.bandcheckdata').each(function(){
  if ($(this).prop('checked')) {
      var invite_id = $(this).val(); 
      brandvalue.push(invite_id);
    
  }

});
this.clrfavbrand=true;
//console.log(brandvalue);
this.brandchecckvalue=brandvalue;
this.Filtercategories();
this.Getcategories();
$("#brandlast").hide();
}



clearbrandslast(){
  this.brandchecckvalue=[];
  this.clrfavbrand=false;
  this.Filtercategories();
this.Getcategories();
}


mycatpopup(){
  $("#mycat").show();
}
filterseller(){
  $("#sellers").show();
}

pripopup(){
  
  $("#price").show();

}

brandmod(){

  $("#brandlast").show();
}

clodemodal(){
      $("#mycat").hide();
      $("#sellers").hide();
      $("#price").hide();
      $("#brandlast").hide();
     }


     slider(){
      this.clrprice=true;
       console.log(this.minValue);
       console.log(this.maxValue);
       var newnumber=Number;
       newnumber=this.maxValue;

       var lowl=new Array();
       var high1=new Array();
      $(function() {
        lowl=new Array();
        high1=new Array();
        $( "#slider-6" ).slider({
           range:true,
           min: 0,
           max: newnumber,
           values: [ 0, newnumber ],
           start: function( event, ui ) {
              $( "#startvalue" )
                 .val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
           },
           stop: function( event, ui ) {
           
              $( "#stopvalue" )
                 .val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
                 lowl.push(ui.values[0]);
                 high1.push(ui.values[1]);
                 $("#low").val(ui.values[0]);
                 $("#high").val(ui.values[1]);
                 
           },
           change: function( event, ui ) {
              $( "#changevalue" )
                 .val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
           },
           slide: function( event, ui ) {
              $( "#slidevalue" )
                 .val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
                
                 
                
           }
           
       });
     
      
     });
     }

}
