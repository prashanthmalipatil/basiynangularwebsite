import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerPlaceOrderByProductNewComponent } from './buyer-place-order-by-product-new.component';

describe('BuyerPlaceOrderByProductNewComponent', () => {
  let component: BuyerPlaceOrderByProductNewComponent;
  let fixture: ComponentFixture<BuyerPlaceOrderByProductNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerPlaceOrderByProductNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerPlaceOrderByProductNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
