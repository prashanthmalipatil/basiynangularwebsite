import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $ :any;
@Component({
  selector: 'app-buyer-place-order-by-product-new',
  templateUrl: './buyer-place-order-by-product-new.component.html',
  styleUrls: ['./buyer-place-order-by-product-new.component.css']
})
export class BuyerPlaceOrderByProductNewComponent implements OnInit {
  currentUser: User;
  asbasiyncatids: any[] = [];
  asbrands:any[]=[];
  ascategories:any[]=[];
  asfavorites:any[]=[];
  asmycategories:any[]=[];
  assellers:any[]=[];
  loadingnew=true;
  loding=false;
  catlength:any[]=[];
level2:any[]=[];


level3:any[]=[];
level3cat:any[]=[];
//level2catlength:any[]=[];
  constructor(private userService: UserService, private formBuilder:FormBuilder, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));}

  ngOnInit() {
    this.loadplaceorderbyproducts();
    // $('.menu').hover( function() {
    //   $(this).siblings('.desc').toggle();
    // } );
    //this.level1click(user);



// $(".clicklevel1").on('click',function(){
//   alert('hi hello');
//   var rowid=$(this).prop('id');
//   console.log(rowid);
//   $('.boxlink').hide().filter('#box' + rowid).show();
// });
  }

 loadplaceorderbyproducts() {

    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    //form.append('value',this.sellerid)

    this.userService.getplaceorderbyproducts(form).pipe(first()).subscribe(result => { 
        
        this.asbasiyncatids=result['basiyn_cat_ids'];
        this.asbrands=result['brands'];
        this.ascategories=result['categories'];
        this.asfavorites=result['favourites'];
        this.asmycategories=result['my_categories'];
        this.assellers= result['sellers']; 
        this.loadingnew=false;
        
        for(var j=0; j<this.ascategories.length; j++){
            this.catlength.push(this.ascategories[j]);
        }

        // console.log(this.catlength);
        // console.log(this.catlength.length);
        for(var i=0; i<this.catlength.length; i++){
          this.level2.push(result['categories'][i]['level2_categories']);

        }
        console.log(this.level2);
     
        
    });
  }

  level1click(user){
    
      $('.subbox').hide().filter('#subbox' + user.id).show();
      $(".clicklevel").mouseover(function(){
        //alert('hi')
        $('.level3box').hide();
      });

      
  }

  level2click(level2){
   
    $('.level3box').hide().filter('#level3box' + level2.id).show();

    
  }

  //level1 cat on click
  level1onclick(user){
    console.log(user);
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "level":'1',
          "catlevel":user.id,
      }
  };
  this.router.navigate(['/buyer-place-order-new'], navigationExtras);
  }

   //level2 cat on click
   level2onclick(level2){
    console.log(level2);
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "level":'2',
          "catlevel":level2.id,
      }
  };
  this.router.navigate(['/buyer-place-order-new'], navigationExtras);
  }

   //level3 cat on click
   level3onclick(level3){
    console.log(level3);
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "level":'3',
          "catlevel":level3.id,
      }
  };
  this.router.navigate(['/buyer-place-order-new'], navigationExtras);
  }
}
