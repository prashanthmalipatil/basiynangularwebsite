import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService, PagerService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

declare var $ :any;
declare var swal:any; 
@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {
  currentUser: User;
  asorderhistory: any[] = [];
  loadingnew=true;
  ordersummaryid: any[]=[];
  Searchseller: FormGroup;
  Filterbydates:FormGroup;
    from:any;
    to:any;
    submitted = false;
    loading=false;
    searchseller:any[]=[];
    ids:any[]=[];
    exportdatacon:any[]=[];
    filterbytwodate:any;
    searchdata:any;
    filterbydd:any;
    selectedvalue:any;
    total: number;
    p: number = 1;
    custname=true;
    csvdata:any[]=[];
    filename:any;
    per_page:any;
  constructor(private userService: UserService,private pagerService:PagerService, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); }

  ngOnInit() {
    $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
 $(".clear").hide();
  $("#refresh").hide();
  
  //     this.Searchseller = this.formBuilder.group({
  //     seller_name: ['', Validators.required],
      
  // });
  // this.Filterbydates = this.formBuilder.group({
  //   fromdate: ['', Validators.required],
  //   todate:['',Validators.required]});

  
  
  this.checkvalue();

  

  }
  // dateLessThan(from: string, to: string) {
  //   console.log(this.Filterbydates.value);
  //   return (Filterbydates: FormGroup): {[key: string]: any} => {
  //   let f=  this.Filterbydates['fromdate'];
  //   let t=this.Filterbydates['todate'];
  //   if (f.value > t.value) {
  //   return {
  //   dates: "Date from should be less than Date to"
  //   };
  //   }
  //   return {};
  //   }
  //   }
  // get f() { return this.Searchseller.controls; }
  // get g(){return this.Filterbydates.controls;}
  


  // onSubmitsearch() {
  //   // console.log("came to submit");
  //   this.submitted = true;
     
  //   // // stop here if form is invalid
  //    if (this.Searchseller.invalid) {
        
  //        console.log('false data');
  //        return;
  //    }
  //    this.loading = true;
      
  //      var form = new FormData();
      
  //      form.append('userid', this.currentUser.id);
  //      form.append('seller_name', this.Searchseller.value.seller_name);
  //       console.log(form);
  //      this.userService.searchsellernames(form, this.Searchseller.value.seller_name)
  //      .pipe(first())
  //      .subscribe(
  //       data => {
                  
  //         this.asorderhistory=data['order'];
       
  //         });

  // }
  allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];


  checkvalue(page: number = 1){
    //this.selectedvalue=$( "#group" ).val();
    //this.filterbydd=$("#group-by").val();
    this.from=$("#new1").val();
    this.to=$("#new2").val();
    this.searchdata=$("#mysearch").val();
    console.log(this.from);
    console.log(this.to);
    var form=new FormData();
    
    
  
    if(this.from!=null && this.to!=null){
      
     
      form.append('from', this.from);
      form.append('to', this.to);
    }
  
    
  
    if(this.searchdata!=null){
      form.append('sname', this.searchdata);
    }
    form.append('userid', this.currentUser.id);
    this.userService.getorderhistory(form).pipe(first()).subscribe(
      data=>{
        //this.asprocessorder= data['order']; 
        this.asorderhistory= data['order']; 
        
  //       this.loadingnew=false;
        this.loadingnew=false;
        this.total = data['products_count'];
        this.per_page=20;
         this.pager = this.pagerService.getPager(this.total, page,this.per_page);
      }
    )
  }

  
  Searchbar(){
    this.checkvalue();
    
  }
  searchbydates(){
    

    if($("#new1").val()=='' || $("#new2").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new1").val()>$("#new2").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new1").val()<=$("#new2").val()){
      this.checkvalue();
    $("#refresh").show();
    }

    
    //$('#refresh').css("display", "block");
  }
  
  handleSelectedValue(value){
    this.custname=false;
    this.selectedvalue=$( "#group" ).val();
    this.checkvalue();
    
  }
  
  
  clearsearch(){
    $(".clear").hide();
    $("#mysearch").val('');
    this.checkvalue();
   }
  
   cleardates(){
     $("#refresh").hide();
     $("#new1").val('');
     $("#new2").val('');
     //this.from=[];
     //this.to=[];
     this.checkvalue();
    
   }
  
   
  // clearsearch(){
  //   $(".clear").hide();
  //   this.asorderhistory=[];
  //  this.Searchseller.reset();
  //   this.loadorderhistory();
  //  }

  // loadorderhistory() {
    
  //   var form = new FormData();
  //   form.append('userid', this.currentUser.id);

  //   this.userService.getorderhistory(form).pipe(first()).subscribe(result => { 
     
  //       this.asorderhistory= result['order']; 
        
  //       this.loadingnew=false;
  //       //console.log(result);
        
  //   });
  // }

  ordersummary(user){
    console.log(user);
    this.ordersummaryid=user.id_order_summary;
    console.log(this.ordersummaryid);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
      "id_order_summary":user.id_order_summary
    
      }
      };
      this.router.navigate(['/buyer-order-summary-products/'],navigationExtras);
  }
 

  closemod(){
    $('#export').hide();
  }

  exportorders(){
    $('#export').show();

  }
  datesub(ngForm){
    if($("#new3").val()=='' || $("#new4").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new3").val()>$("#new4").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new3").val()<=$("#new4").val()){

      var form=new FormData();
      form.append('from', ngForm.from);
      form.append('to', ngForm.to);
      form.append('userid', this.currentUser.id)
      this.userService.buyerordersumm(form).pipe(first()).subscribe(
        data=>{
          this.exportdatacon=data['order'];
          this.filename=data['filename']
        var exportdata1=this.exportdatacon.length;
        for (let i=0; i<exportdata1; i++){
          var datajs={"Order No": this.exportdatacon[i]['order_no'], "Product Id": this.exportdatacon[i]['product_id'], "Product Name": this.exportdatacon[i]['product_name'], "Seller Id":this.exportdatacon[i]['seller_id'],  "Seller Name":this.exportdatacon[i]['seller_nickname'] , "Cost":this.exportdatacon[i]['rate'], "order Date": this.exportdatacon[i]['order_date'], "Deliver by Date": this.exportdatacon[i]['deliver_by_date'], 'Order Quantity':this.exportdatacon[i]['product_level_quantity']  }
            this.csvdata.push(datajs);
           }

        var options = {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: true,
          useBom: true,
          noDownload: false,
          headers: ["Order No", "Product Id", "Product Name", "Seller Id", "Seller Name", "Cost","order Date", "Deliver by Date", "Order Quantity"]
        };
      
      new Angular5Csv(this.csvdata, this.filename, options);
      }
        
      )
    console.log(ngForm);
  }
}
    
  // onSubmitdates(){
  //   //console.log(this.g.value)
  //   this.from=this.g.fromdate.value;
  //   this.to=this.g.todate.value;
  //   console.log(this.from);
  //   console.log(this.to);
  //   //console.log()
  //   if($("#new1").val()=='' || $("#new2").val()==''){
      
  //     swal("please select a from and to dates");
    
  //   }
  //   if($("#new1").val()>$("#new2").val()){
  //     swal("from date should be less than to date");
  //   }
  //   if($("#new1").val()<=$("#new2").val()){

  //     $("#refresh").show();
  //     this.loading = true;
      
  //      var form = new FormData();
      
  //      form.append('userid', this.currentUser.id);
  //     //  form.append('from_date', this.Searchseller.value.fromdate);
  //     //  form.append('to_date', this.Searchseller.value.todate);
  //       console.log(form);
  //      this.userService.searchsellernamesbydates(form,this.from,this.to)
  //      .pipe(first())
  //      .subscribe(
  //       data => {
                  
  //         this.asorderhistory=data['order'];
       
  //         });
  //   }

  // }
  // cleardates(){
  //   $("#refresh").hide();
  //  this.Filterbydates.reset();
  //   this.loadorderhistory();
  //  }
  
}
