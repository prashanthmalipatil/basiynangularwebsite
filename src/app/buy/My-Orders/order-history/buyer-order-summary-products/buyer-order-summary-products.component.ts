import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';

declare var $ :any;
@Component({
  selector: 'app-buyer-order-summary-products',
  templateUrl: './buyer-order-summary-products.component.html',
  styleUrls: ['./buyer-order-summary-products.component.css']
})
export class BuyerOrderSummaryProductsComponent implements OnInit {
  currentUser: User;
  asbuyerordersummary: any[] = [];
  assummary: any[]=[];
  loadingnew=true;
  idordersummary:any;
  

  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.queryParams.subscribe(params => {
      this.idordersummary=params['id_order_summary']
      
      
      });
      console.log(this.idordersummary);
  }

  ngOnInit() {
    this.loadbuyerordersummaryproducts();
  }

  loadbuyerordersummaryproducts() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('id_order_summary',this.idordersummary)
    this.userService.getbuyerordersummary(form).pipe(first()).subscribe(result => { 
        this.assummary=result['summary'];
        this.asbuyerordersummary= result['order']; 
        this.loadingnew=false;
        
        //console.log(result);
        
    });
  }
}
