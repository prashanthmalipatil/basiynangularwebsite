import { Component, OnInit } from '@angular/core';
import { PagerService, UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
declare var $ :any;
declare var swal:any;
@Component({
  selector: 'app-manage-my-store',
  templateUrl: './manage-my-store.component.html',
  styleUrls: ['./manage-my-store.component.css']
})
export class ManageMyStoreComponent implements OnInit {
  total: number;
  p: number = 1;
  currentUser: User;
  buyerproducts:any[];
  catalogdata:any[];
  buyerpages:any[];
  loadingnew=true;
  salemrp:any=[];
  per_page:any;
  constructor(private pagerService: PagerService, private userService: UserService, private router: Router, private route: ActivatedRoute ) 
  { this.currentUser = JSON.parse(localStorage.getItem('currentUser'));}

  allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];
  ngOnInit() {
    this.setPage(1);
    this.loadbuyerproducts();
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });
  }

  loadbuyerproducts(page: number = 1){
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    this.userService.loadbuyerproducts(form, page)
    .pipe(first())
    .subscribe(
      data => {
        console.log(data);
       
        
        if(status='true'){
          this.loadingnew=false;
          this.buyerproducts=data['products'];
          console.log(this.buyerproducts);
          this.total = data['products_count'];
         this.per_page=20;
         this.pager = this.pagerService.getPager(this.total, page, this.per_page);

        }
        else{
          swal('No products Found')
        }
        
    });
  }

  setPage(page: number) {

    if (page < 1 || page > this.pager.total) {
        return;
    }
  

    
   
  
  }
  deletebuyproduct(){
    
    var invite_ids = new Array();
   
    $('.sellerinv').each(function(){
        if ($(this).prop('checked')) {
          
            var invite_id = $(this).val(); 
            console.log(invite_ids);
            invite_ids.push(invite_id);
          
        }

    });

    if(invite_ids.length!=0){
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('product_ids', JSON.stringify(invite_ids));
    this.userService.deletebuyproduct(form)
    .pipe(first())
    .subscribe(
      data => {
        this.salemrp=data;
        this.catalogdata=data['pages'];
        if(status=='true'){
         swal(this.salemrp.message, "","success");
         this.loadbuyerproducts();
        }
        else if(status=='false'){
          swal(this.salemrp.message)
        }
        else{
          swal("something went wrong","", "warning")
        }
        
    });


  }
  else{
      swal('Select Product Before Deleting', "", "warning");
  }

  
  
}

}
