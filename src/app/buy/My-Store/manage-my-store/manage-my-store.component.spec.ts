import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageMyStoreComponent } from './manage-my-store.component';

describe('ManageMyStoreComponent', () => {
  let component: ManageMyStoreComponent;
  let fixture: ComponentFixture<ManageMyStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageMyStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageMyStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
