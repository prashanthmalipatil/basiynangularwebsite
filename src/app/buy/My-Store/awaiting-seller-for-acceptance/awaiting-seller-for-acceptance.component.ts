import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { NavbarService } from 'src/app/_services/navbar.service';
declare var swal:any;
declare var $ :any;
@Component({
  selector: 'app-awaiting-seller-for-acceptance',
  templateUrl: './awaiting-seller-for-acceptance.component.html',
  styleUrls: ['./awaiting-seller-for-acceptance.component.css']
})
export class AwaitingSellerForAcceptanceComponent implements OnInit {
  currentUser: User;
  asawaitingselforaccept: User[] = [];
  newmatchid:any[]=[];
  loadingnew=true;
  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
    
    this.loadawaitingsellerforaccept();
  }


  loadawaitingsellerforaccept() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.getawaitingsellerforaccept(form).pipe(first()).subscribe(result => { 
      if(result['count']!=0){
      this.loadingnew=false;
        this.asawaitingselforaccept = result['sellers']; 
        
        //console.log(result);
      }
      else{
   
        swal({
          text: 'None of your product is awaiting seller acceptance.You can add product and notify seller.',
          buttons: {
           
                confirm: true,
                //value: this.router.navigate(['/buyer-add-new-product']),
          }
         
          // showCancelButton: true, 
          // cancelButtonText: 'Ok.'
        }).then(() => {
          if (result) {
            this.router.navigate(['/buyer-add-new-product'])
          }
        });
      }
    });
  }
 


  matchid(user){
    console.log(user);
    this.newmatchid=user.user_match_id;
    console.log(this.newmatchid);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
      "user_match_id":user.user_match_id,
      "seller_name":user.seller_nickname
      }
      };
      this.router.navigate(['/buyer-awaiting-seller-acceptance/'],navigationExtras);
  }


}
