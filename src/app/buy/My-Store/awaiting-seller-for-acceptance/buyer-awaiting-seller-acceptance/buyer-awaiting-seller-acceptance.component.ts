import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
declare var $ :any;
declare var swal:any;
@Component({
  selector: 'app-buyer-awaiting-seller-acceptance',
  templateUrl: './buyer-awaiting-seller-acceptance.component.html',
  styleUrls: ['./buyer-awaiting-seller-acceptance.component.css']
})
export class BuyerAwaitingSellerAcceptanceComponent implements OnInit {
  currentUser: User;
  asbuyerawaitingselreq: User[] = [];
  userdetailid:any;
  sellername:any;
  loadingnew=true;
  Searchseller:FormGroup;
  loading=true;
  submitted=false;
  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService, private formBuilder:FormBuilder,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    
    this.route.queryParams.subscribe(params => {
      this.userdetailid=params['user_match_id'],
      this.sellername=params['seller_name']
      
      });
      //console.log(this.userdetailid);
  }

  ngOnInit() {
   
    $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
 $(".clear").hide();
 this.Searchseller = this.formBuilder.group({
  product_name: ['', Validators.required],
  
});
     console.log(this.userdetailid)

    
    this.loadbuyerawaitingsellerforaccept();
  }
  get f() { return this.Searchseller.controls; }

  onSubmitsearch() {
    // console.log("came to submit");
    this.submitted = true;
     
    // // stop here if form is invalid
     if (this.Searchseller.invalid) {
        
         console.log('false data');
         return;
     }
     this.loading = true;
      
       var form = new FormData();
      
       form.append('userid', this.currentUser.id);
      //  form.append('pname', this.Searchseller.value.product_name);
      form.append('user_match_id', this.userdetailid);
        console.log(form);
       this.userService.searchbuyerawaitingselleracceptanceproductnames(form, this.Searchseller.value.product_name)
       .pipe(first())
       .subscribe(
        data => {
                  
          this.asbuyerawaitingselreq=data['products'];
       
          });

  }

  clearsearch(){
    $(".clear").hide();
    this.asbuyerawaitingselreq=[];
   this.Searchseller.reset();
    this.loadbuyerawaitingsellerforaccept();
   }


  loadbuyerawaitingsellerforaccept() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('user_match_id',this.userdetailid)
    this.userService.getbuyerawaitingsellerforaccept(form).pipe(first()).subscribe(result => {
      if(result['count']!=0){
      this.loadingnew=false;
        this.asbuyerawaitingselreq = result['products']; 
      }
      else{
        swal({
          text: 'No more shared product with this seller. You can create product and notify the seller.',
          buttons: {
           
                confirm: true,
                cancel:true,
               
          }
        
        }).then((value) => {
          console.log(value);
          if (value) {
            this.router.navigate(['/buyer-add-new-product'])
          }
          else{
            this.router.navigate(['/buyer-awaiting-seller-acceptance-sellers-list'])
          }
        });
      }
       
    
     
    });
  }

  deletepro(){
    //console.log(user)
    var invite_ids = new Array();
    $('.checkeAll').each(function(){
      if ($(this).prop('checked')) {
          var invite_id = $(this).val(); 
          invite_ids.push(invite_id);
         console.log(invite_ids); 
      }

  });
  if(invite_ids.length!=0){
    var form = new FormData();
    form.append('product_ids', JSON.stringify(invite_ids));
    form.append('user_match_id',this.userdetailid)
    this.userService.buyerawwatingacceptance(form).pipe(first()).subscribe(result => { 
     
      this.loadbuyerawaitingsellerforaccept();
        
        //console.log(result);
        
    });
  }
  else{
    swal('Please select user to delete','','error');
  }
}


}
