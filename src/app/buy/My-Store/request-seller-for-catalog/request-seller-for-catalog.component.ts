import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';

declare var $ :any;
declare var swal:any;
@Component({
  selector: 'app-request-seller-for-catalog',
  templateUrl: './request-seller-for-catalog.component.html',
  styleUrls: ['./request-seller-for-catalog.component.css']
})
export class RequestSellerForCatalogComponent implements OnInit {
  currentUser: User;
  asreqselforcat: any[] = [];
  loadingnew=true;

  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService, ){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.loadrequestselforcatalog();
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });
  }

  loadrequestselforcatalog() {
    
       
    var form = new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.getreqselforcat(form).pipe(first()).subscribe(result => { 
      console.log(result['sellers'].length);
      if(result['sellers'].length!=0){
      this.loadingnew=false;
        this.asreqselforcat = result['sellers']; 
      }
      else{
        swal('Either you have requested all your connected sellers or you do not have connected seller -> Either you have already requested for catalog to all your connected sellers or you do not have connected seller/s',' ')
      }
       
        //console.log(this.asreqselforcat.length);
        
    });
  }
  
  
  requestcatalog(){
    
    var invite_ids = new Array();
    $('.checkeAll').each(function(){
      if ($(this).prop('checked')) {
          var invite_id = $(this).val(); 
          invite_ids.push(invite_id);
         console.log(invite_ids); 
      }

  });
  console.log(invite_ids.length);
  if(invite_ids.length!=0){

    var form=new FormData();
    form.append('userid',  this.currentUser.id);
    form.append('seller_ids', JSON.stringify(invite_ids));
      this.userService.reqcatalogbuyside(form).pipe(first()).subscribe(
        data=>{
          swal(data[0]['messsage'],'','success');
          this.loadrequestselforcatalog();
        }
      )
  }
  else if(invite_ids.length==0){
      swal('Please select atleast one Seller', '', 'error');
  }
  }
}
