import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services';
import { Router } from '@angular/router';

import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
declare var swal:any;
@Component({
  selector: 'app-shared-by-seller',
  templateUrl: './shared-by-seller.component.html',
  styleUrls: ['./shared-by-seller.component.css']
})
export class SharedBySellerComponent implements OnInit {
  currentUser: User;
  shareddata:any[]=[];
  constructor(private userService: UserService, private router: Router,private http: HttpClient) {this.currentUser = JSON.parse(localStorage.getItem('currentUser')); }

  ngOnInit() {
    this.sellershared();
  }

  sellershared(){
    var form = new FormData();
      form.append('userid', this.currentUser.id);
     
  
      this.userService.sharedbyseller(form).pipe(first()).subscribe(result => { 
          
        this.shareddata=result['sellers'];
        console.log(this.shareddata)
          
      });
  }

}
