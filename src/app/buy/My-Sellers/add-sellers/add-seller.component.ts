import { Component, OnInit, ViewChild } from "@angular/core";

import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserService, AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { first } from "rxjs/operators";

declare var $: any;
declare var swal:any;              
@Component({
    templateUrl: 'add-seller.component.html',
    styleUrls: ['add-seller.component.css'],

})

export class Addseller implements OnInit{
    loading = false;
    addsellerForm: FormGroup;
    bulkuploadForm:FormGroup
    submitted = false;
    returnUrl: string;
    currentUser: User;
    result:string;
    msg:string;
    status:boolean;
    constructor(private formBuilder: FormBuilder,private userService: UserService, private router: Router,private authenticationService: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute)
    {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit(){
        this.addsellerForm = this.formBuilder.group({
            mobile: ['', Validators.required],
            
        });


            
        this.bulkuploadForm = this.formBuilder.group({
            csv: [''],
            
        });


        
    }

    
    get f() { return this.addsellerForm.controls; }
 

    onSubmit() {
       
          this.submitted = true;
       
         // // stop here if form is invalid
          if (this.addsellerForm.invalid) {
            swal("Enter valid Mobile Number")
         //     //console.log(this.loginForm.controls);
              return;
          }
 
          this.loading = true;
        
         var form = new FormData();
        
         form.append('mobile', this.addsellerForm.value.mobile);
         form.append('userid', this.currentUser.id);
          console.log(form);
         this.userService.addseller(form)
         .pipe(first())
         .subscribe(
            
                 data => {
                     //console.log(data.status);
                     
                     if(data.status == 'SMS Sent'){
                     
                         //console.log('data');
                         console.log('new user sms sent')
                         this.toastr.success('SMS has been sent to the seller'); 
                         }
                         else if(data.status=='Invited'){
                            console.log('already invited')
                            this.toastr.error(data.message);
                         }
                         else if(data.status=='failed'){
                            console.log('cant invite yourself')
                            this.toastr.error(data.message);
                         }
                         else{
                             this.toastr.success(data.message);
                            
                              console.log('false new')
                             
                         }
                      
                      
                       
                 });
                 
               
                 
         
     }

     onSubmitbulk(){
        this.submitted = true;
        console.log('submit')
        // // stop here if form is invalid
         if (this.bulkuploadForm.invalid) {
        //     //console.log(this.loginForm.controls);
             return;
         }

         this.loading = true;
         console.log(this.bulkuploadForm.value)

         var form = new FormData();
         var csv = $("#csvfile")[0].files[0];
         form.append('csv', csv);
         
         $.ajax({
                 url:'https://dev.basiyn.com/csvreader.php',
                 method:'POST',
                 dataType:'JSON',
                 processData:false,
                 contentType:false,
                 mimeType:'multipart/formdata',
                 crossDomain:false,
                 data:form,
                 success:function(res){
                     if(res.status){
                        $("#csv").modal();
                        var html = '';
                        for(var i=0; i<res.csvdata.length; i++){
                            html+='<tr><td>'+res.csvdata[i]+'</td><td style="border:1px solid #ccc; text-align:center"><input type="checkbox" name="csvdatamobile" value="'+res.csvdata[i]+'" class="selectphone" checked></td></tr>';
                            // html+='<tr><td></td></tr>';
                        }
                        $("#invitePhone").html(html);
                        $(".cnfmsubmit").show();
                     }else{
                        swal(res.msg)
                     }
                     
                 }
                
             })
        
     }

     
     confirmsubmit(){
        var phonenumbers = new Array();
        
         $(".selectphone").each(function(){
            
             if($(this).prop('checked')){
                phonenumbers.push($(this).val());
             }
         })
         if(phonenumbers.length > 0){
            var form = new Array();
             for(var i=0; i<phonenumbers.length; i++){
                 var phone = new Object();
                 phone = {'userid':this.currentUser.id,'user_nickname':this.currentUser.nickname,'invited_user_mobile':phonenumbers[i],  'invited_user_nickname':''}
                 form.push(phone);
             }
             var formnew= new FormData();
             formnew.append('seller_invites',JSON.stringify(form));
             $.ajax({
                 url:'https://dev.basiyn.com/add-seller.php',
                 method:'POST',
                 dataType:'JSON',
                 data:formnew,
                 processData:false,
                 contentType:false,
                 success:function(res){
                     console.log(res);
                    var html = '';
                    if(res.length>0){
                        for(var i=0; i<res.length; i++){
                            html+='<tr><td>'+res[i].invited_user_mobile+'</td><td style="border:1px solid #ccc; text-align:center">'+res[i]['result']['message']+'</td></tr>';
                        }
                        $("#invitePhone").html(html);
                        $(".cnfmsubmit").hide();
                    }
                 }
             })
         }else{
             swal('Please Select atleast 1 customer to proceed');
         }
     }
 
            
}

