import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserService, AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { ActivatedRoute, Router } from "@angular/router";

import { ToastrService } from "ngx-toastr";
import { first } from "rxjs/operators";
declare var $: any;
@Component({
    selector:'app-csvupload',
    templateUrl: 'csv-uploader.component.html',
    styleUrls: ['csv-uploader.component.css'],
})

export class Csvupload implements OnInit{
    Bulkupload:FormGroup
    submitted = false;
    currentUser: User;
    loading = false;
    constructor(private formBuilder: FormBuilder,private userService: UserService, private router: Router,private authenticationService: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    ngOnInit(){
        this.Bulkupload = this.formBuilder.group({
            csv: ['', Validators.required],
            
        });
    }
    get f() { return this.Bulkupload.controls; }

    onSubmit(){
        this.submitted = true;
        console.log('submit')
        // // stop here if form is invalid
         if (this.Bulkupload.invalid) {
        //     //console.log(this.loginForm.controls);
             return;
         }

         this.loading = true;
         console.log(this.Bulkupload.value)

         var form = new FormData();
         var csv = $("#csvfile")[0].files[0];
         form.append('csv', csv);
         
         this.userService.bulkupload(form)
         .pipe(first())
         .subscribe(
            
                 data => {
                    if(status == 'true'){
                        console.log(data);
                        $("#csv").modal();
                         
                        }
                        else if(status=='false'){
                           console.log('already invited')
                           this.toastr.error(status);
                        }
                });
}
}