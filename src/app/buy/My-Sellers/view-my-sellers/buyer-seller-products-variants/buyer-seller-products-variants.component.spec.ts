import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerSellerProductsVariantsComponent } from './buyer-seller-products-variants.component';

describe('BuyerSellerProductsVariantsComponent', () => {
  let component: BuyerSellerProductsVariantsComponent;
  let fixture: ComponentFixture<BuyerSellerProductsVariantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerSellerProductsVariantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerSellerProductsVariantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
