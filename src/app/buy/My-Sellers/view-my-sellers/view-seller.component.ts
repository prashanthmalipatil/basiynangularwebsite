import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { UserService, AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $: any;
declare var swal:any; 
@Component({
    templateUrl: 'view-seller.component.html',
    styleUrls: ['view-seller.component.css'],

})

export class Viewseller implements OnInit{
    currentUser: User;
    viewseller: User[] = [];
    Searchseller:FormGroup
    submitted = false;
    returnUrl: string;
   loading=false;
   searchseller:any[]=[];
   multipleid:any[]=[];
   deleteseller:any=[];
   ids:any[]=[];
   loadingnew=true;
    constructor(private formBuilder: FormBuilder,private userService: UserService, private router: Router,private authenticationService: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit(){
        $( document ).ready(function() {
            $("#checkAll").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
        });

        $("#searchbar").keypress(function(){
            $(".clear").show();
           
       });
       $(".clear").hide();
      
        this.loadasviewseller(this.currentUser.id);

        this.Searchseller = this.formBuilder.group({
            seller_name: ['', Validators.required],
            
        });
 
    
    }

    get f() { return this.Searchseller.controls; }


    private loadasviewseller(id) {
        var form = new FormData();
        form.append('userid', this.currentUser.id);
       
       
        this.userService.getviewsellers(form).pipe(first()).subscribe(result => { 
            this.viewseller = result['results']; 
            this.loadingnew=false;
            console.log(result);
            
             
        });
      }



      //search bar
      onSubmit() {
      // console.log("came to submit");
        this.submitted = true;
     
       // // stop here if form is invalid
        if (this.Searchseller.invalid) {
           
            console.log('false data');
            return;
        }

        this.loading = true;
      
       var form = new FormData();
      
       form.append('userid', this.currentUser.id);
       form.append('seller_name', this.Searchseller.value.seller_name);
        console.log(form);
       this.userService.searchseller(form, this.Searchseller.value.seller_name)
       .pipe(first())
       .subscribe(
          
               data => {
                  
                   console.log(data);
                   
                   
                    
                       if(data['results'].length!='0'){
                        console.log('came to if')
                        for(var i=0; i<data['results'].length; i++){
                            console.log('came to for loop')
                        this.searchseller.push(data['results'][i]['user']); 
                        
                        console.log(this.ids)
                        }
                        for(var j=0; j<data['results'].length; j++){
                            this.ids.push(data['results'][j]['invite_id']);
                        }

                        console.log(this.ids);
                       }
                    else {
                            
                            this.toastr.error('No Sellers Found');
                    }
                   
                
                    
                     
                
           });
}
   clearsearch(){
    $(".clear").hide();
    this.searchseller=[];
    this.Searchseller.reset();
   }




   checksingle(user){
    //    console.log(user);
    //    $('#checkedAll').change(function(){
    //     if($(this).prop('checked') === true){
    //    this.deleteseller= user;
        
    //    this.multipleid.push(this.deleteseller.invite_id);
    //     }
    //     else{
    //         this.multipleid=[];
    //     }
    //     });
   }
   
   checksingle1(ids){
    $('#checkedAll').change(function(){
        if($(this).prop('checked') === true){
            console.log(ids);
        }
        else{
            this.ids=[];
        }
    });
}

   //single seeller delete
   singledelete(){
       console.log()
    var invite_ids = new Array();
    $('.checkedAll').each(function(){
        if ($(this).prop('checked')) {
            var invite_id = $(this).val(); 
            invite_ids.push(invite_id);
           //console.log(invite_ids); 
        }

    });
   

    var newids =new Array();
        $('.sellerdelete').each(function(){
            if ($(this).prop('checked')) {
                var new_id = $(this).val();
                newids.push(new_id);
            }
        });
        console.log(newids); 


       if(invite_ids.length!=0){
           console.log('came to if')
    var form = new FormData();
    //var inids = new Array();
    //inids.push(this.deleteseller.invite_id);
    form.append('invite_id', JSON.stringify(invite_ids));
    form.append('userid', this.currentUser.id);
    form.append('status', '7');
    $.ajax({
        url:'https://dev.basiyn.com/delete-customers.php',
        method:'POST',
        dataType:'JSON',
        processData:false,
        contentType:false,
        mimeType:'multipart/formdata',
        crossDomain:false,
        data:form,
        success:function(res){
           if(res.status=='true'){
              swal('You are no more connected with this customer');
              //this.ngOnInit();
              //location.reload();
           }else{
            swal('Customer coudnt be updated');
           }
        }
       
    })
       }

       else if(this.ids.length !=0){
        console.log('came to else if search delete')
        var form = new FormData();
        //var inids = new Array();
        //inids.push(this.deleteseller.invite_id);
        form.append('invite_id', JSON.stringify(this.ids));
        form.append('userid', this.currentUser.id);
        form.append('status', '7');
        $.ajax({
            url:'https://dev.basiyn.com/delete-customers.php',
            method:'POST',
            dataType:'JSON',
            processData:false,
            contentType:false,
            mimeType:'multipart/formdata',
            crossDomain:false,
            data:form,
            success:function(res){
               if(res.status=='true'){
                swal('You are no more connected with this customer');
                  //this.ngOnInit();
                  //location.reload();
               }else{
                swal('Customer coudnt be updated');
               }
            }
           
        })
       }
       else if(newids.length!= 0){
        console.log('came to else bulk delete')
        var form = new FormData();
        //var inids = new Array();
        //inids.push(this.deleteseller.invite_id);
        form.append('invite_id', JSON.stringify(newids));
        form.append('userid', this.currentUser.id);
        form.append('status', '7');
        $.ajax({
            url:'https://dev.basiyn.com/delete-customers.php',
            method:'POST',
            dataType:'JSON',
            processData:false,
            contentType:false,
            mimeType:'multipart/formdata',
            crossDomain:false,
            data:form,
            success:function(res){
               if(res.status=='true'){
                swal('You are no more connected with this customer');
                  //this.ngOnInit();
                  //location.reload();
               }else{
                swal('Customer coudnt be updated');
               }
            }
           
        })
       }
       else if(newids.length==0){
        swal('please select user to delete');
       }
    
}


   delete(user){
    this.deleteseller.push(user);
    console.log(this.deleteseller)
   }
   
   viewmysellerdetail(user){
     console.log(user);
//this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
let navigationExtras: NavigationExtras = {
    queryParams: {
    'user':user
  
    }
    };
 this.router.navigate(['/buyer-seller-products-variants/'],navigationExtras);
  }
}