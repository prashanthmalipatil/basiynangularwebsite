import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { UserService, AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models';
declare var $ :any;
declare var swal:any; 
@Component({
    templateUrl: 'seller-invitation.component.html',
    styleUrls: ['seller-invitation.component.css'],

})

export class Sellerinvitation implements OnInit{
    currentUser: User;
    asinvited: any[] = [];
    sellerinfo:any=[];
    acceptallid:User[]=[];
    loadingnew=true;
    constructor( private userService: UserService,private http: HttpClient, private toastr: ToastrService, ){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }


    ngOnInit(){

        this.loadasinvited();
        $("#checkAll").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    }



    private loadasinvited() {
            var form = new FormData();
            form.append('userid', this.currentUser.id);
        
            this.userService.getsellerinvited(form).pipe(first()).subscribe(result => { 
                this.asinvited = result['results']; 
                this.loadingnew=false;
                console.log(result);
                
            });
      }




      public sellerinvclick(user){
    //     console.log(user);
    //     $('#checkedAll').change(function(){
    //         if($(this).prop('checked') === true){
               
    //             this.sellerinfo= user;
    //             console.log(this.sellerinfo);
    //         }
    //         else{
    //             this.sellerinfo=[];
    //             //alert('Please select user')
    //         }
        
    //     //reject
    // });
    }


    //Accept Invitation
    acceptinvit(){

        var newids =new Array();
        $('.sellerinv').each(function(){
            if ($(this).prop('checked')) {
                var new_id = $(this).val();
                newids.push(new_id);
            }
        });
        console.log(newids); 



        //console.log(this.sellerinfo.length)
        var invite_ids = new Array();
        $('.checkedAll').each(function(){
            if ($(this).prop('checked')) {
                var invite_id = $(this).val(); 
                invite_ids.push(invite_id);
               console.log(invite_ids); 
            }

        });
    
        if(invite_ids.length!=0 ){
            console.log('came to if');
                var form = new FormData();
                //var inids = new Array();
                //inids.push(this.sellerinfo.invite_id);
                form.append('invite_id', JSON.stringify(invite_ids));
                form.append('userid', this.currentUser.id);
                form.append('status', '3');
                $.ajax({
                    url:'https://dev.basiyn.com/update-seller-invitation-status.php',
                    method:'POST',
                    dataType:'JSON',
                    processData:false,
                    contentType:false,
                    mimeType:'multipart/formdata',
                    crossDomain:false,
                    data:form,
                    success:function(res){
                    if(res.status=='true'){
                        swal('Status Updated Successfully');
                        //this.ngOnInit();
                        //location.reload();
                    }else{
                        swal('Status Coudnt be updated');
                    }
                    }
                
        })
        }
        else if(newids.length==0){
            console.log('came to else');
            swal('please select user in order to accept or reject');
            this.sellerinfo=[];

        }
        else if(newids.length!=0){
            console.log('came to else if length !=0')
                var form = new FormData();
                form.append('invite_id', JSON.stringify(newids));
                form.append('userid', this.currentUser.id);
                form.append('status', '3');
                $.ajax({
                    url:'https://dev.basiyn.com/update-seller-invitation-status.php',
                    method:'POST',
                    dataType:'JSON',
                    processData:false,
                    contentType:false,
                    mimeType:'multipart/formdata',
                    crossDomain:false,
                    data:form,
                    success:function(res){
                    if(res.status=='true'){
                        swal('Status Updated Successfully');
                        //this.ngOnInit();
                        location.reload();
                    }else{
                        swal('Status Coudnt be updated');
                    }
                    }
                
        })
        }
        
    }
    newids(newids: any): string | Blob {
        throw new Error("Method not implemented.");
    }
   


    //reject invitation
    rejectnew(){
        var newids =new Array();
        $('.sellerinv').each(function(){
            if ($(this).prop('checked')) {
                var new_id = $(this).val();
                newids.push(new_id);
            }
        });
        console.log(newids); 


            var invite_ids = new Array();
            $('.checkedAll').each(function(){
                if ($(this).prop('checked')) {
                    var invite_id = $(this).val(); 
                    invite_ids.push(invite_id);
                console.log(invite_ids); 
                }

            });



        if(invite_ids.length!=0){
                var form = new FormData();
                //var inids = new Array();
                //inids.push(this.sellerinfo.invite_id);
                form.append('invite_id', JSON.stringify(invite_ids));
                form.append('userid', this.currentUser.id);
                form.append('status', '8');

                $.ajax({
                    url:'https://dev.basiyn.com/update-seller-invitation-status.php',
                    method:'POST',
                    dataType:'JSON',
                    processData:false,
                    contentType:false,
                    mimeType:'multipart/formdata',
                    crossDomain:false,
                    data:form,
                    success:function(res){
                    if(res.status=='true'){
                        swal('Status Updated Successfully');
                        location.reload();
                    }else{
                        swal('Status Coudnt be updated');
                    }
                    }
           
        })
        }
        else if(newids.length==0){
            console.log('came to else');
            swal('please select user in order to accept or reject');
            this.sellerinfo=[];

        }
        else if(newids.length!=0){
                    var form = new FormData();
                    //var inids = new Array();
                    //inids.push(this.sellerinfo.invite_id);
                    form.append('invite_id', JSON.stringify(newids));
                    form.append('userid', this.currentUser.id);
                    form.append('status', '8');

                    $.ajax({
                        url:'https://dev.basiyn.com/update-seller-invitation-status.php',
                        method:'POST',
                        dataType:'JSON',
                        processData:false,
                        contentType:false,
                        mimeType:'multipart/formdata',
                        crossDomain:false,
                        data:form,
                        success:function(res){
                        if(res.status=='true'){
                            swal('Status Updated Successfully');
                            location.reload();
                        }else{
                            swal('Status Coudnt be updated');
                        }
                        }
               
            })
        }

        
    }
}
                                                                                                                           