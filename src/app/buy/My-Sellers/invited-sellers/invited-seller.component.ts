import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { UserService, AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models';
@Component({
    templateUrl: 'invited-seller.component.html',
    styleUrls: ['invited-seller.component.css'],

})
export class Invitedsellerlist implements OnInit{
    currentUser: User;
    asinvitedseller: User[] = [];

    constructor(private userService: UserService,private http: HttpClient){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit(){
        this.loadasbinvitedsellers(this.currentUser.id);
    }

    private loadasbinvitedsellers(id) {
        var form = new FormData();
        form.append('userid', this.currentUser.id);
       
        this.userService.getbuyinvitedsellerslist(form).pipe(first()).subscribe(result => { 
            this.asinvitedseller = result['customers']; 
            console.log(result['customers'])
             
        });
      }

     
}