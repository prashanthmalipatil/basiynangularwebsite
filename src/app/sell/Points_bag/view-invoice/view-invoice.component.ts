import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
declare var $:any;
declare var swal:any;
@Component({
  selector: 'app-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.css']
})
export class ViewInvoiceComponent implements OnInit {
  currentuser:User;
  invoice:any;
  exportdata:any[]=[];
  csvdata:any[]=[];
  Filterbydates:FormGroup;
  from:any;
  to:any;
  constructor(private userService:UserService, private formBuilder:FormBuilder) {this.currentuser=JSON.parse(localStorage.getItem('currentUser')) }

  ngOnInit() {
    this.loadinvoice();
    this.Filterbydates = this.formBuilder.group({
      fromdate: ['', Validators.required],
      todate:['',Validators.required]});
    //this.export();
  }


  loadinvoice(){
    var form=new FormData();
    form.append('userid', this.currentuser.id)

    this.userService.loadinvoices(form).pipe(first()).subscribe(
      data => {
        this.invoice=data['paymentList'];
      }
    );
  }

  export(){
    var exportdata1=this.invoice.length;
   
    
     for (let i=0; i<exportdata1; i++){
    var datajs={"id": this.invoice[i]['id'], "payment Refrence": this.invoice[i]['payment_reference'], "Date Added": this.invoice[i]['date_added'], "Total Amount":this.invoice[i]['total_amount'] }
      this.csvdata.push(datajs);
     }
      console.log(this.csvdata);
   
     var options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: true,
        useBom: true,
        noDownload: false,
        headers: ["id", "Payment Rreference", "Date Added", "Total Amount"]
      };
    
    new Angular5Csv(this.csvdata, 'payment_list', options);
    //console.log(Angular5Csv);
  }


  //filter by dates

  get g(){return this.Filterbydates.controls;}


  onSubmit(){
    console.log(this.Filterbydates.value);
    console.log(this.g.fromdate.value);
    this.from=this.g.fromdate.value;
    this.to=this.g.todate.value;
    if($("#new1").val()=='' || $("#new2").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new1").val()>$("#new2").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new1").val()<=$("#new2").val()){

      $("#refresh").show();
      
  }

  var form= new FormData()
  form.append('userid', this.currentuser.id);
  form.append('from', this.from);
  form.append('to', this.to);
  this.userService.filterinvoice(form).pipe(first()).subscribe(
    data => {
      this.invoice=data['paymentList'];
    }
  );
}


cleardates(){
    $("#refresh").hide();
   this.Filterbydates.reset();
  this.loadinvoice();
   }



}
