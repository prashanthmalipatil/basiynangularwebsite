import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAndViewpointsComponent } from './add-and-viewpoints.component';

describe('AddAndViewpointsComponent', () => {
  let component: AddAndViewpointsComponent;
  let fixture: ComponentFixture<AddAndViewpointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAndViewpointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAndViewpointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
