import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
declare var $:any;
@Component({
  selector: 'app-add-and-viewpoints',
  templateUrl: './add-and-viewpoints.component.html',
  styleUrls: ['./add-and-viewpoints.component.css']
})
export class AddAndViewpointsComponent implements OnInit {
  currentUser:User;
  points:any;
  avapoints:any;
  addpointsform:FormGroup;
  convfac:any;
  amnt:any;
  gstval:any;
  totalamnt:any;
  entamount:any;
  
  constructor(private userService:UserService,private formBuilder:FormBuilder) { this.currentUser=JSON.parse(localStorage.getItem('currentUser'))}

  ngOnInit() {
    this.loadpoints();
    this.availpoints();

  ;
  }

  loadpoints(){
   
      var form = new FormData();
          form.append('userid', this.currentUser.id);
          
          this.userService.getpoints(form)
          .pipe(first())
          .subscribe(
            data => {
              this.points=data['transactions'];
            }
          );

  }

  //get avaliable points
  availpoints(){
    var form=new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.availpoints(form).pipe(first()).subscribe(
      data => {
        this.avapoints=data['available_points'];
      }
    
  );
  }

  //add points
  addpoints(){
    $("#points").modal();
  }

 

  submit(ngForm) {
    console.log(ngForm);
    $("#payu").modal();
   // this.entamount=ngForm.ramount;
    //console.log(this.entamount);
  }

  submitnew(ngForm){
    console.log(ngForm);
    var form=new FormData()
    form.append('amount', "123");
    form.append('email', "admin@gmail.com");
    form.append('firstname', "admin");
    form.append('furl', "123");
    form.append('phone', "8050888073");
    form.append('productinfo', "123");
      form.append('service_provider', "payu_paisa");
      form.append('surl',"145");


      $.ajax({
        url:'https://sandboxsecure.payu.in/_payment',
        method:'POST',
        dataType:'JSON',
        processData:false,
        contentType:false,
        mimeType:'multipart/formdata',
        crossDomain:true,
        data:form,
        success:function(res){
           console.log(res);
        }
       
    })
  }

  //calc amnt on key up
  onSearchChange(searchValue : string ) {  
  
    this.entamount=searchValue;
    this.gstval=parseInt(this.entamount)*(0.18);
    this.totalamnt=parseInt(this.entamount)+parseFloat(this.gstval);
    this.userService.amntcalc().pipe(first()).subscribe(
      data => {
        //console.log(data);
        if(data['status']=="true"){
            this.convfac=data['conversion_factor'];
            this.amnt=(this.convfac)*(this.entamount);
           
        }
        
      }
    );
  }
}
