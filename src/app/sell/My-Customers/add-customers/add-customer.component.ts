import { Component, OnInit } from "@angular/core";

import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserService, AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { first } from "rxjs/operators";
declare var $: any;
declare var swal:any;
@Component({
    templateUrl: 'add-customer.component.html',
    styleUrls: ['add-customer.component.css'],

})

export class Addcustomer implements OnInit{
    loading = false;
    Addcustomer: FormGroup;
    Bulkupload:FormGroup
    submitted = false;
    returnUrl: string;
    currentUser: User;
    result:string;
  
    constructor(private formBuilder: FormBuilder,private userService: UserService, private router: Router,private authenticationService: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute,)
    {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit(){
        this.Addcustomer = this.formBuilder.group({
            invited_user_mobile: ['', Validators.required],
          
        });

        this.Bulkupload = this.formBuilder.group({
            csv: ['', Validators.required],
           
        });
    }
    get f() { return this.Addcustomer.controls; }


    onSubmit() {
       
          this.submitted = true;
       
         // // stop here if form is invalid
          if (this.Addcustomer.invalid) {
        
              return;
          }
          console.log(this.Addcustomer.value);
          this.loading = true;
         


     
        
        // this.buyer_invites = form;
        var buyer_invites=[];
        var user = {'userid':this.currentUser.id,'user_nickname':this.currentUser.nickname,'invited_user_mobile':this.Addcustomer.value.invited_user_mobile,  'invited_user_nickname':''};
        buyer_invites.push(user);
        var form = new FormData();
        form.append('buyer_invites',JSON.stringify(buyer_invites));
        
       
        console.log(form);
       
         this.userService.addcustomer(form)
         .pipe(first())
         .subscribe(
            
                 data => {
                     console.log(data[0].result.status);
                     console.log(data[0].result.message)
                     
                     if(data[0].result.status == 'Not Sent'){
                     
                       
                         this.toastr.error(data[0].result.message); 
                         }
                         else if(data[0].result.status=='Invited'){
                            //console.log('already invited')
                            this.toastr.success(data[0].result.message);
                         }
                    //      else if(data[0].result.status=='failed'){
                    //         console.log('cant invite yourself')
                    //         this.toastr.error(data.message);
                    //      }
                    //      else{
                    //          this.toastr.error("Already Invited");
                            
                    //           console.log('false new')
                             
                    //      }
                      
                      
                       
                 });
                 
               
                 
         
     }


     //bulk upload on submit
     onSubmitbulk(){
        this.submitted = true;
        console.log('submit')
        // // stop here if form is invalid
         if (this.Bulkupload.invalid) {
        //     //console.log(this.loginForm.controls);
             return;
         }

         this.loading = true;
         console.log(this.Bulkupload.value)

         var form = new FormData();
         var csv = $("#csvfile1")[0].files[0];
         form.append('csv', csv);
         
         $.ajax({
                 url:'https://dev.basiyn.com/csvreader.php',
                 method:'POST',
                 dataType:'JSON',
                 processData:false,
                 contentType:false,
                 mimeType:'multipart/formdata',
                 crossDomain:false,
                 data:form,
                 success:function(res){
                     if(res.status){
                        $("#csvnew").modal();
                        var html = '';
                        for(var i=0; i<res.csvdata.length; i++){
                            html+='<tr><td>'+res.csvdata[i]+'</td><td style="border:1px solid #ccc; text-align:center"><input type="checkbox" name="csvdatamobile" value="'+res.csvdata[i]+'" class="selectphone" checked></td></tr>';
                            // html+='<tr><td></td></tr>';
                        }
                        $("#invitePhone1").html(html);
                        $(".cnfmsubmit").show();
                     }else{
                        swal(res.msg)
                     }
                     
                 }
                
             })
        
     }

//popup submit
     confirmsubmit(){
        var phonenumbers = new Array();
        
         $(".selectphone").each(function(){
            
             if($(this).prop('checked')){
                phonenumbers.push($(this).val());
             }
         })
         if(phonenumbers.length > 0){
            var form = new Array();
             for(var i=0; i<phonenumbers.length; i++){
                 var phone = new Object();
                 phone = {'userid':this.currentUser.id,'user_nickname':this.currentUser.nickname,'invited_user_mobile':phonenumbers[i],  'invited_user_nickname':''}
                 form.push(phone);
             }
             var formnew= new FormData();
             formnew.append('buyer_invites',JSON.stringify(form));
             $.ajax({
                 url:'https://dev.basiyn.com/add-buyer.php',
                 method:'POST',
                 dataType:'JSON',
                 data:formnew,
                 processData:false,
                 contentType:false,
                 success:function(res){
                     console.log(res);
                    var html = '';
                    if(res.length>0){
                        for(var i=0; i<res.length; i++){
                            html+='<tr><td>'+res[i].invited_user_mobile+'</td><td style="border:1px solid #ccc; text-align:center">'+res[i]['result']['message']+'</td></tr>';
                        }
                        $("#invitePhone1").html(html);
                        $(".cnfmsubmit").hide();
                    }
                 }
             })
         }else{
             swal('Please Select atleast 1 customer to proceed');
         }
     }
    }