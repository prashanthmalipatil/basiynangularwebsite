import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first, switchAll } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { UserService, AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models';


declare var $ :any;
declare var swal:any; 
@Component({
    templateUrl: 'customer-invitation.component.html',
    styleUrls: ['customer-invitation.component.css'],

})

export class Customerinvitation implements OnInit{
    currentUser: User;
    ascustomerinvited: User[] = [];
    customerinfo:any=[];
    acceptallcustomerid:User[]=[];
    loadingnew=true;
    constructor(private userService: UserService,private http: HttpClient, private toastr: ToastrService,){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit(){
        this.loadascustomerinvited(this.currentUser.id);


        $("#checkAll").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    }
    
    private loadascustomerinvited(id) {
        var form = new FormData();
        form.append('userid', this.currentUser.id);
       
        this.userService.getcustomerinvited(form).pipe(first()).subscribe(result => { 
            this.ascustomerinvited = result['results']; 
            this.loadingnew=false;
            console.log(result['results'])
             
        });
      }


      public customerinvclick(user){
        // console.log(user);
        // $('#checkedAll').change(function(){
        //     if($(this).prop('checked') === true){
        //             this.customerinfo= user;
        //     }
        //     else{
        //         this.customerinfo=[];
        //     }
        // });

        
        
    }
    

    //Accept Customer Invitation
    acceptinvitcustomer(){

        var newids =new Array();
        $('.customerclick').each(function(){
            if ($(this).prop('checked')) {
                var new_id = $(this).val();
                newids.push(new_id);
            }
        });
        console.log(newids); 
        

        var invite_ids = new Array();
        $('.checkedAll').each(function(){
            if ($(this).prop('checked')) {
                var invite_id = $(this).val(); 
                invite_ids.push(invite_id);
               console.log(invite_ids); 
            }

        });
      
        if(invite_ids.length!=0){
            console.log("if");
        var form = new FormData();
        //var inids = new Array();
        //inids.push(this.customerinfo.invite_id);
        form.append('invite_id', JSON.stringify(invite_ids));
        form.append('userid', this.currentUser.id);
        form.append('status', '3');
        $.ajax({
            url:'https://dev.basiyn.com/update-seller-invitation-status.php',
            method:'POST',
            dataType:'JSON',
            processData:false,
            contentType:false,
            mimeType:'multipart/formdata',
            crossDomain:false,
            data:form,
            success:function(res){
               if(res.status=='true'){
                swal('Status Updated Successfully');
                  //this.ngOnInit();
                  location.reload();
               }else{
                swal('Status Coudnt be updated');
               }
            }
           
        })
        }
        else if(newids.length==0){
            console.log("elseif newids ==0");
            console.log('came to else');
        swal('please select user in order to accept or reject');

            
            this.customerinfo=[];

        }
        else if(newids.length!=0){
            console.log("length !==0");
            var form = new FormData();
            //var inids = new Array();
            //inids.push(this.sellerinfo.invite_id);
            form.append('invite_id', JSON.stringify(newids));
            form.append('userid', this.currentUser.id);
            form.append('status', '3');
            $.ajax({
                url:'https://dev.basiyn.com/update-seller-invitation-status.php',
                method:'POST',
                dataType:'JSON',
                processData:false,
                contentType:false,
                mimeType:'multipart/formdata',
                crossDomain:false,
                data:form,
                success:function(res){
                   if(res.status=='true'){
                    swal('Status Updated Successfully');
                      //this.ngOnInit();
                      location.reload();
                   }else{
                    swal('Status Coudnt be updated');
                   }
                }
               
            })
        }
       
       
    }


    //Reject CUstomer Invitation
    rejectnewcustomer(){
        var newids =new Array();
        $('.customerclick').each(function(){
            if ($(this).prop('checked')) {
                var new_id = $(this).val();
                newids.push(new_id);
            }
        });
        console.log(newids); 

        var invite_ids = new Array();
        $('.checkedAll').each(function(){
            if ($(this).prop('checked')) {
                var invite_id = $(this).val(); 
                invite_ids.push(invite_id);
               console.log(invite_ids); 
            }

        });



        if(invite_ids.length!=0){

        var form = new FormData();
        //var inids = new Array();
        //inids.push(this.customerinfo.invite_id);
        form.append('invite_id', JSON.stringify(invite_ids));
        form.append('userid', this.customerinfo.userid);
        form.append('status', '8');
        $.ajax({
            url:'https://dev.basiyn.com/update-seller-invitation-status.php',
            method:'POST',
            dataType:'JSON',
            processData:false,
            contentType:false,
            mimeType:'multipart/formdata',
            crossDomain:false,
            data:form,
            success:function(res){
               if(res.status=='true'){
                  swal('Status Updated Successfully');
                  location.reload();
               }else{
                   swal('Status Coudnt be updated');
               }
            }
           
        })

        }
        else if(newids.length==0){
            console.log('came to else');
            swal('please select user in order to accept or reject');
            this.customerinfo=[];
           

        }
        else if(newids.length!=0){

           
            var form = new FormData();
        //var inids = new Array();
        //inids.push(this.sellerinfo.invite_id);
        form.append('invite_id', JSON.stringify(newids));
        form.append('userid', this.currentUser.id);
        form.append('status', '8');
        }
        // $.ajax({
        //     url:'https://dev.basiyn.com/update-seller-invitation-status.php',
        //     method:'POST',
        //     dataType:'JSON',
        //     processData:false,
        //     contentType:false,
        //     mimeType:'multipart/formdata',
        //     crossDomain:false,
        //     data:form,
        //     success:function(res){
        //        if(res.status=='true'){
        //         swal('Status Updated Successfully');
        //           location.reload();
        //        }else{
        //         swal('Status Coudnt be updated');
        //        }
        //     }
           
        // })
    }
   

}