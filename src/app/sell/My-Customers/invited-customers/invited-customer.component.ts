import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { UserService, AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models';
@Component({
    templateUrl: 'invited-customer.component.html',
    styleUrls: ['invited-customer.component.css'],

})
export class Invitedcustomer implements OnInit{
    currentUser: User;
    asinvitedcustomer: User[] = [];
    loadingnew=true;

    constructor(private userService: UserService,private http: HttpClient){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit(){
        this.loadassinvitedcustomers(this.currentUser.id);
    }

    private loadassinvitedcustomers(id) {
        var form = new FormData();
        form.append('userid', this.currentUser.id);
       
        this.userService.getsellinvitedcustomerslist(form).pipe(first()).subscribe(result => { 
            this.asinvitedcustomer = result['customers']; 
            this.loadingnew=false;
            console.log(result['customers'])
             
        });
      }
}