import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerBuyerProductsVariantsComponent } from './seller-buyer-products-variants.component';

describe('SellerBuyerProductsVariantsComponent', () => {
  let component: SellerBuyerProductsVariantsComponent;
  let fixture: ComponentFixture<SellerBuyerProductsVariantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerBuyerProductsVariantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerBuyerProductsVariantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
