import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;
declare var swal:any; 
@Component({
  selector: 'app-seller-buyer-products-variants',
  templateUrl: './seller-buyer-products-variants.component.html',
  styleUrls: ['./seller-buyer-products-variants.component.css']
})
export class SellerBuyerProductsVariantsComponent implements OnInit {
  currentUser: User;
  asviewcustomerprodlist: any[] = [];
  loadingnew=true;
  loading=false;
  Searchseller: FormGroup;
  submitted = false;
  id:any;
  constructor(private userService: UserService,private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.queryParams.subscribe(params => {
      this.id=params['user']
      });
      console.log(this.id);
    
  }

  ngOnInit() {

    

    $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
 $(".clear").hide();
  $("#refresh").hide();
  
      this.Searchseller = this.formBuilder.group({
        product_name: ['', Validators.required],
      });
      this.loadviewcustomerproductvarients();
  }
  get f() { return this.Searchseller.controls; }


  onSubmitsearch() {
    // console.log("came to submit");
    this.submitted = true;
     
    // // stop here if form is invalid
     if (this.Searchseller.invalid) {
        
         console.log('false data');
         return;
     }
     this.loading = true;
      
       var form = new FormData();
      
       form.append('userid', this.currentUser.id);
      //  form.append('seller_name', this.Searchseller.value.seller_name);
        console.log(form);
       this.userService.searchviewcustomerproddetailpage(form, this.id,this.Searchseller.value.product_name)
       .pipe(first())
       .subscribe(
        data => {
          
          this.asviewcustomerprodlist=data['products'];
       
          });

  }
  clearsearch(){
    $(".clear").hide();
    // this.asviewcustomerprod=[];
    
   this.Searchseller.reset();
    this.loadviewcustomerproductvarients();
   }

  loadviewcustomerproductvarients() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    //form.append('buyer_id',this.buyerid);
    this.userService.getsellbuyerprodvarients(form, this.id).pipe(first()).subscribe(result => { 
     
        this.asviewcustomerprodlist= result['products']; 
        this.loadingnew=false;

        //console.log(result);
        
    });
  }

}
