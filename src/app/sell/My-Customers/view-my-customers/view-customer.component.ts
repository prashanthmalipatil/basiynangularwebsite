import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { UserService, AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $: any;
declare var swal:any; 
@Component({
    templateUrl: 'view-customer.component.html',
    styleUrls: ['view-customer.component.css'],

})

export class Viewcustomer implements OnInit{
    currentUser: User;
    viewcustomer: User[] = [];
    Searchcustomer:FormGroup
    submitted = false;
    returnUrl: string;
   loading=false;
   loadingnew=true;
   searchcustomer:any[]=[];
   deletecustomer:any=[];
   multipleid=new Array();
   ids:any[]=[];
    buyerid:any[]=[];
    user: any;
    constructor(private formBuilder: FormBuilder,private userService: UserService, private router: Router,private authenticationService: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit(){
        $( document ).ready(function() {
            $("#searchcustomerbar").keypress(function(){
                 $(".clear").show();
                
            });
            $(".clear").hide();
           
        });
        this.loadasviewcustomer(this.currentUser.id);
        
        this.Searchcustomer = this.formBuilder.group({
            customer_name: ['', Validators.required],

        });
       
    }

    get f() { return this.Searchcustomer.controls; }
    
    private loadasviewcustomer(id) {
        var form = new FormData();
        form.append('userid', this.currentUser.id);
        
        this.userService.getviewcustomers(form).pipe(first()).subscribe(result => {
          
            this.viewcustomer = result['customers']; 
            this.loadingnew=false;
            console.log(result);   
        });
      }




      //search bar
      onSubmit() {
        console.log("came to submit");
         this.submitted = true;
      
        // // stop here if form is invalid
         if (this.Searchcustomer.invalid) {
            
             console.log('false data');
             return;
         }
 
         this.loading = true;
       
        var form = new FormData();
       
        form.append('userid', this.currentUser.id);
        form.append('customer_name', this.Searchcustomer.value.customer_name);
         console.log(form);
        this.userService.searchcustomer(form, this.Searchcustomer.value.customer_name)
        .pipe(first())
        .subscribe(
           
                data => {
                   
                    console.log(data['customers']);
                    
                    console.log(data);
                     
                        if(data['customers'].length!='0'){
                            for(var i=0; i<data['customers'].length; i++){
                                //console.log(data['customers'].length)
                         this.searchcustomer.push(data['customers'][i]['user']); 
                       
                            }
                            for(var j=0; j< data['customers'].length; j++){
                                this.ids.push(data['customers'][j]['invite_id'])
                            }
                            console.log(this.ids);
                        }
                     else {
                             
                             this.toastr.error('No Customers Found');
                     }
                    
                 
                     
                      
                 
            });
 }


 clearsearchcustomer(){
     $(".clear").hide();
     this.searchcustomer=[];
     this.Searchcustomer.reset();
    }

   
   
    checksinglecustomer(){
        
        // alert(this.value);
        //     if($(this).prop('checked') === true){
        //     this.multipleid.push(this.value);
        //     }
        //     console.log(this.multipleid);

    }
 
    checksinglecustomer1(ids){
        $('#checkedAll').change(function(){
            if($(this).prop('checked') === true){
                console.log(ids);
            }else{
                this.ids=[];
            }
        });
    }



    //Single  Delete Customer
    singledeletecustomers(){
        var invit_ids =new Array();
        $('.singlecust').each(function(){
            if ($(this).prop('checked')) {
                var invite_id = $(this).val();
                invit_ids.push(invite_id);
            }
        });
        console.log(invit_ids); 

        //console.log(this.multipleid.length);

      
         if(this.ids.length !=0){
            console.log('came to search delete')
            var form = new FormData();
            //var inids = new Array();
            //inids.push(this.deletecustomer.invite_id);
            form.append('invite_id', JSON.stringify(this.ids));
            form.append('userid', this.currentUser.id);
            form.append('status', '7');
            $.ajax({
                url:'https://dev.basiyn.com/delete-customers.php',
                method:'POST',
                dataType:'JSON',
                processData:false,
                contentType:false,
                mimeType:'multipart/formdata',
                crossDomain:false,
                data:form,
                success:function(res){
                   if(res.status=='true'){
                      swal('You are no more connected with this customer');
                      //this.ngOnInit();
                      location.reload();
                   }else{
                       swal('Customer coudnt be updated');
                   }
                }
               
            })
        }
        else if(invit_ids.length!= 0){
            var form = new FormData();
            form.append('invite_id', JSON.stringify(invit_ids));
            form.append('userid', this.currentUser.id);
            form.append('status', '7');
            $.ajax({
                url:'https://dev.basiyn.com/delete-customers.php',
                method:'POST',
                dataType:'JSON',
                processData:false,
                contentType:false,
                mimeType:'multipart/formdata',
                crossDomain:false,
                data:form,
                success:function(res){
                   if(res.status=='true'){
                      swal('You are no more connected with this customer');
                      //this.ngOnInit();
                      location.reload();
                   }else{
                    swal('Customer coudnt be updated');
                   }
                }
               
            })
        }
        else if(invit_ids.length==0){
            swal('please select user to delete');
       }
     
    }

    viewmycustomerdetail(user){
        console.log(user);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
        queryParams: {
        'user':user
      
        }
        };
     this.router.navigate(['/seller-buyer-products-variants/'],navigationExtras);
      }

}

