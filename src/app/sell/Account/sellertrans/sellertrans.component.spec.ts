import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellertransComponent } from './sellertrans.component';

describe('SellertransComponent', () => {
  let component: SellertransComponent;
  let fixture: ComponentFixture<SellertransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellertransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellertransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
