import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { UserService } from 'src/app/_services';
import { first } from 'rxjs/operators';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
declare var $:any;
@Component({
  selector: 'app-seller-accountdetails',
  templateUrl: './seller-accountdetails.component.html',
  styleUrls: ['./seller-accountdetails.component.css']
})
export class seltranamntComponent implements OnInit {
  currentUser:User;
  loadcustomerdetails:any;
  loadingnew=true;
  constructor(private userService:UserService, private route:ActivatedRoute, private router: Router) { this.currentUser=JSON.parse(localStorage.getItem('currentUser'))}

  ngOnInit() {
    this.loadselleraccount();
    $(".clear").hide();
  }

    loadselleraccount(){
      var form=new FormData();
      form.append('userid', this.currentUser.id);

      this.userService.loadselleraccountdetails(form).pipe(first()).subscribe(
        data=>{
          this.loadcustomerdetails=data['customers'];
          this.loadingnew=false;
        }
      )
    }

    transdet(user_match_id){
      let navigationExtras: NavigationExtras = {
        queryParams: {
       "id":user_match_id,
      
        }
        };
        this.router.navigate(['/mini-account-seller-transaction-details/'],navigationExtras);
    }

    //search
    submit(ngForm){
      $(".clear").show();
      console.log(ngForm);
      var form=new FormData();
      form.append('cname', ngForm.cname);
      form.append('userid', this.currentUser.id);
      this.userService.searchcust(form).pipe(first()).subscribe(
        data=>{
            this.loadcustomerdetails=data['customers'];

        }
      )
    }

    clearsearchcustomer(){
      $(".clear").hide();
      this.loadselleraccount();
    }

}
