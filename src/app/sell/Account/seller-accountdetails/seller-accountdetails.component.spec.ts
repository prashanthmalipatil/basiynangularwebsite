import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerAccountdetailsComponent } from './seller-accountdetails.component';

describe('SellerAccountdetailsComponent', () => {
  let component: SellerAccountdetailsComponent;
  let fixture: ComponentFixture<SellerAccountdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerAccountdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerAccountdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
