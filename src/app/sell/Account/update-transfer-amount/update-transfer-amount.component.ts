import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { UserService } from 'src/app/_services';
import { first } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FindValueSubscriber } from 'rxjs/internal/operators/find';
declare var swal:any;
@Component({
  selector: 'app-update-transfer-amount',
  templateUrl: './update-transfer-amount.component.html',
  styleUrls: ['./update-transfer-amount.component.css']
})
export class sellupdamntComponent implements OnInit {
  currentUser:User;
  customers:any;
  updateprice:FormGroup;
  submitted=false;
  loading=false;
  show=false;
  constructor(private userService:UserService, private formBuilder:FormBuilder) {this.currentUser=JSON.parse(localStorage.getItem('currentUser')) }

  ngOnInit() {
    this.loadcustomer();
    this.updateprice = this.formBuilder.group({
      match_id: ['', Validators.required],
      category: ['', Validators.required],
      mydate: ['', Validators.required],
      myamount: ['', Validators.required],
      remarks: [''],
  });
  }


  loadcustomer(){
    var form=new FormData()
    form.append('userid', this.currentUser.id);
    this.userService.loadcustomer(form).pipe(first()).subscribe(
      data =>{
          this.customers=data;
      }
    );
  }


  ChangingValue(event){

    console.log(event);
  }
  seleccat(){
    this.show=true;
  }


  get f() { return this.updateprice.controls; }


  onSubmit(){

    this.submitted = true;
      
        // stop here if form is invalid
        if (this.updateprice.invalid) {
            //console.log(this.loginForm.controls);
            return;
        }

        this.loading = true;
    console.log(this.updateprice.value);

    var form=new FormData();
    form.append('userid', this.currentUser.id);
    form.append('user_match_id', this.f.match_id.value);
    form.append('category', this.f.category.value);
    form.append('myamount', this.f.myamount.value);
    form.append('remarks', this.f.remarks.value);
    form.append('mydate', this.f.mydate.value);

    this.userService.updatepriceselleraccount(form).pipe(first()).subscribe(data=>{
      console.log(data);
      if(data['status']=='success'){
        swal('Amount is inserted', "", 'success');
          this.updateprice.reset();
          this.submitted=false;
      }
    });
  }


}
