import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateTransferAmountComponent } from './update-transfer-amount.component';

describe('UpdateTransferAmountComponent', () => {
  let component: UpdateTransferAmountComponent;
  let fixture: ComponentFixture<UpdateTransferAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateTransferAmountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTransferAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
