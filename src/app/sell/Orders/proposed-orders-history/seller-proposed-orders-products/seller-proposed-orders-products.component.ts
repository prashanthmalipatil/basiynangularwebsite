import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;

@Component({
  selector: 'app-seller-proposed-orders-products',
  templateUrl: './seller-proposed-orders-products.component.html',
  styleUrls: ['./seller-proposed-orders-products.component.css']
})
export class SellerProposedOrdersProductsComponent implements OnInit {
  currentUser: User;
  assellproposedsummary: any[] = [];
  assellproposed:any[]=[];
  astotal:any[]=[];
  loadingnew=true;
  
  idbuyer:any;
  idproposedsummary:any;

  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  
    this.route.queryParams.subscribe(params => {
      this.idbuyer=params['buyer_id'],
      this.idproposedsummary=params['id']
     
      
      });
      console.log(this.idproposedsummary);
      console.log(this.idbuyer);  
      
    }

  ngOnInit() {

    this.loadsellproposedddetailhistory();
  }



  loadsellproposedddetailhistory() {
    
    var form = new FormData();
    
    
    form.append('userid', this.currentUser.id);
    form.append('proposed_order_id',this.idproposedsummary);
    form.append('buyer_id',this.idbuyer);

    this.userService.getsellproposeddetailhistory(form).pipe(first()).subscribe(result => { 
        
        this.assellproposedsummary= result['summary']; 
        this.assellproposed=result['order'];
        this.loadingnew=false;
        //console.log(result);
        
    });
  }


}
