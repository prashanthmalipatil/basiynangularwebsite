import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerProposedOrdersProductsComponent } from './seller-proposed-orders-products.component';

describe('SellerProposedOrdersProductsComponent', () => {
  let component: SellerProposedOrdersProductsComponent;
  let fixture: ComponentFixture<SellerProposedOrdersProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerProposedOrdersProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerProposedOrdersProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
