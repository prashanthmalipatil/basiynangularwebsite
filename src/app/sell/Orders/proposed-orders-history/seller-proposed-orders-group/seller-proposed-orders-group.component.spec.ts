import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerProposedOrdersGroupComponent } from './seller-proposed-orders-group.component';

describe('SellerProposedOrdersGroupComponent', () => {
  let component: SellerProposedOrdersGroupComponent;
  let fixture: ComponentFixture<SellerProposedOrdersGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerProposedOrdersGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerProposedOrdersGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
