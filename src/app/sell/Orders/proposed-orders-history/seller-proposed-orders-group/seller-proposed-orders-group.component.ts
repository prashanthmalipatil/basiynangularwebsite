import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;
declare var swal:any; 


@Component({
  selector: 'app-seller-proposed-orders-group',
  templateUrl: './seller-proposed-orders-group.component.html',
  styleUrls: ['./seller-proposed-orders-group.component.css']
})
export class SellerProposedOrdersGroupComponent implements OnInit {
  currentUser: User;
  assellproposed: any[] = [];
  loadingnew=true;
  proposedorderid:any[]=[];
  buyerid:any[]=[];
  loading=false;
  Searchseller: FormGroup;
  Filterbydates:FormGroup;
    from:any;
    to:any;
    submitted = false;
    searchdata:any;
  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));}

  ngOnInit() {

    $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
 $(".clear").hide();
 $("#refresh").hide();
  


   
    this.checkvalue();
  }
  
    


  // loadsellproposedhistory() {
    
  //   var form = new FormData();
  //   form.append('userid', this.currentUser.id);

  //   this.userService.getsellproposedhistory(form).pipe(first()).subscribe(result => { 
     
  //       this.assellproposed= result['order']; 
        
  //       this.loadingnew=false;
  //       //console.log(result);
        
  //   });
  // }

  sellerproposeddetailhistory(user){
    console.log(user);
    this.proposedorderid=user.id;
    console.log(this.proposedorderid);
   
    this.buyerid=user.buyer_id;
    console.log(this.buyerid);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "id":user.id,
        "buyer_id":user.buyer_id
     }
      };
      this.router.navigate(['/seller-proposed-orders-products/'],navigationExtras);
  }

  checkvalue(){
    //this.selectedvalue=$( "#group" ).val();
    //this.filterbydd=$("#group-by").val();
    this.from=$("#new1").val();
    this.to=$("#new2").val();
    this.searchdata=$("#mysearch").val();
    console.log(this.from);
    console.log(this.to);
    var form=new FormData();
    
    
  
    if(this.from!=null && this.to!=null){
      
     
      form.append('from', this.from);
      form.append('to', this.to);
    }
  
    
  
    if(this.searchdata!=null){
      form.append('sname', this.searchdata);
    }
    form.append('userid', this.currentUser.id);
    this.userService.getsellproposedhistory(form).pipe(first()).subscribe(
      data=>{
        //this.asprocessorder= data['order']; 
        this.assellproposed= data['order']; 

        this.loadingnew=false;
      
      }
    )
  }

  
  Searchbar(){
    this.checkvalue();
    
  }
  searchbydates(){
    

    if($("#new1").val()=='' || $("#new2").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new1").val()>$("#new2").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new1").val()<=$("#new2").val()){
      this.checkvalue();
    $("#refresh").show();
    }

    
    //$('#refresh').css("display", "block");
  }
  
  
  
  
  clearsearch(){
    $(".clear").hide();
    $("#mysearch").val('');
    this.checkvalue();
   }
  
   cleardates(){
     $("#refresh").hide();
     $("#new1").val('');
     $("#new2").val('');
     //this.from=[];
     //this.to=[];
     this.checkvalue();
    
   }

}
