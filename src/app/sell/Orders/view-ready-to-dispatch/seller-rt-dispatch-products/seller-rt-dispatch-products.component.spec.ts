import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerRtDispatchProductsComponent } from './seller-rt-dispatch-products.component';

describe('SellerRtDispatchProductsComponent', () => {
  let component: SellerRtDispatchProductsComponent;
  let fixture: ComponentFixture<SellerRtDispatchProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerRtDispatchProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerRtDispatchProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
