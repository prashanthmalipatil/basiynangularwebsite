import { Component, OnInit, PipeTransform, Pipe  } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';
declare var swal:any;
declare var $ :any;
@Component({
  selector: 'app-seller-rt-dispatch-products',
  templateUrl: './seller-rt-dispatch-products.component.html',
  styleUrls: ['./seller-rt-dispatch-products.component.css']
})
@Pipe({
  name: 'dateFormatPipe',
})
export class SellerRtDispatchProductsComponent implements OnInit {
  currentUser: User;
  asviewrtddetail: any[] = [];
  asviewrtdorder:any[]=[];
  loadingnew=true;
  loading=false;
  idusermatch:any;
  idaddress:any;
  idbuyerpickup:any;
  invite_ids:any;
  teammemberid:any;

  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); 
  
    this.route.queryParams.subscribe(params => {
      this.idusermatch=params['user_match_id'],
      this.idbuyerpickup=params['buyer_pickup_flag'],
      this.idaddress=params['address_id'],
      this.teammemberid=params['newid']
     
      
      });
         }

  ngOnInit() {
    this.loadsellviewrtddetail();
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });

  $('#newcheck').each(function(){
    if ($(this).prop('checked')) {
        var invite_id = $(this).val(); 
        this.invite_id.push(invite_id);
        console.log(this.invite_id);
      
    }

});
  }
  loadsellviewrtddetail() {
    
    var form = new FormData();
    form.append('user_match_id', this.idusermatch);
    form.append('userid', this.currentUser.id);
    form.append('buyer_pickup', this.idbuyerpickup);
    form.append('address_id', this.idaddress);
    
    

    this.userService.getsellviewrtddetail(form).pipe(first()).subscribe(result => { 
     
        this.asviewrtddetail= result['detail']; 
        this.asviewrtdorder=result['order'];
        this.loadingnew=false;
        //console.log(result);
        
    });
  }

  confdispatch(){
    var comp2=new Array();
    var comp1=new Array();
  var teamid=new Array();
  var usematchid=new Array();
  usematchid=this.idusermatch;
  teamid=this.teammemberid;

    var data = new Array();

    
    //data=[];
    console.log(data);
    $('.checkeAll').each(function(){
    
    comp1=JSON.parse($(this).data('check'));
    comp2=$(this).val();

      if ($(this).prop('checked')) {

        //console.log($('.checkeAll:checked').val());
    var objnews= new Object();
        objnews['team_member_id'] =teamid;
        objnews['match_master_id']= this.idusermatch;
        objnews['user_match_id']=usematchid;
        objnews['dispatch_qty'] = [JSON.stringify($(this).data('dqty'))];
        objnews['match_master_id'] = [JSON.stringify($(this).data('mmid'))];
        objnews['order_summary_id'] = [JSON.stringify($(this).data('dstatus'))];
        objnews['order_no'] = [$(this).data('ordno')];
        objnews['address_id'] = $(this).data('addid');
        objnews['total_qty'] = [$(this).val()];
        objnews['total_amount'] = $(this).data('totalamnt');
        objnews['dispatch_flag'] = $(this).data('flag');
       
  }

  else{
    swal('Please select atlest one product before confirm dispatch', '', 'error');
  }
  data.push(objnews);
})
   
if(comp1!=null && comp2!=null){
  swal('Please Select Row and Dispatch Quantity should not have 0 values', '','error');
}

  else if((comp1==comp2 || comp1>comp2)){
   
    var form=new FormData();
    form.append('userid', this.currentUser.id);
    form.append('product_data', JSON.stringify(data));

    this.userService.viewrtdcondispatch(form).pipe(first()).subscribe(
      data =>{

        console.log(data);
        swal(data['message'], '', 'success');
        this.loadsellviewrtddetail();
      }
    )
    
  }
  else{
    swal('You can not enter more than outstanding Quantity','','error')
  }
}

}
