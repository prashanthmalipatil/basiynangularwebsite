import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerRtDispatchComponent } from './seller-rt-dispatch.component';

describe('SellerRtDispatchComponent', () => {
  let component: SellerRtDispatchComponent;
  let fixture: ComponentFixture<SellerRtDispatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerRtDispatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerRtDispatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
