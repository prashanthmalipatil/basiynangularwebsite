import { Component, OnInit, PipeTransform, Pipe  } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;
@Component({
  selector: 'app-seller-order-summary-products',
  templateUrl: './seller-order-summary-products.component.html',
  styleUrls: ['./seller-order-summary-products.component.css']
})
@Pipe({
  name: 'dateFormatPipe',
})
export class SellerOrderSummaryProductsComponent implements OnInit {
  currentUser: User;
  assellreceiveddetail: any[] = [];
  assellreceived:any[]=[];
  loadingnew=true;
  idbuyer:any;
  idordersummary:any;
  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); 
  
    this.route.queryParams.subscribe(params => {
      this.idbuyer=params['buyer_id'],
      this.idordersummary=params['id_order_summary']
     
      
      });
      console.log(this.idordersummary);
      console.log(this.idbuyer);  
  
  
  }

  ngOnInit() {
    this.loadsellreceivedddetailhistory();
  }

  loadsellreceivedddetailhistory() {
    
    var form = new FormData();
    
    form.append('userid', this.currentUser.id);
    form.append('id_order_summary',this.idordersummary);
    form.append('buyer_id',this.idbuyer);

    this.userService.getsellreceiveddetailhistory(form).pipe(first()).subscribe(result => { 
     
        this.assellreceiveddetail= result['summary']; 
        this.assellreceived=result['order'];
        this.loadingnew=false;
        //console.log(result);
        
    });
  }

}
