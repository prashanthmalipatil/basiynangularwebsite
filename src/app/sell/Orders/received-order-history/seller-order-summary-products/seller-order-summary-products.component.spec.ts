import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerOrderSummaryProductsComponent } from './seller-order-summary-products.component';

describe('SellerOrderSummaryProductsComponent', () => {
  let component: SellerOrderSummaryProductsComponent;
  let fixture: ComponentFixture<SellerOrderSummaryProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerOrderSummaryProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerOrderSummaryProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
