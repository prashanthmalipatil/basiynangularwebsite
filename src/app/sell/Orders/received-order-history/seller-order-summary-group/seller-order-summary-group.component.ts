import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

declare var $ :any;
declare var swal:any; 

@Component({
  selector: 'app-seller-order-summary-group',
  templateUrl: './seller-order-summary-group.component.html',
  styleUrls: ['./seller-order-summary-group.component.css']
})
export class SellerOrderSummaryGroupComponent implements OnInit {
  currentUser: User;
  assellreceived: any[] = [];
  loadingnew=true;
  loading=false;
  buyerid:any[]=[];
  ordersummaryid:any[]=[];
  Searchseller: FormGroup;
  Filterbydates:FormGroup;
    from:any;
    to:any;
    submitted = false;
    searchdata:any;
    exportdatacon:any[]=[];
    csvdata:any[]=[];
    filename:any;
  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); }

  ngOnInit() {

    $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
 $(".clear").hide();
 $("#refresh").hide();
  

    this.checkvalue();
  }
  

 
  

  // loadsellreceivedhistory() {
    
  //   var form = new FormData();
  //   form.append('userid', this.currentUser.id);

  //   this.userService.getsellreceivedhistory(form).pipe(first()).subscribe(result => { 
     
  //       this.assellreceived= result['order']; 
        
  //       this.loadingnew=false;
  //       //console.log(result);
        
  //   });
  // }

  sellerreceivedhistory(user){
    console.log(user);
    this.ordersummaryid=user.id_order_summary;
    console.log(this.ordersummaryid);
    
    this.buyerid=user.buyer_id;
    console.log(this.buyerid);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "id_order_summary":user.id_order_summary,
        "buyer_id":user.buyer_id
     }
      };
      this.router.navigate(['/seller-order-summary-products/'],navigationExtras);
  }

  
  checkvalue(page: number = 1){
    //this.selectedvalue=$( "#group" ).val();
    //this.filterbydd=$("#group-by").val();
    this.from=$("#new1").val();
    this.to=$("#new2").val();
    this.searchdata=$("#mysearch").val();
    console.log(this.from);
    console.log(this.to);
    var form=new FormData();
    
    
  
    if(this.from!=null && this.to!=null){
      
     
      form.append('from', this.from);
      form.append('to', this.to);
    }
  
    
  
    if(this.searchdata!=null){
      form.append('sname', this.searchdata);
    }
    form.append('userid', this.currentUser.id);
    this.userService.getsellreceivedhistory(form).pipe(first()).subscribe(
      data=>{
        //this.asprocessorder= data['order']; 
        this.assellreceived= data['order']; 

        this.loadingnew=false;
      
      }
    )
  }

  
  Searchbar(){
    this.checkvalue();
  }

  searchbydates(){
    

    if($("#new1").val()=='' || $("#new2").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new1").val()>$("#new2").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new1").val()<=$("#new2").val()){
      this.checkvalue();
    $("#refresh").show();
    }

    
    //$('#refresh').css("display", "block");
  }
  
  
  
  
  clearsearch(){
    $(".clear").hide();
    $("#mysearch").val('');
    this.checkvalue();
   }
  
   cleardates(){
     $("#refresh").hide();
     $("#new1").val('');
     $("#new2").val('');
     //this.from=[];
     //this.to=[];
     this.checkvalue();
    
   }



   
  closemod(){
    $('#export').hide();
  }

  exportorders(){
    $('#export').show();

  }
  datesub(ngForm){
    if($("#new3").val()=='' || $("#new4").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new3").val()>$("#new4").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new3").val()<=$("#new4").val()){

      var form=new FormData();
      form.append('from', ngForm.from);
      form.append('to', ngForm.to);
      form.append('userid', this.currentUser.id)
      this.userService.receivedordhistory(form).pipe(first()).subscribe(
        data=>{
          this.exportdatacon=data['order'];
          this.filename=data['filename'];
          console.log(this.filename);
        var exportdata1=this.exportdatacon.length;
        for (let i=0; i<exportdata1; i++){
          var datajs={"Order no": this.exportdatacon[i]['order_no'], "Product Id": this.exportdatacon[i]['product_id'], "Product Name": this.exportdatacon[i]['product_name'], "Customer Id":this.exportdatacon[i]['buyer_id'],  "Customer Name":this.exportdatacon[i]['buyer_nickname'] ,  "Cost": this.exportdatacon[i]['total_amount'],  'Address':this.exportdatacon[i]['address_line_1'], 'City':this.exportdatacon[i]['city'],'pincode':this.exportdatacon[i]['pincode']  }
            this.csvdata.push(datajs);
           }

        var options = {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: true,
          useBom: true,
          noDownload: false,
          headers: ["Order Id", "Product Id", "Product Name", "Customer Id", "Customer Name", "Cost",  "Address","City", "Pincode" ]
        };
      
      new Angular5Csv(this.csvdata, this.filename, options);
      }
        
      )
    console.log(ngForm);
  }
}
}
