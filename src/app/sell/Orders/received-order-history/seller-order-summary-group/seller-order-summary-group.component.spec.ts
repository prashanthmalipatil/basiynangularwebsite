import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerOrderSummaryGroupComponent } from './seller-order-summary-group.component';

describe('SellerOrderSummaryGroupComponent', () => {
  let component: SellerOrderSummaryGroupComponent;
  let fixture: ComponentFixture<SellerOrderSummaryGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerOrderSummaryGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerOrderSummaryGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
