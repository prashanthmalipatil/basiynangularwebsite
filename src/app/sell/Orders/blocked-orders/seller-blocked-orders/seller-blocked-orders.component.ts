import { Component, OnInit,  PipeTransform, Pipe } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;

@Component({
  selector: 'app-seller-blocked-orders',
  templateUrl: './seller-blocked-orders.component.html',
  styleUrls: ['./seller-blocked-orders.component.css']
})
@Pipe({
  name: 'dateFormatPipe',
})
export class SellerBlockedOrdersComponent implements OnInit {
  currentUser: User;
  asblocked: any[] = [];
  loadingnew=true;
  loading=false;

  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));}

  ngOnInit() {
    this.loadblockedorders();
  }
  loadblockedorders() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.getblockedorders(form).pipe(first()).subscribe(result => { 
     
        this.asblocked= result['blocked_orders']; 
        
        this.loadingnew=false;
        //console.log(result);
        
    });
  }

}
