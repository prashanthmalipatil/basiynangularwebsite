import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerBlockedOrdersComponent } from './seller-blocked-orders.component';

describe('SellerBlockedOrdersComponent', () => {
  let component: SellerBlockedOrdersComponent;
  let fixture: ComponentFixture<SellerBlockedOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerBlockedOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerBlockedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
