import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerCancelOrderedProductsComponent } from './seller-cancel-ordered-products.component';

describe('SellerCancelOrderedProductsComponent', () => {
  let component: SellerCancelOrderedProductsComponent;
  let fixture: ComponentFixture<SellerCancelOrderedProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerCancelOrderedProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerCancelOrderedProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
