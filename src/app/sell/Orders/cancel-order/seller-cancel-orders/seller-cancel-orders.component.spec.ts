import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerCancelOrdersComponent } from './seller-cancel-orders.component';

describe('SellerCancelOrdersComponent', () => {
  let component: SellerCancelOrdersComponent;
  let fixture: ComponentFixture<SellerCancelOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerCancelOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerCancelOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
