import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService, PagerService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;
declare var swal:any;

@Component({
  selector: 'app-seller-cancel-orders',
  templateUrl: './seller-cancel-orders.component.html',
  styleUrls: ['./seller-cancel-orders.component.css']
})
export class SellerCancelOrdersComponent implements OnInit {
  currentUser: User;
  ascancelorder: any[] = [];
  outstandingorderquantity: any[]=[];
  outstandingordervalue: any[]=[];
  newsellerid:any[]=[];
  loadingnew=true;
  loading=false;
  Searchseller: FormGroup;
  Filterbydates:FormGroup;
    from:any;
    to:any;
    submitted = false;
    filterbytwodate:any;
    searchdata:any;
    filterbydd:any;
    selectedvalue:any;
    total: number;
    p: number = 1;
    custname=true;
    per_page:any;
  constructor(private userService: UserService,private pagerService:PagerService, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); }



    allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];


  ngOnInit() {

    $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
 $(".clear").hide();
  $("#refresh").hide();


  this.checkvalue();
}

checkvalue(page: number = 1){
  this.selectedvalue=$( "#group" ).val();
  this.filterbydd=$("#group-by").val();
  this.from=$("#new1").val();
  this.to=$("#new2").val();
  this.searchdata=$("#mysearch").val();
  console.log(this.from);
  console.log(this.to);
  var form=new FormData();
  
  if(this.selectedvalue!=null){
    form.append('order_by', this.selectedvalue);
  }

  if(this.from!=null && this.to!=null){
    
    if(this.filterbydd=='ddate'){
    form.append('byfilter', this.filterbydd);
  }
  if(this.filterbydd=='odate'){
    form.append('byfilter', this.filterbydd);

  }
    form.append('from', this.from);
    form.append('to', this.to);
  }

  

  if(this.searchdata!=null){
    form.append('pname', this.searchdata);
  }
  form.append('userid', this.currentUser.id);
  this.userService.getcancelledorders(form).pipe(first()).subscribe(
    data=>{
      //this.asprocessorder= data['order']; 
      this.ascancelorder= data['order']; 
      this.outstandingorderquantity= data['TotaloutStandingQty'];
      this.outstandingordervalue=data['Totaloutstanding_order_amount'];
      this.loadingnew=false;
      this.total = data['products_count'];
      this.per_page=20;
       this.pager = this.pagerService.getPager(this.total, page, this.per_page);
    }
  )
}


 

Searchbar(){
  this.checkvalue();
  
}
searchbydates(){
  if($("#new1").val()=='' || $("#new2").val()==''){
      
    swal("please select a from and to dates", '', 'info');
  
  }
  if($("#new1").val()>$("#new2").val()){
    swal("from date should be less than to date", '', 'error');
  }
  if($("#new1").val()<=$("#new2").val()){
    this.checkvalue();
  $("#refresh").show();
  }
  //$('#refresh').css("display", "block");
}

handleSelectedValue(value){
  this.custname=false;
  this.selectedvalue=$( "#group" ).val();
  this.checkvalue();
  
}


clearsearch(){
  $(".clear").hide();
  $("#mysearch").val('');
  this.checkvalue();
 }

 cleardates(){
   $("#refresh").hide();
   $("#new1").val('');
   $("#new2").val('');
   //this.from=[];
   //this.to=[];
   this.checkvalue();
  
 }
  
  //     this.Searchseller = this.formBuilder.group({
  //       product_name: ['', Validators.required],
      
  // });
  // this.Filterbydates = this.formBuilder.group({
  //   fromdate: ['', Validators.required],
  //   todate:['',Validators.required]});
    

  
  // get f() { return this.Searchseller.controls; }
  // get g(){return this.Filterbydates.controls;}

  // onSubmitsearch() {
  //   // console.log("came to submit");
  //   this.submitted = true;
     
  //   // // stop here if form is invalid
  //    if (this.Searchseller.invalid) {
        
  //        console.log('false data');
  //        return;
  //    }
  //    this.loading = true;
      
  //      var form = new FormData();
      
  //      form.append('userid', this.currentUser.id);
  //     //  form.append('seller_name', this.Searchseller.value.seller_name);
  //       console.log(form);
  //      this.userService.searchcancelorderproductnames(form, this.Searchseller.value.product_name)
  //      .pipe(first())
  //      .subscribe(
  //       data => {
  //         this.outstandingorderquantity= data['TotaloutStandingQty'];
  //         this.outstandingordervalue=data['Totaloutstanding_order_amount'];
  //         this.ascancelorder=data['order'];
       
  //         });

  // }
  // clearsearch(){
  //   $(".clear").hide();
  //   this.ascancelorder=[];
  //   this.outstandingordervalue=[];
  //   this.outstandingorderquantity=[];
  //  this.Searchseller.reset();
  //   this.loadcancelledorders();
  //  }
  // loadcancelledorders() {
    
  //   var form = new FormData();
  //   form.append('userid', this.currentUser.id);

  //   this.userService.getcancelledorders(form).pipe(first()).subscribe(result => { 
  //     this.outstandingorderquantity= result['TotaloutStandingQty'];
  //     this.outstandingordervalue=result['Totaloutstanding_order_amount'];
  //       this.ascancelorder= result['order']; 
  //       this.loadingnew=false;

  //       //console.log(result);
        
  //   });
  // }

  cancelorder(user){
    // console.log(user);
    this.newsellerid=user.seller_id;
    console.log(this.newsellerid);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
      // "seller_id":user.seller_id
    
      }
      };
      this.router.navigate(['/seller-cancel-ordered-products/'],navigationExtras);
  }
}

