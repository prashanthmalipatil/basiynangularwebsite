import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;
@Component({
  selector: 'app-seller-cancelled-products-group',
  templateUrl: './seller-cancelled-products-group.component.html',
  styleUrls: ['./seller-cancelled-products-group.component.css']
})
export class SellerCancelledProductsGroupComponent implements OnInit {
  currentUser: User;
  asviewrtc: any[] = [];
  loadingnew=true;
  loading=false;
  usermatchid:any[]=[];
  buyerpickupid:any[]=[];
  addressid:any[]=[];
  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); }

  ngOnInit() {
    this.loadsellviewrtc();
  }

  loadsellviewrtc() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.getsellviewrtc(form).pipe(first()).subscribe(result => { 
     
        this.asviewrtc= result['order']; 
        
        this.loadingnew=false;
        //console.log(result);
        
    });
  }

  sellerviewRTC(user){
    console.log(user);
    this.usermatchid=user.user_match_id;
    console.log(this.usermatchid);
    this.buyerpickupid=user.buyer_pickup_flag;
    console.log(this.buyerpickupid);
    this.addressid=user.address_id;
    console.log(this.addressid);

    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "user_match_id":user.user_match_id,
        "buyer_pickup_flag":user.buyer_pickup_flag,
        "address_id":user.address_id
     }
      };
      this.router.navigate(['/seller-cancelled-products/'],navigationExtras);
  }

}
