import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerCancelledProductsGroupComponent } from './seller-cancelled-products-group.component';

describe('SellerCancelledProductsGroupComponent', () => {
  let component: SellerCancelledProductsGroupComponent;
  let fixture: ComponentFixture<SellerCancelledProductsGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerCancelledProductsGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerCancelledProductsGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
