import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerCancelledProductsComponent } from './seller-cancelled-products.component';

describe('SellerCancelledProductsComponent', () => {
  let component: SellerCancelledProductsComponent;
  let fixture: ComponentFixture<SellerCancelledProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerCancelledProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerCancelledProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
