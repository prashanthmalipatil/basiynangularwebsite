import { Component, OnInit, PipeTransform, Pipe } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';
declare var swal:any;


declare var $ :any;
@Component({
  selector: 'app-seller-cancelled-products',
  templateUrl: './seller-cancelled-products.component.html',
  styleUrls: ['./seller-cancelled-products.component.css']
})

@Pipe({
  name: 'dateFormatPipe',
})

 
export class SellerCancelledProductsComponent implements OnInit {
  currentUser: User;
  asviewrtccancel: any[] = [];
  asviewrtcorder:any[]=[];
  loadingnew=true;
  loading=false;
  idusermatch:any;
  idaddress:any;
  idbuyerpickup:any;
  selectvalue:any;


  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); 
  
    this.route.queryParams.subscribe(params => {
      this.idusermatch=params['user_match_id'],
      this.idbuyerpickup=params['buyer_pickup_flag'],
      this.idaddress=params['address_id']
     
      
      });
      
      console.log(this.idusermatch);
      console.log(this.idbuyerpickup); 
      console.log(this.idaddress);
    }
    

  ngOnInit() {
    //  this.date = this.datePipe.transform(Date, 'dd-MM-yy');
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });
   
    this.loadsellviewrtcdetail();
  }

  loadsellviewrtcdetail() {
    
    var form = new FormData();
    form.append('user_match_id', this.idusermatch);
    form.append('userid', this.currentUser.id);
    form.append('buyer_pickup', this.idbuyerpickup);
    form.append('address_id', this.idaddress);
    
    

    this.userService.getsellviewrtcdetail(form).pipe(first()).subscribe(result => { 
     
        this.asviewrtccancel= result['detail']; 
        this.asviewrtcorder=result['order'];
        this.loadingnew=false;
        //console.log(result);
        
    });
  }

  


  confcancel(){
    var data = new Array();
    //data=[];
    console.log(data);
    $('.checkeAll').each(function(){
     
      if ($(this).prop('checked')) {
        //console.log($('.checkeAll:checked').val());
    var objnews= new Object();
    objnews['team_member_id'] =$(this).data('teamid');
        objnews['match_master_id']= $(this).data('matchid');
        objnews['user_match_id']=$(this).data('matchid');
        objnews['reason'] = [JSON.stringify($(this).data('resason'))];
        objnews['order_summary_id'] = [JSON.stringify($(this).data('summid'))];
        objnews['order_no'] = [JSON.stringify($(this).data('orderno'))];
        objnews['address_id'] = $(this).data('addid');
        objnews['total_qty'] = [$(this).val()];
        objnews['total_amount'] = $(this).data('totalamnt');
        objnews['dispatch_flag'] = $(this).data('flag');
      }
      else{
        swal('please selectc checckbox','','error')
      }
  data.push(objnews);
  console.log(data);
})
  }
}
