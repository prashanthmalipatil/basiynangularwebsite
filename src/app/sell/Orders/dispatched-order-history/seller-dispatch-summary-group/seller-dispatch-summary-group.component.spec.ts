import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerDispatchSummaryGroupComponent } from './seller-dispatch-summary-group.component';

describe('SellerDispatchSummaryGroupComponent', () => {
  let component: SellerDispatchSummaryGroupComponent;
  let fixture: ComponentFixture<SellerDispatchSummaryGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerDispatchSummaryGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerDispatchSummaryGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
