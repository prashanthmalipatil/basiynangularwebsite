import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;
@Component({
  selector: 'app-seller-dispatch-summary-group',
  templateUrl: './seller-dispatch-summary-group.component.html',
  styleUrls: ['./seller-dispatch-summary-group.component.css']
})
export class SellerDispatchSummaryGroupComponent implements OnInit {
  currentUser: User;
  asselldispatchsummary: any[] = [];
  assellsummary: any[]=[];
  assellattachments: any[]=[];
  loadingnew=true;
  iddispatch:any;
  idbuyer:any;
  idusermatch:any;

  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.queryParams.subscribe(params => {
      this.idbuyer=params['buyer_id'],
      this.iddispatch=params['id_dispatch'],
      this.idusermatch=params['user_match_id']
      
      });
      console.log(this.iddispatch);
      console.log(this.idbuyer);  
      console.log(this.idusermatch);
  }

  ngOnInit() {
    this.loadselldispatchsummaryproducts();
  }

  loadselldispatchsummaryproducts() {
    
    var form = new FormData();
   
    form.append('buyer_id',this.idbuyer);
    
    form.append('dispatch_id',this.iddispatch);
    form.append('user_match_id',this.idusermatch);
    form.append('userid', this.currentUser.id);
    
   
    this.userService.getselldispatchsummary(form).pipe(first()).subscribe(result => { 
        this.assellsummary=result['dispatch_details'];
        this.asselldispatchsummary= result['order']; 
        this.assellattachments= result['attachments'];
        this.loadingnew=false;
        
        //console.log(result);
        
    });
  }

  downloadatt(){
    $('#export').show();
  }
  closemod(){
    $('#export').hide();
  }

}
