import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchedOrderHistoryComponent } from './dispatched-order-history.component';

describe('DispatchedOrderHistoryComponent', () => {
  let component: DispatchedOrderHistoryComponent;
  let fixture: ComponentFixture<DispatchedOrderHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatchedOrderHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchedOrderHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
