import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;
@Component({
  selector: 'app-seller-propose-order-by-customer',
  templateUrl: './seller-propose-order-by-customer.component.html',
  styleUrls: ['./seller-propose-order-by-customer.component.css']
})
export class SellerProposeOrderByCustomerComponent implements OnInit {
  currentUser: User;
  asproposedconnected: any[] = [];
  asproposednonconnected:any[]=[];
  loadingnew=true;
  loading=false;
  Searchseller: FormGroup;
  Searchseller1:FormGroup;
  submitted = false;

  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));}

  ngOnInit() {
    $("#connected-customer").hide();
    $("#connected-customer-details").hide();
    $("#non-connected-customer").hide();
    $("#non-connected-customer-details").show();
    
  }
  connectedcustomers(){
    $("#connected").hide();
    $("#nonconnected").hide();
    $("#connected-customer").show();
    $("#connected-customer-details").show();
    
    $("#mysearch").keypress(function(){
      $(".clear").show();
    });
    $(".clear").hide();

    this.Searchseller = this.formBuilder.group({
      seller_name: ['', Validators.required]
      
  });
    var form = new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.getsellproposedconnected(form).pipe(first()).subscribe(result => { 
     
        this.asproposedconnected= result['order']; 
        
        this.loadingnew=false;
        //console.log(result);
        
    });
    



  }
  get f() { return this.Searchseller.controls; }

  onSubmitsearch() {
    // console.log("came to submit");
    this.submitted = true;
     
    // // stop here if form is invalid
     if (this.Searchseller.invalid) {
        
         console.log('false data');
         return;
     }
     this.loading = true;
      
       var form = new FormData();
      
       form.append('userid', this.currentUser.id);
       form.append('seller_name', this.Searchseller.value.seller_name);
        console.log(form);
       this.userService.searchsellproposedconnectednames(form, this.Searchseller.value.seller_name)
       .pipe(first())
       .subscribe(
        data => {
                  
          this.asproposedconnected=data['order'];
       
          });

  }


  clearsearch(){
    $(".clear").hide();
    this.asproposedconnected=[];
   this.Searchseller.reset();
    this.connectedcustomers();
   }


 

  nonconnectedcustomers(){
    $("#connected").hide();
    $("#nonconnected").hide();
    $("#non-connected-customer").show();
    $("#non-connected-customer-details").show();

    $("#mysearch").keypress(function(){
      $(".clear").show();
    });
    $(".clear").hide();

    this.Searchseller1 = this.formBuilder.group({
      seller_name1: ['', Validators.required],
      
  });

    var form = new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.getsellproposednonconnected(form).pipe(first()).subscribe(result => { 
     
        this.asproposednonconnected= result['order']; 
        
        this.loadingnew=false;
        //console.log(result);
        
    });
  }
   get g() { return this.Searchseller1.controls; }

  onSubmitsearch1() {
    // console.log("came to submit");
    this.submitted = true;
     
    // // stop here if form is invalid
     if (this.Searchseller1.invalid) {
        
         console.log('false data');
         return;
     }
     this.loading = true;
      
       var form = new FormData();
      
       form.append('userid', this.currentUser.id);
       form.append('seller_name', this.Searchseller1.value.seller_name1);
        console.log(form);
       this.userService.searchsellproposednonconnectednames(form, this.Searchseller1.value.seller_name1)
       .pipe(first())
       .subscribe(
        data => {
                  
          this.asproposednonconnected=data['order'];
       
          });

  }


  clearsearch1(){
    $(".clear").hide();
    this.asproposednonconnected=[];
   this.Searchseller1.reset();
    this.nonconnectedcustomers();
   }
  
}
