import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerProposeOrderByCustomerComponent } from './seller-propose-order-by-customer.component';

describe('SellerProposeOrderByCustomerComponent', () => {
  let component: SellerProposeOrderByCustomerComponent;
  let fixture: ComponentFixture<SellerProposeOrderByCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerProposeOrderByCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerProposeOrderByCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
