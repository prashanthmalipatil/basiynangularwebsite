import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerCancelSummaryProductsComponent } from './seller-cancel-summary-products.component';

describe('SellerCancelSummaryProductsComponent', () => {
  let component: SellerCancelSummaryProductsComponent;
  let fixture: ComponentFixture<SellerCancelSummaryProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerCancelSummaryProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerCancelSummaryProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
