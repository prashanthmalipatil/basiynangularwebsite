import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;

@Component({
  selector: 'app-seller-cancel-summary-products',
  templateUrl: './seller-cancel-summary-products.component.html',
  styleUrls: ['./seller-cancel-summary-products.component.css']
})
export class SellerCancelSummaryProductsComponent implements OnInit {
  currentUser: User;
  assellcancelleddetail: any[] = [];
  assellcancelled:any[]=[];
  loadingnew=true;
  idcancelledsummary:any;
  idbuyer:any;
  idusermatch:any;
  
  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.queryParams.subscribe(params => {
      this.idbuyer=params['buyer_id'],
      this.idcancelledsummary=params['cancel_summary_id'],
      this.idusermatch=params['user_match_id']
      
      });
      console.log(this.idcancelledsummary);
      console.log(this.idbuyer);  
      console.log(this.idusermatch);
  }

  ngOnInit() {

    
    this.loadsellcancelleddetailhistory();
  }

  loadsellcancelleddetailhistory() {
    
    var form = new FormData();
    form.append('user_match_id',this.idusermatch);
    form.append('cancel_summary_id',this.idcancelledsummary);
    form.append('userid', this.currentUser.id);
    form.append('buyer_id',this.idbuyer);

    this.userService.getsellcancelleddetailhistory(form).pipe(first()).subscribe(result => { 
     
        this.assellcancelleddetail= result['cancel_details']; 
        this.assellcancelled=result['order'];
        this.loadingnew=false;
        //console.log(result);
        
    });
  }

}
