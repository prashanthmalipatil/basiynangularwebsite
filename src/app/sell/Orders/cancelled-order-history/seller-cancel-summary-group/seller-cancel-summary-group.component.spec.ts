import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerCancelSummaryGroupComponent } from './seller-cancel-summary-group.component';

describe('SellerCancelSummaryGroupComponent', () => {
  let component: SellerCancelSummaryGroupComponent;
  let fixture: ComponentFixture<SellerCancelSummaryGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerCancelSummaryGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerCancelSummaryGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
