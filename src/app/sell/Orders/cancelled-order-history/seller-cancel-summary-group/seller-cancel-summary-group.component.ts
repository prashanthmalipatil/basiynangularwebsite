import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;
declare var swal:any; 

@Component({
  selector: 'app-seller-cancel-summary-group',
  templateUrl: './seller-cancel-summary-group.component.html',
  styleUrls: ['./seller-cancel-summary-group.component.css']
})
export class SellerCancelSummaryGroupComponent implements OnInit {
  currentUser: User;
  assellcancelled: any[] = [];
  loadingnew=true;
  loading=false;
  usermatchid:any[]=[];
  cancelsummaryid:any[]=[];
  buyerid:any[]=[];
  Searchseller: FormGroup;
  Filterbydates:FormGroup;
    from:any;
    to:any;
    submitted = false;
    searchdata:any;
  constructor(private userService: UserService, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')); }

  ngOnInit() {

        $("#mysearch").keypress(function(){
      $(".clear").show();
     
 });
      $(".clear").hide();
      $("#refresh").hide();
  


   this.checkvalue();
  }
 





  // loadsellcancelledhistory() {
    
  //   var form = new FormData();
  //   form.append('userid', this.currentUser.id);

  //   this.userService.getsellcancelledhistory(form).pipe(first()).subscribe(result => { 
     
  //       this.assellcancelled= result['order']; 
        
  //       this.loadingnew=false;
  //       //console.log(result);
        
  //   });
  // }

  sellercancelledhistory(user){
    console.log(user);
    this.usermatchid=user.user_match_id;
    console.log(this.usermatchid);
    this.cancelsummaryid=user.cancel_summary_id;
    console.log(this.cancelsummaryid);
    this.buyerid=user.buyer_id;
    console.log(this.buyerid);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "user_match_id":user.user_match_id,
        "cancel_summary_id":user.cancel_summary_id,
      "buyer_id":user.buyer_id
     
     
    
      }
      };
      this.router.navigate(['/seller-cancel-summary-products/'],navigationExtras);
  }


  checkvalue(page: number = 1){
    //this.selectedvalue=$( "#group" ).val();
    //this.filterbydd=$("#group-by").val();
    this.from=$("#new1").val();
    this.to=$("#new2").val();
    this.searchdata=$("#mysearch").val();
    console.log(this.from);
    console.log(this.to);
    var form=new FormData();
    
    
  
    if(this.from!=null && this.to!=null){
      
     
      form.append('from', this.from);
      form.append('to', this.to);
    }
  
    
  
    if(this.searchdata!=null){
      form.append('sname', this.searchdata);
    }
    form.append('userid', this.currentUser.id);
    this.userService.getsellcancelledhistory(form).pipe(first()).subscribe(
      data=>{
        //this.asprocessorder= data['order']; 
        this.assellcancelled= data['order']; 

        this.loadingnew=false;
      
      }
    )
  }

  
  Searchbar(){ 
    this.checkvalue();
    
  }
  searchbydates(){
    

    if($("#new1").val()=='' || $("#new2").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new1").val()>$("#new2").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new1").val()<=$("#new2").val()){
      this.checkvalue();
    $("#refresh").show();
    }

    
    //$('#refresh').css("display", "block");
  }
  
  
  
  
  clearsearch(){
    $(".clear").hide();
    $("#mysearch").val('');
    this.checkvalue();
   }
  
   cleardates(){
     $("#refresh").hide();
     $("#new1").val('');
     $("#new2").val('');
     //this.from=[];
     //this.to=[];
     this.checkvalue();
    
   }
}
