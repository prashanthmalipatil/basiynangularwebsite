import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { UserService } from 'src/app/_services';
import { first } from 'rxjs/operators';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { Router } from '@angular/router';
declare var swal:any;
declare var $:any;
@Component({
  selector: 'app-seller-bulk-process-order',
  templateUrl: './seller-bulk-process-order.component.html',
  styleUrls: ['./seller-bulk-process-order.component.css']
})
export class SellerBulkProcessOrderComponent implements OnInit {
  currentUser:User;
  exportdata:any;
csvdata:any[]=[];
jsonnew:any[]=[];
convjson:any[]=[];
convertedcsv:any;
invoice:any;
dispdataintab:any;
  constructor(private userService:UserService, private router: Router) {this.currentUser=JSON.parse(localStorage.getItem('currentUser')) }

  ngOnInit() {
  }

  exportorders(){
    var form=new FormData();
    form.append('userid', this.currentUser.id);
    this.userService.exportorder(form).pipe(first()).subscribe(
      data=>{
        if(data['status']=='true'){
          //console.log(data['order'].length);
          if(data['order'].length!=0){

            //console.log('came to 2nd if');
            this.exportdata=data['order'];

            var exportdata1=this.exportdata.length;
           // console.log(exportdata1);


          for (let i=0; i<exportdata1; i++){
            var datajs={"ProductId": this.exportdata[i]['product_id'], "Product Name": this.exportdata[i]['product_name'], "Buyer Id": this.exportdata[i]['buyer_id'], "Customer Name":this.exportdata[i]['buyer_nickname'],  "Order No":this.exportdata[i]['order_no'], "Order Date":this.exportdata[i]['order_date'], "Deliver by date": this.exportdata[i]['deliver_by_date'], "Outstand Quantity":this.exportdata[i]['outstanding_qty'] }
              this.csvdata.push(datajs);
             }
              //console.log(this.csvdata);

              var options = {
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalseparator: '.',
                showLabels: true,
                showTitle: true,
                useBom: true,
                noDownload: false,
                headers: ["ProductId", "Product name", "Product Id", "Customer Name", "Order No", "Order Date", "Deliver by date", "Outstand Quantity"]
              };
            
            new Angular5Csv(this.csvdata, 'payment_list', options);
          }
          else{
            swal('There Is No products To Export', '', 'error');
          }
        }
        else{
          swal('Sorry something went wrong Please refresh page/ logout and login again', '', 'info');
        }
        //console.log(data);
      }
    )
  }

  
  

  fileEvent(){
    var csv = $("#csvfile1")[0].files[0];
 
  
      var fileReaded = $("#csvfile1")[0].files[0];  
        
      let reader: FileReader = new FileReader();  
      reader.readAsText(fileReaded);  
        
      reader.onload = (e) => {  
      let csv: any = reader.result;  
      let allTextLines = csv.split(/\r|\n|\r/);  
      let headers = allTextLines[0].split(',');  
      let lines = [];  
       
      for (let i = 0; i < allTextLines.length; i++) {  
      // split content based on comma  
      let data = allTextLines[i].split(',');
      console.log(data[i],'');
      if (data.length === headers.length) {  
      let tarr = [];  
      for (let j = 0; j < headers.length; j++) { 
      tarr.push(data[j]);  
      }  
      lines.push(tarr);  
      }  
      }  
      var myobj = ['match_master_id','productname', 'user_match_id', 'custemername',  'order_no', 'order_date', 'deliver_by_date', 'outstanding_qty', 'qty'    ];
      var objArray = [];
      for (var i = 1; i < lines.length; i++) {
        objArray[i - 1] = {};
        for (var k = 0; k < lines[0].length && k < lines[i].length; k++) {
            var key = myobj[k];
            objArray[i - 1][key] = lines[i][k]
        }
    }
    var json = objArray;
    console.log(objArray);
    this.postcsvdata(json);
     
  }
  }
  postcsvdata(uploadedcsvresp){

    console.log(uploadedcsvresp);
    var form= new FormData();
    form.append('userid', this.currentUser.id);
    form.append('product_data', JSON.stringify(uploadedcsvresp));
  this.userService.uploadconcsvdata(form).pipe(first()).subscribe(
    data=>{
      console.log(data['error_message']);
      this.dispdataintab=data['error_message'];
      $('#export').modal();
    }
  )
  }

  //markrtd button redirect
  markrtd(){
    $('#export').modal('hide');
    this.router.navigate(['/seller-rt-dispatch']);
  }


  //export table data
  exportmodaldata(){
    $('#export').modal('hide');
    this.invoice=this.dispdataintab;
          //var exportdata1=this.invoice.length;
          // for (let i=0; i<exportdata1; i++){
          //   var datajs={"Date": this.invoice[i]['Date'], "Customer Name": this.invoice[i]['CustomerName'], "Category": this.invoice[i]['Category'], "Amount":this.invoice[i]['amount'],  "Updated By":'Seller', "Remarks":this.invoice[i]['Remarks'] }
          //     this.csvdata.push(datajs);
          //    }
          //     console.log(this.csvdata);

              var options = {
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalseparator: '.',
                showLabels: true,
                showTitle: true,
                useBom: true,
                noDownload: false,
                headers: ["Product Id", "Product Name", "Customer Id", "Customer Name", "Order No", "Order date", "Dispatch by date", "Outstanding Qty", "Quantity", "Status", "Comment"]
              };
            
            new Angular5Csv(this.invoice, 'payment_list', options);
  }

}
