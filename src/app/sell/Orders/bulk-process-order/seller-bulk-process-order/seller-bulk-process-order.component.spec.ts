import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerBulkProcessOrderComponent } from './seller-bulk-process-order.component';

describe('SellerBulkProcessOrderComponent', () => {
  let component: SellerBulkProcessOrderComponent;
  let fixture: ComponentFixture<SellerBulkProcessOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerBulkProcessOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerBulkProcessOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
