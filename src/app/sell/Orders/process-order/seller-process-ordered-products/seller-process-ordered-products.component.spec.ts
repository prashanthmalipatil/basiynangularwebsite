import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerProcessOrderedProductsComponent } from './seller-process-ordered-products.component';

describe('SellerProcessOrderedProductsComponent', () => {
  let component: SellerProcessOrderedProductsComponent;
  let fixture: ComponentFixture<SellerProcessOrderedProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerProcessOrderedProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerProcessOrderedProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
