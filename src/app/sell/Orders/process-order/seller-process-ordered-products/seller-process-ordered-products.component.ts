import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;
declare var swal:any; 
@Component({
  selector: 'app-seller-process-ordered-products',
  templateUrl: './seller-process-ordered-products.component.html',
  styleUrls: ['./seller-process-ordered-products.component.css']
})
export class SellerProcessOrderedProductsComponent implements OnInit {
  currentUser: User;
  assellprocessorderdetail: any[] = [];
  loadingnew=true;
  showq=true;
  showdata=false;
  Searchseller: FormGroup;
  submitted = false;
  loading=false;
  inputval:any;
  newobjorarr:any[]=new Array();
  inpvalues:any[]=[];
  objnew:any[]=[];
  outval:any;
  givenval:any;
  constructor(private userService: UserService, private route:ActivatedRoute, private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
   
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }




  ngOnInit() {
              $("#mysearch").keypress(function(){
                $(".clear").show();
              
          });
          this.checkboxsel();

          $(".clear").hide();
          this.Searchseller = this.formBuilder.group({
            product_name: ['', Validators.required],
            
          });
              this.loadprocessordersdetail();

            

  }


  get f() { return this.Searchseller.controls; }
  


  loadprocessordersdetail() {
    
    var form = new FormData();
    form.append('userid', this.currentUser.id);

    this.userService.getprocessordersdetail(form).pipe(first()).subscribe(result => { 
      
        this.assellprocessorderdetail= result['order']; 

        this.loadingnew=false;
        
    });
  }


  //checkbox selectc 

  checkboxsel(){

    $("#check").click(function () {
      $('.checkeAll:checkbox').not(this).prop('checked', this.checked); 
  });
  }

  //make ready to dispatch

  dispatch(){
    console.log($(this).data('value'))
    var qtynewdata=new Array();
    qtynewdata=this.inputval
    console.log(qtynewdata);
    var data = new Array();
    
    $('.checkeAll').each(function(){
      if ($(this).prop('checked')==true) {
         this.outval = $(this).data('vals');
         this.givenval = $(this).val();
        
        if(this.outval>=this.givenval){
          console.log(true);
            var objnews= new Object();
            objnews['team_member_id'] = $(this).data('teammember');
            objnews['match_master_id']=$(this).data('masterid');
            objnews['order_no'] = $(this).data('ordno');
            objnews['order_date'] = $(this).data('odate');
            objnews['delivery_status'] = $(this).data('dstatus');
            objnews['qty'] = $(this).val();
            objnews['delivery_by_date'] = $(this).data('ddate');
            objnews['user_match_id'] = $(this).data('umatchid');
            data.push(objnews);
        }
       
      
    }
  })
  if(data.length!=0){
    var form=new FormData();
         
    form.append('userid', this.currentUser.id);
    form.append('product_data', JSON.stringify(data));
  this.userService.markreadytodisp(form).pipe(first()).subscribe(
    data=>{
      swal(data['message'],'','success');
      //console.log(data);
      this.loadprocessordersdetail();
    }
  )
    }
  
 console.log(this.outval);
 console.log(this.givenval);
  if(this.outval<this.givenval){
    console.log('came to first if')
    swal('given value shld not be greater then outstanding val', '', 'error');
  }
  else if(data.length==0){
    console.log('false')
    swal('Please select product before dispatch', '', 'error');
  }
  console.log(data); 
  }

  onchangenew(newValue:any){
    
    this.inputval=newValue; 
  }
 
 
//checkbox

  markdisp(){
    
    if($('#check').not(':checked').length){
      console.log(this.inputval)
      this.showq=true;
    this.showdata=false;
    this.inputval=[];
   }else{
    this.inputval=[];
    console.log(this.inputval)
    this.showq=false;
    this.showdata=true;
   } 
  }

  

}
