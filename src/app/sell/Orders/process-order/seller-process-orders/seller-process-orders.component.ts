import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { UserService, PagerService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { toDate } from '@angular/common/src/i18n/format_date';

declare var $ :any;
declare var swal:any; 
@Component({
  selector: 'app-seller-process-orders',
  templateUrl: './seller-process-orders.component.html',
  styleUrls: ['./seller-process-orders.component.css']
})
export class SellerProcessOrdersComponent implements OnInit {
  currentUser: User;
  asprocessorder: any[] = [];
  outstandingorderquantity: any[]=[];
  outstandingordervalue: any[]=[];
  selectedvalue:any;
  loadingnew=true;
  loading=false;
  Searchseller: FormGroup;
  Filterbydates:FormGroup;
    from:any;
    to:any;
    submitted = false;
    submitteddate = false;
    total: number;
    p: number = 1;
    custname=true;
    filterbytwodate:any;
    searchdata:any;
    filterbydd:any;
    per_page:any;
    // ddate:any;
    // odate:any;
  constructor(private userService: UserService, private pagerService:PagerService,private router: Router,private http: HttpClient, private toastr: ToastrService,private formBuilder: FormBuilder){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));}


    //Pagination
  allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];


  ngOnInit() {
    //this.setPage(1);
    $("#mysearch").keypress(function(){
      $(".clear").show();  
 });

 $(".clear").hide();
  $("#refresh").hide();


  this.checkvalue();

}

  checkvalue(page: number = 1){
    this.selectedvalue=$( "#group" ).val();
    this.filterbydd=$("#group-by").val();
    this.from=$("#new1").val();
    this.to=$("#new2").val();
    this.searchdata=$("#mysearch").val();
    console.log(this.from);
    console.log(this.to);
    var form=new FormData();
    
    if(this.selectedvalue!=null){
      form.append('order_by', this.selectedvalue);
    }

    if(this.from!=null && this.to!=null){
      
      if(this.filterbydd=='ddate'){
      form.append('byfilter', this.filterbydd);
    }
    if(this.filterbydd=='odate'){
      form.append('byfilter', this.filterbydd);

    }
      form.append('from', this.from);
      form.append('to', this.to);
    }

    

    if(this.searchdata!=null){
      form.append('pname', this.searchdata);
    }
    form.append('userid', this.currentUser.id);
    this.userService.outstndorddrop(form).pipe(first()).subscribe(
      data=>{
        //this.asprocessorder= data['order']; 
        this.asprocessorder= data['order']; 
        this.outstandingorderquantity= data['TotaloutStandingQty'];
        this.outstandingordervalue=data['Totaloutstanding_order_amount'];
        this.loadingnew=false;
        this.total = data['products_count'];
        this.per_page=20;
         this.pager = this.pagerService.getPager(this.total, page, this.per_page);
      }
    )
  }

  
   
  
  Searchbar(){
    this.checkvalue();
    
  }
  searchbydates(){
    if($("#new1").val()=='' || $("#new2").val()==''){
      
      swal("please select a from and to dates", '', 'info');
    
    }
    if($("#new1").val()>$("#new2").val()){
      swal("from date should be less than to date", '', 'error');
    }
    if($("#new1").val()<=$("#new2").val()){
      this.checkvalue();
    $("#refresh").show();
    }
  }

  handleSelectedValue(value){
    this.custname=false;
    this.selectedvalue=$( "#group" ).val();
    this.checkvalue();
    
  }


  clearsearch(){
    $(".clear").hide();
    $("#mysearch").val('');
    this.checkvalue();
   }

   cleardates(){
     $("#refresh").hide();
     $("#new1").val('');
     $("#new2").val('');
     //this.from=[];
     //this.to=[];
     this.checkvalue();
    
   }


  // handleSelectedfilter(value){
  //   this.filterbydd=$("#group-by").val();
  //   this.checkvalue();
  // }

  // handleSelectedValue(value){
  //   this.filterbydd=value;
  //   var form= new FormData();
  //   form.append('ddate', this.filterbydd);

  // }
  //     this.Searchseller = this.formBuilder.group({
  //       product_name: ['', Validators.required],
      
  // });
  //     this.Filterbydates = this.formBuilder.group({
  //       fromdate: ['', Validators.required],
  //       todate:['',Validators.required]});
  //       this.loadprocessorders();
       
  // }



  // get f() { return this.Searchseller.controls; }
  

  // onSubmitsearch() {
  //   // console.log("came to submit");
  //   this.submitted = true;
     
  //   // // stop here if form is invalid
  //    if (this.Searchseller.invalid) {
        
  //        //console.log('false data');
  //        return;
  //    }
  //    this.loading = true;
      
  //      var form = new FormData();
      
  //      form.append('userid', this.currentUser.id);
  //     //  form.append('seller_name', this.Searchseller.value.seller_name);
  //       console.log(form);
  //      this.userService.searchprocessorderproductnames(form, this.Searchseller.value.product_name)
  //      .pipe(first())
  //      .subscribe(
  //       data => {
  //         this.outstandingorderquantity= data['TotaloutStandingQty'];
  //         this.outstandingordervalue=data['Totaloutstanding_order_amount'];
  //         this.asprocessorder=data['order'];
       
  //         });

  // }


  // clearsearch(){
  //   $(".clear").hide();
  //   this.asprocessorder=[];
  //   this.outstandingordervalue=[];
  //   this.outstandingorderquantity=[];
  //  this.Searchseller.reset();
  //   this.loadprocessorders();
  //  }

  // loadprocessorders(page: number = 1) {
    
  //   var form = new FormData();
  //   form.append('userid', this.currentUser.id);

  //   this.userService.getprocessorders(form).pipe(first()).subscribe(result => { 
  //     this.outstandingorderquantity= result['TotaloutStandingQty'];
  //     this.outstandingordervalue=result['Totaloutstanding_order_amount'];
  //       this.asprocessorder= result['order']; 
  //       this.loadingnew=false;
  //       this.total = result['products_count'];
  //        this.pager = this.pagerService.getPager(this.total, page);
  //       //console.log(result);
        
  //   });
  // }

  setPage(page: number) {

    if (page < 1 || page > this.pager.total) {
        return;
    }
    this.checkvalue(page);
  }
  processorder(user){
    // console.log(user);
    // this.newsellerid=user.seller_id;
    // console.log(this.newsellerid);
    //this.router.navigate(['/buyer-awaiting-seller-acceptance/'+ this.newmatchid]);
    let navigationExtras: NavigationExtras = {
      queryParams: {
      // "seller_id":user.seller_id
    
      }
      };
      this.router.navigate(['/seller-process-ordered-products/'],navigationExtras);
  }


  // get g(){return this.Filterbydates.controls;}

  // onSubmitdates(){

  //   console.log()
  //   this.submitteddate = true;

  //   if (this.Filterbydates.invalid) {    
  //     return;
  // }
  // this.handleSelectedValue(this.Filterbydates.value);
  // }



  // handleSelectedValue(value){
  //   this.custname=false;
   
  //   this.selectedvalue=value;
  //   console.log(this.selectedvalue);
  //   var form=new FormData();
  //   if(this.Searchseller.value.product_name.length!=null){
  //     form.append('pname', this.Searchseller.value.product_name);
  //   }
  //   if(this.filterbytwodate!=null){
  //     if(this.filterbytwodate=='odate'){
  //       form.append('odate', this.filterbytwodate);
  //     }
  //     else{
  //       form.append('ddate', this.filterbytwodate);
  //     }
  //   }

  //   if(this.Filterbydates!=null){
  //     form.append('from', this.g.fromdate.value);
  //   form.append('to', this.g.todate.value);
  //   }
  //   form.append('userid', this.currentUser.id);
    
  //   this.userService.outstndorddrop(this.selectedvalue,form ).pipe(first()).subscribe(
  //     data=>{
  //       this.asprocessorder=data['order'];
  //     }
  //   )

  // }

  // filterbytwo(value){
  //   this.filterbytwodate=value;
  // }


}
