import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerProcessOrdersComponent } from './seller-process-orders.component';

describe('SellerProcessOrdersComponent', () => {
  let component: SellerProcessOrdersComponent;
  let fixture: ComponentFixture<SellerProcessOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerProcessOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerProcessOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
