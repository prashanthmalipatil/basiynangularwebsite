import { Component, OnInit } from "@angular/core";
import { User } from "../_models";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserService, AuthenticationService } from "../_services";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { first } from "rxjs/operators";
declare var $: any;
@Component({
    templateUrl: 'create-group.component.html',
    styleUrls: ['create-group.component.css'],

})
export class Creategroup implements OnInit{
    loading = false;
    Viewgroup: FormGroup;
    submitted = false;
    returnUrl: string;
    currentUser: User;
    result:string;
    constructor(private formBuilder: FormBuilder,private userService: UserService, private router: Router,private authenticationService: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute,){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }


    ngOnInit(){}
}