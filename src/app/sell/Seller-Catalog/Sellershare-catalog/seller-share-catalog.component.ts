import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { Router, NavigationExtras } from '@angular/router';
declare var $:any;
declare var swal:any;
@Component({
  selector: 'app-seller-share-catalog',
  templateUrl: './seller-share-catalog.component.html',
  styleUrls: ['./seller-share-catalog.component.css']
})
export class SharecatalogComponent implements OnInit {
  currentUser:User;
  Sellerinfo:any[];
  username:any[];
  buyerdata:any[]=[];
  constructor(private userService:UserService, private router: Router ) {
    this.currentUser=JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
    this.loadseller();
  }

  loadseller(){
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    this.userService.sharecatalog(form)
    .pipe(first())
    .subscribe(
      data => {
       
       // this.catalogdata=data['pages'];
        if(status='true'){
          this.Sellerinfo=data['buyers'];
          //console.log(this.Sellerinfo[]);
        }
        else{
          swal('No products Found')
        }
        
    });

  }

  charecatalog(shared){
    
    //console.log(shared);
    this.username=shared.nickname;
   // console.log(shared);
    this.buyerdata=shared;

    swal({
      
      buttons: {
       
        roll: {
          text: "Share all Products",
          value: "true",
        },
        roll1: {
          text: "Share few products",
          value: "false1",
        },
        roll2: {
          text: "Cancel",
          value: false,
        },
      },
          }).then((value) => {
            //console.log(value);
            if (value=='true') {
             // console.log('true')
                      var form = new FormData();
                      form.append('userid', this.currentUser.id);
                      form.append('buyer_id',this.buyerdata['id'] );
                      form.append('user_match_id',this.buyerdata['invite_id'] )
                      this.userService.shareallproducts(form)
                      .pipe(first())
                      .subscribe(
                        data => {
                          console.log(data['status']);
                        // this.catalogdata=data['pages'];
                          if(data['status']== 'true'){
                           swal(data['msg'],'','success')
                           this.loadseller();
                          }
                          else{
                            swal(data['msg']);
                          }
                          
                      });
            }
            else if(value=='false1'){
             // console.log('false')
             let navigationExtras: NavigationExtras = {
              queryParams: {
                  "buyer_id":this.buyerdata['id'],
                  "user_match_id":this.buyerdata['invite_id'],
              }
          };
         
              this.router.navigate(['/seller-sharable-products'], navigationExtras)
            }
            else{
              //console.log('else');
              this.router.navigate(['/seller-share-catalog']);
            }
      
        
        });

}
}
