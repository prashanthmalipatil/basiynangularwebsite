import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerShareCatalogComponent } from './seller-share-catalog.component';

describe('SellerShareCatalogComponent', () => {
  let component: SellerShareCatalogComponent;
  let fixture: ComponentFixture<SellerShareCatalogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerShareCatalogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerShareCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
