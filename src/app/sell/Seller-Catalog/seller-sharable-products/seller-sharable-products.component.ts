import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
declare var swal:any;
declare var $:any;
@Component({
  selector: 'app-seller-sharable-products',
  templateUrl: './seller-sharable-products.component.html',
  styleUrls: ['./seller-sharable-products.component.css']
})
export class SellerSharableProductsComponent implements OnInit {
  buyerid=new Array();
  matchid=new Array();
  sharedpro:any[];
  currentUser:User;
  loadingnew=true;
  constructor(private route: ActivatedRoute,  private userService: UserService,) {

    this.route.queryParams.subscribe(params => {
      this.buyerid=JSON.parse(params['buyer_id']),
      this.matchid=JSON.parse(params['user_match_id'])

      
     });
     console.log(this.buyerid);
     console.log(this.matchid)
     this.currentUser=JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
    this.loadshareableproducts();
  }


  loadshareableproducts(){
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('buyer_id', JSON.stringify(this.buyerid));
    form.append('user_match_id',JSON.stringify(this.matchid));
    //console.log(form);
    this.userService.shareableproducts(form)
    .pipe(first())
    .subscribe(
      data => {
         // console.log(status);
       if(data['status']==true){
        this.loadingnew=false;
        this.sharedpro=data['products'];
        console.log(this.sharedpro.length)
       }
       else{
         swal('no Products found to share');
       }
       
        

     
        
    });
  }


  //share products
  shareproducts(){
    var invite_ids = new Array();
   
    $('.checkeAll').each(function(){
        if ($(this).prop('checked')) {
            var invite_id = $(this).val(); 
            invite_ids.push(invite_id);
          
        }

    });

    if(invite_ids.length!=0){
      
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('buyer_id', JSON.stringify(this.buyerid));
    form.append('user_match_id',JSON.stringify(this.matchid));
    form.append('master_product_id', invite_ids.toString());
    //console.log(form);
    this.userService.shareallcustproducts(form)
    .pipe(first())
    .subscribe(
      data => {
         // console.log(status);
       if(data['status']==true){
         swal(data['msg'], '', 'success');
         this.loadshareableproducts();
       }
       else{
         swal(data['msg'], '', 'info');
       }
       
        

     
        
    });


    }
    else{
      swal('Please select Product to share', '', 'error');
    }
      console.log()
  }
}
