import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerSharableProductsComponent } from './seller-sharable-products.component';

describe('SellerSharableProductsComponent', () => {
  let component: SellerSharableProductsComponent;
  let fixture: ComponentFixture<SellerSharableProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerSharableProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerSharableProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
