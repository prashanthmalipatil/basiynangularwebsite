import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UserService } from 'src/app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-seller-manage-sellingprice',
  templateUrl: './seller-manage-sellingprice.component.html',
  styleUrls: ['./seller-manage-sellingprice.component.css']
})
export class CustomerobjectionComponent implements OnInit {
  currentUser:User;
  loadcustomers:any[];
  constructor(private route: ActivatedRoute,  private userService: UserService, private router: Router) { this.currentUser=JSON.parse(localStorage.getItem('currentUser'));}

  ngOnInit() {
    this.loadcustomerobj();
  }


  //load customer objection
  loadcustomerobj(){
  
    var form=new FormData();
    form.append('userid', this.currentUser.id)
    this.userService.custobjection(form)
    .pipe(first())
    .subscribe(
      data => {
        console.log(data);
        if(data['status']==true){
        this.loadcustomers=data['customers'];
        }
  }
    );

}

//onclick
charecatalog(buyer){
  console.log(buyer['id']);

    let navigationExtras: NavigationExtras = {
      queryParams: {
          "id":buyer['id'],
         
      }
  };
  this.router.navigate(['/seller-manage-selling-price-products'], navigationExtras);
  }

}