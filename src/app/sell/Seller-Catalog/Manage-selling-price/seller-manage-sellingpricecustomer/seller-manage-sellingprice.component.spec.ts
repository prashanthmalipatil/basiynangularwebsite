import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerobjectionComponent } from './seller-manage-sellingprice.component';

describe('CustomerobjectionComponent', () => {
  let component: CustomerobjectionComponent;
  let fixture: ComponentFixture<CustomerobjectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerobjectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerobjectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
