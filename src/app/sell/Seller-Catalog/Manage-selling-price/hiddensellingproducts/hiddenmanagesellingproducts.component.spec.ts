import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HiddenmanagesellingproductsComponent } from './hiddenmanagesellingproducts.component';

describe('HiddenmanagesellingproductsComponent', () => {
  let component: HiddenmanagesellingproductsComponent;
  let fixture: ComponentFixture<HiddenmanagesellingproductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HiddenmanagesellingproductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HiddenmanagesellingproductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
