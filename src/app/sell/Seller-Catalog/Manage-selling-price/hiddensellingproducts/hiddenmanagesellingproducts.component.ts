import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UserService, PagerService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
declare var  $:any;
declare var swal:any;

@Component({
  selector: 'app-hiddenmanagesellingproducts',
  templateUrl: './hiddenmanagesellingproducts.component.html',
  styleUrls: ['./hiddenmanagesellingproducts.component.css']
})
export class HiddenmanagesellingproductsComponent implements OnInit {
  currentUser: User;
  buyerid:any[];
  sellerdata:any;
  p: number = 1;
  total: number;
  loadingnew=true;
  submitted=false;
  revsellprice:FormGroup;
  search:FormGroup;
  per_page:any;
  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private userService: UserService,  private pagerService: PagerService,) { 
    this.currentUser=JSON.parse(localStorage.getItem('currentUser'))
    this.route.queryParams.subscribe(params => {
      this.buyerid=JSON.parse(params['id']);
     

      
     });

  }

     //Pagination
     allItems: any[]=[];

     // pager object
     pager: any = {};
   
     // paged items
     pagedItems: any[];
  ngOnInit() {
    this.loadsellingpricediff();
    $("input.form-control").val("");
    $("#searchpro").keypress(function(){
        $(".clear").show();
       
   });
   $(".clear").hide();
  


    this.setPage(1);
    
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    this.revsellprice = this.formBuilder.group({
      suggested_selling_price: ['', Validators.required],
        
    });

    this.search = this.formBuilder.group({
        'product-name': ['', Validators.required],
        
    });

    

  }

  setPage(page: number) {

    if (page < 1 || page > this.pager.total) {
        return;
    }
  
    this.loadsellingpricediff(page);
  
  }

  //load product diff selling price
  loadsellingpricediff(page:number=1){
    var form=new FormData();
    form.append('userid', this.currentUser.id);
    form.append('buyer_id', JSON.stringify(this.buyerid));
    this.userService.sellingpricobj(form)
    .pipe(first())
    .subscribe(
      data => {
       this.sellerdata=data['products'];
       this.total = data['products_count'];
       this.pager = this.pagerService.getPager(this.total, page, this.per_page);
       this.loadingnew=false;

        }
    );

  }
  get f() { return this.revsellprice.controls; }

  //Confirm
  onSubmit(){
    this.submitted = true;
      
    // stop here if form is invalid
    if (this.revsellprice.invalid) {
        //console.log(this.loginForm.controls);
        return;
    }
    var inpdata=this.f.suggested_selling_price.value;
    var deletedata = new Array();

    $('.sellerinv').each(function(){
        if ($(this).prop('checked')) {
            var value = $(this).val();
            var inputval = $("#row_"+value).val();
            if(inputval!=''){
                $("#getvalue_"+value).html(inputval);
                var invite_id = $(this).val(); 
                var umid = $(this).data('value');
                var deleteProd = {'seller_product_id':invite_id,'buyer_id':umid, "suggested_selling_price":inputval, "update_for_all":0};
                deletedata.push(deleteProd);
            }
              
        }
    });

    if(deletedata.length!=0){
      var form=new FormData();
      form.append('product_data', JSON.stringify(deletedata));
      form.append('status','1');
      form.append('userid', this.currentUser.id);
     
      this.userService.confsellingprice(form).pipe(first())
      .subscribe(
        data => {
           if(data['status']=='true'){
              swal(data['message'],'','success');
              this.loadsellingpricediff();
              $("input.form-control").val("");
              
           }
           else{
               swal('price coudnt be updated', '', 'info');
           }
        }
      );
    }
    
    else{
      swal('Please select at least one product', '', 'error');
    }
    
  }

  //ignore productt
  ignorepro(){
    var inpdata=this.f.suggested_selling_price.value;
    var deletedata = new Array();

    $('.sellerinv').each(function(){
        if ($(this).prop('checked')) {
            var value = $(this).val();
            var inputval = $("#row_"+value).val();
            if(inputval!=''){
                $("#getvalue_"+value).html(inputval);
                var invite_id = $(this).val(); 
                var umid = $(this).data('value');
                var deleteProd = {'seller_product_id':invite_id,'buyer_id':umid, "suggested_selling_price":inputval, "update_for_all":0};
                deletedata.push(deleteProd);
            }
              
        }
    });

    if(deletedata.length!=0){
      var form=new FormData();
      form.append('product_data', JSON.stringify(deletedata));
      form.append('status','2');
      form.append('userid', this.currentUser.id);
     
      this.userService.confsellingprice(form).pipe(first())
      .subscribe(
        data => {
           if(data['status']=='true'){
              swal(data['message'],'','success');
              this.loadsellingpricediff();
              $("input.form-control").val("");
              
           }
           else{
               swal('price coudnt be updated', '', 'info');
           }
        }
      );
    }
    
    else{
      swal('Please select at least one product', '', 'error');
    }

  }


  //search

  searchsub(page: number = 1){


    var form=new FormData();
       
    form.append('filters', JSON.stringify(this.search.value));
    form.append('userid', this.currentUser.id);
    form.append('buyer_id', JSON.stringify(this.buyerid));
   
    this.userService.seachprodcust(form).pipe(first())
    .subscribe(
      data => {
        
       this.sellerdata=data['products'];
      // console.log(this.loadsellingpricediff)
       this.total = data['products_count'];
       this.per_page=20;
           this.pager = this.pagerService.getPager(this.total, page, this.per_page);
      }
    );
}


clearsearh(){
  $(".clear").hide();
   this.loadsellingpricediff();
}
}
