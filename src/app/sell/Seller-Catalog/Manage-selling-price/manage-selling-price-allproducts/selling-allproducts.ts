import { Component, OnInit } from "@angular/core";
import { UserService, PagerService } from "src/app/_services";
import { Router, ActivatedRoute } from "@angular/router";
import { User } from "src/app/_models";
import { first } from "rxjs/operators";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
declare var swal:any;
declare var $:any;
@Component({
    templateUrl:'selling-allproducts.html',
    styleUrls:['selling-allproducts.css']
})

export class SellingallproductsComponent implements OnInit{
    currentuser:User;
    buyerid:any[];
    loadcustomerproducts:any[];
    p: number = 1;
    total: number;
    loadingnew=true;
    submitted=false;
    revsellprice:FormGroup;
    search:FormGroup;
    product_id:any;
    per_page:any;
    constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router, private pagerService: PagerService, private route: ActivatedRoute){
        this.route.queryParams.subscribe(params => {
            this.buyerid=JSON.parse(params['id']);
           
     
            
           });

           this.currentuser=JSON.parse(localStorage.getItem('currentUser'));
    }

     //Pagination
  allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];
    ngOnInit(){
        $("input.form-control").val("");
        $("#searchpro").keypress(function(){
            $(".clear").show();
           
       });
       $(".clear").hide();
      


        this.setPage(1);
        this.loadbyerproducts();
        $("#checkAll").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        this.revsellprice = this.formBuilder.group({
            suggested_selling_price: ['', Validators.required],
            
        });

        this.search = this.formBuilder.group({
            'product-name': ['', Validators.required],
            
        });

        
    }
    

    //load buyer all products
    loadbyerproducts(page: number = 1){
        $("input.form-control").val("");
        var form=new FormData();
        form.append('buyer_id', JSON.stringify(this.buyerid));
        form.append('userid', this.currentuser.id);
       
        this.userService.buyershareprodu(form, page).pipe(first())
        .subscribe(
          data => {
              console.log(data['products_count']);
              if(data['products_count']!=0){
                  this.loadingnew=false;
                this.loadcustomerproducts=data['products'];
                this.total = data['products_count'];
                this.per_page=20;
                this.pager = this.pagerService.getPager(this.total, page, this.per_page);
          }
          else{
             // swal('No connected products with this customers. You can share product with the customer first..','','info');

              swal({
                text:"No connected products with this customers. You can share product with the customer first..",
                buttons: {
                 
                  roll: {
                    text: "ok",
                    value: "true",
                  },
                 
                },
                    }).then((value) => {
                      //console.log(value);
                      if (value=='true') {
                       // console.log('true')
                       this.router.navigate(['/seller-share-catalog'])
                      }
                  
                  });
              
          }
        }
        );
    }

    setPage(page: number) {

        if (page < 1 || page > this.pager.total) {
            return;
        }
      
        this.loadbyerproducts(page);
      
      }

      get f() { return this.revsellprice.controls; }


     
            
        
      
      //on confirm rev selling price
      onSubmit(){
        this.submitted = true;
      
        // stop here if form is invalid
        if (this.revsellprice.invalid) {
            //console.log(this.loginForm.controls);
            return;
        }
        var inpdata=this.f.suggested_selling_price.value;
        var deletedata = new Array();
   
        $('.sellerinv').each(function(){
            if ($(this).prop('checked')) {
                var value = $(this).val();
                var inputval = $("#row_"+value).val();
                if(inputval!=''){
                    $("#getvalue_"+value).html(inputval);
                    var invite_id = $(this).val(); 
                    var umid = $(this).data('value');
                    var deleteProd = {'seller_product_id':invite_id,'buyer_id':umid, "suggested_selling_price":inputval, "update_for_all":0};
                    deletedata.push(deleteProd);
                }
                  
            }
        });
        // console.log(deletedata);
        // console.log(this.revsellprice.value)
        
       
        if(deletedata.length!=0){
        var form=new FormData();
        form.append('product_data', JSON.stringify(deletedata));
        
        form.append('userid', this.currentuser.id);
       
        this.userService.revsellingprice(form).pipe(first())
        .subscribe(
          data => {
             if(data['status']=='true'){
                swal(data['message'],'','success');
                this.loadbyerproducts();
                $("input.form-control").val("");
                
             }
             else{
                 swal('price coudnt be updated', '', 'info');
             }
          }
        );
      }
      else{
          swal('Please select any product to update price', '', 'error');
      }
    }


    searchsub(page: number = 1){
      
         console.log(this.search.value);

         var form=new FormData();
            
         form.append('filters', JSON.stringify(this.search.value));
         form.append('userid', this.currentuser.id);
         form.append('buyer_id', JSON.stringify(this.buyerid));
        
         this.userService.seachprod(form).pipe(first())
         .subscribe(
           data => {
            this.loadcustomerproducts=data['products'];
            this.total = data['products_count'];
            this.per_page=20;
                this.pager = this.pagerService.getPager(this.total, page, this.per_page);
           }
         );
    }


    clearsearh(){
        this.loadbyerproducts();
    }
    }
