import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { UserService } from 'src/app/_services';
declare var swal:any;
@Component({
  selector: 'app-customer-specific-pricing',
  templateUrl: './customerspecific-pricing.component.html',
  styleUrls: ['./customerspecific-pricing.component.css']
})
export class specificpricComponent implements OnInit {
  currentUser:User;
  loadbuyers:any[];
  constructor(private route: ActivatedRoute,  private userService: UserService, private router: Router) {this.currentUser=JSON.parse(localStorage.getItem('currentUser')); }

  ngOnInit() {
    this.loadspecpricing();
  }

  loadspecpricing(){
    var form=new FormData();
    form.append('userid',this.currentUser.id);
    this.userService.specificpricinig(form)
    .pipe(first())
    .subscribe(
      data => {
        console.log(data);
        if(data['status']==true){
          this.loadbuyers=data['buyers'];
          console.log(this.loadbuyers)
        }
        else{
          swal('No user found','','info');
        }
      }
      
    );
  }


  //on click on user
  charecatalog(buyer){
    console.log(buyer['id']);

    let navigationExtras: NavigationExtras = {
      queryParams: {
          "id":buyer['id'],
         
      }
  };
  this.router.navigate(['/seller-manage-selling-price-all-products'], navigationExtras);
  }

}
