import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { specificpricComponent } from './customerspecific-pricing.component';



describe('CustomerSpecificPricingComponent', () => {
  let component: specificpricComponent;
  let fixture: ComponentFixture<specificpricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ specificpricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(specificpricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
