import { Component, OnInit } from '@angular/core';
import { UserService, PagerService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
declare var $:any;
declare var swal:any;
@Component({
  selector: 'app-from-basiyn-catalog',
  templateUrl: './from-basiyn-catalog.component.html',
  styleUrls: ['./from-basiyn-catalog.component.css']
})
export class SellerBasiyncatalogComponent implements OnInit {
  currentUser:User;
  total: number;
  p: number = 1;
  basiyncatdata:any[];
  loadingnew=true;
  per_page:any;
  constructor(private userService: UserService, private pagerService: PagerService) {
    this.currentUser=JSON.parse(localStorage.getItem('currentUser'));
    
   }
     //Pagination
  allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];


  ngOnInit() {
    this.setPage(1);
    this.frombasiyncatalog();
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });
  }


  setPage(page: number) {

    if (page < 1 || page > this.pager.total) {
        return;
    }
  
    this.frombasiyncatalog(page);
  
  }
  //load data on page load

  frombasiyncatalog(page: number = 1){
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    //form.append('product_ids', )
    this.userService.frombasiynsellside(form,page)
    .pipe(first())
    .subscribe(
      data => {
        this.loadingnew=false;
        console.log(data);
        this.basiyncatdata=data['products'];
        this.total = data['products_count'];
        this.per_page=20;
         this.pager = this.pagerService.getPager(this.total, page, this.per_page);
       
       
        
    });
  }

  //update price on click of next
  updateprice(){
    var invite_ids = new Array();
   
    $('.sellerinv').each(function(){
        if ($(this).prop('checked')) {
            var invite_id = $(this).val(); 
            invite_ids.push(invite_id);
          
        }

    });
    console.log(invite_ids);
  }

}
