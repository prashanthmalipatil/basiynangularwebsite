import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FromBASIYNCatalogComponent } from './from-basiyn-catalog.component';

describe('FromBASIYNCatalogComponent', () => {
  let component: FromBASIYNCatalogComponent;
  let fixture: ComponentFixture<FromBASIYNCatalogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FromBASIYNCatalogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FromBASIYNCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
