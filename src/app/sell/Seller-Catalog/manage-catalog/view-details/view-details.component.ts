
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/_services';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
declare var $:any;
@Component({
  selector: 'app-view-details',
  templateUrl: './view-details.component.html',
  styleUrls: ['./view-details.component.css']
})

export class ViewDetailsComponent implements OnInit {
    user_Rec:any;
    
    details:[]=[];
    masterproid:[]=[];
    storedetails:any[]=[];
    masterproductname:any[]=[];
    variantdata:any[]=[];
   
  constructor( private route: ActivatedRoute,  private userService: UserService,) { 
    // this.variantdetails = new FormGroup({
    //     masterproductname: new FormControl('', Validators.required),
    // });
   
    this.route.queryParams.subscribe(params => {
       this.details=params['userid']
       this.masterproid=params['master_product_id']

       
      });
     

}



  ngOnInit() {

    this.loadviewdetails(); 
    
}


submit(ngForm) {
  console.log(ngForm);
}
//load details in form using api

loadviewdetails(){
    var form = new FormData();
    form.append('userid', JSON.stringify(this.details));
    form.append('master_product_id', JSON.stringify(this.masterproid));
    //console.log(form);
    this.userService.loaddetails(form)
    .pipe(first())
    .subscribe(
      data => {
         // console.log(status);
          this.masterproductname=data;
        this.storedetails=data['masterproduct_data'];

        for(var i=0; i<this.storedetails.length; i++){
          this.variantdata=data['masterproduct_data'][i]['group_product_data']
        }
        

     
        
    });
   
}





}