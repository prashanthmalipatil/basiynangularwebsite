import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder } from '@angular/forms';
import { UserService, PagerService } from 'src/app/_services';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $ :any;
declare var swal:any;
@Component({
  selector: 'app-manage-catalog',
  templateUrl: './manage-catalog.component.html',
  styleUrls: ['./manage-catalog.component.css']
})
export class ManageCatalogComponent implements OnInit {
  employees:Employee[]=[];
  currentUser: User;
  catalogdata:any[];
  checkcatdata:any[];
  total: number;
  p: number = 1;
  loadingnew=true;
  salemrp:any=[];
  navid:any[];
  per_page:any;
  datacount:any[]=[];
  // salesellingprice:Object={};
  
  constructor(private formBuilder: FormBuilder, private pagerService: PagerService,private userService: UserService, private router: Router, private route: ActivatedRoute) {  this.currentUser = JSON.parse(localStorage.getItem('currentUser'));}

  //Pagination
  allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  ngOnInit() {
   
    //this.employees=[{name:"test", selling:'string'} ,{name:"new", selling:'new1'}]
    this.setPage(1);
    this.loadproduct();
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });
  }


  submit(NgForm) {
    // console.log(NgForm);
    // console.log(NgForm.master_product_id);
    // console.log(NgForm.sell_price);
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('master_product_id', NgForm.master_product_id);
    form.append('selling_price',NgForm.sell_price);
    form.append('sales_mrp',NgForm.sales_mrp);

    this.userService.updateprice(form)
    .pipe(first())
    .subscribe(
      data => {
      

        if(status='true'){
          this.salemrp=data;
         
          swal(this.salemrp.message, "", "success");
          this.loadproduct();
         
        }
        else{
          swal(this.salemrp.message)
        }
        
    });

    
  }



  //load data on page load using userid
  loadproduct(page: number = 1){
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    this.userService.loadproducts(form, page)
    .pipe(first())
    .subscribe(
      data => {
       
       // this.catalogdata=data['pages'];
        if(status='true'){
          this.loadingnew=false;
          this.checkcatdata=data['catelog_data']['data'];
          //this.datacount=data['products_count'];
          //console.log(this.datacount);
         //console.log(this.checkcatdata.length);
          this.total = data['products_count'];
          this.per_page=20;
         this.pager = this.pagerService.getPager(this.total, page, this.per_page);

        }
        else{
          swal('No products Found')
        }
        
    });

  }

  //pagination
  
  setPage(page: number) {

  if (page < 1 || page > this.pager.total) {
      return;
  }

  this.loadproduct(page);

}

  //date selection popup
  seldate(){
    $("#seldate").modal();
  }
  selbrand(){
    $("#selbrand").modal();
  }

  //after date selection if clicked on apply
  datefilter(){
    $("#seldate").modal('hide');
  }

  //after brand selection and clicked on apply 
  brandfilter(){
    $("#selbrand").modal('hide');
  }


  

  //delete product
  deleteproduct(){
    var invite_ids = new Array();
   
    $('.sellerinv').each(function(){
        if ($(this).prop('checked')) {
            var invite_id = $(this).val(); 
            invite_ids.push(invite_id);
          
        }

    });

    if(invite_ids.length!=0){
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    form.append('product_ids', JSON.stringify(invite_ids))
    this.userService.deleteproduct(form)
    .pipe(first())
    .subscribe(
      data => {
        this.salemrp=data;
       // this.catalogdata=data['pages'];
       // console.log(this.catalogdata);
        if(status=='true'){
         swal(this.salemrp.message, "","success");
         this.loadproduct();
        }
        else if(status=='false'){
          swal(this.salemrp.message)
        }
        else{
          swal("something went wrong","", "warning")
        }
        
    });


  }
  else{
      swal('Select Product Before Deleting', "", "warning");
  }

  
  
}


//view details
viewdetails(employee){
  // console.log(employee.master_product_id);
  // console.log(this.currentUser.id)
//   this.navid=employee.product_id
  let navigationExtras: NavigationExtras = {
    queryParams: {
        "master_product_id":employee.master_product_id,
        "userid":this.currentUser.id,
    }
};
this.router.navigate(['/view-details'], navigationExtras);


  //this.router.navigate(['/view-details/:'+this.navid]);
}

}
export class Employee{
  name:string;
  selling:string;
}

