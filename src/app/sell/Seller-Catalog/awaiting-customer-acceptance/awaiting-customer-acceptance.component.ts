import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { UserService } from 'src/app/_services';
import { first } from 'rxjs/operators';
import { NavigationExtras, Router } from '@angular/router';
declare var swal:any;
@Component({
  selector: 'app-awaiting-customer-acceptance',
  templateUrl: './awaiting-customer-acceptance.component.html',
  styleUrls: ['./awaiting-customer-acceptance.component.css']
})
export class AwaitingCustomerAcceptanceComponent implements OnInit {
  currentUser:User;
  customers:any;

  constructor(private userService:UserService,private router: Router ) {this.currentUser=JSON.parse(localStorage.getItem('currentUser')) }

  ngOnInit() {
    this.loadcustomeracceptance();
  }

  loadcustomeracceptance(){
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    this.userService.loadcustacceptance(form)
    .pipe(first())
    .subscribe(
      data => {
        if(data['status']==true){
      this.customers=data['customers'];
        }
        else{
          swal('No data found/something went wrong','', 'info');
        }

  }
    );
}

charecatalog(customer){
  console.log(customer['invite_id']);

    let navigationExtras: NavigationExtras = {
      queryParams: {
          "id":customer['invite_id'],
         
      }
  };
  this.router.navigate(['/seller-awaiting-customer-acceptance-products'], navigationExtras);
  
}

}
