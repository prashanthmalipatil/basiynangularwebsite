import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwaitingCustomerAcceptanceComponent } from './awaiting-customer-acceptance.component';

describe('AwaitingCustomerAcceptanceComponent', () => {
  let component: AwaitingCustomerAcceptanceComponent;
  let fixture: ComponentFixture<AwaitingCustomerAcceptanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwaitingCustomerAcceptanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwaitingCustomerAcceptanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
