import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwCustomerProductsComponent } from './aw-customer-products.component';

describe('AwCustomerProductsComponent', () => {
  let component: AwCustomerProductsComponent;
  let fixture: ComponentFixture<AwCustomerProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwCustomerProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwCustomerProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
