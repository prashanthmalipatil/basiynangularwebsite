import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService, PagerService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
declare var swal:any;
declare var $:any;
@Component({
  selector: 'app-awating-customer-products',
  templateUrl: './aw-customer-products.component.html',
  styleUrls: ['./aw-customer-products.component.css']
})
export class AwCustomerProductsComponent implements OnInit {
  buyerid:any;
  currentUser:User;
  loadproducts:any;
  total: number;
  p: number = 1;
  search:FormGroup;
  pname:any;
  per_page:any;
  matchid:any[]=[];
  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private userService: UserService, private pagerService: PagerService) {
    this.route.queryParams.subscribe(params => {
      this.buyerid=JSON.parse(params['id']);
     

      
     });
     this.currentUser=JSON.parse(localStorage.getItem('currentUser'));
   }


    //Pagination
  allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  ngOnInit() {
    this.loadsellingpricediff();
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });


  this.search = this.formBuilder.group({
    search: ['', Validators.required],
      
  });
  }

  setPage(page: number) {

    if (page < 1 || page > this.pager.total) {
        return;
    }
  
    this.loadsellingpricediff(page);
  
  }
  

  loadsellingpricediff(page: number = 1){
    var form=new FormData();
    form.append('userid', this.currentUser.id);
    form.append('user_match_id', JSON.stringify(this.buyerid));
    this.userService.awaitingcustproducts(form)
    .pipe(first())
    .subscribe(
      data => {
        console.log(data);
        if(data['status']==true){
          this.loadproducts=data['products'];
          this.matchid=this.loadproducts[0]['user_match_id'];
          console.log(this.matchid)
          this.total = data['products_count'];
          this.per_page=20;
         this.pager = this.pagerService.getPager(this.total, page, this.per_page);
        }
        else{
          swal('No products found', '', 'info');
        }
        }
    );

  }

  delete(){
    var deletedata = new Array();
   
    $('.sellerinv').each(function(){
        if ($(this).prop('checked')) {
            var invite_id = $(this).val(); 
            var umid = $(this).data('value');
            var deleteProd = {'product_id':invite_id,'user_match_id':umid};
            deletedata.push(deleteProd);  
        }
    });

    console.log(deletedata);

    if(deletedata.length!=0){
    var form=new FormData();
    form.append('seller_id', this.currentUser.id);
    form.append('sharedata', JSON.stringify(deletedata));
    this.userService.deletecustawtproducts(form)
    .pipe(first())
    .subscribe(
      data => {
        if (data['status']=='success'){
        swal(data['message'], '', 'success');
        this.loadsellingpricediff();

        }
        else{
          swal('product/s couldn be deleted','', 'error' );
        }
      }
    );

  }
else{
  swal('Please select atleast one product to delete', '', 'error');
}
}

get f() { return this.search.controls; }
//search
searchsub(){
  console.log(this.loadproducts['user_match_id'])
 console.log(this.matchid)
 this.pname=this.f.search.value
  var form=new FormData();
      
  form.append('user_match_id', JSON.stringify(this.matchid));
  form.append('userid', this.currentUser.id);

 
  this.userService.searchawating(form,this.pname).pipe(first())
  .subscribe(
    data => {
      console.log(data);
      this.loadproducts=data['products'];

    }
  );
}

clearsearchcustomer(){
  $(".clear").hide();
  this.loadsellingpricediff();
}

}
