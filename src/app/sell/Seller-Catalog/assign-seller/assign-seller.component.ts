import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
declare var swal:any;
declare var $:any;
@Component({
  selector: 'app-assign-seller',
  templateUrl: './assign-seller.component.html',
  styleUrls: ['./assign-seller.component.css']
})
export class AssignSellerComponent implements OnInit {
  currentUser:User;
  assignsupp:any;
  loadingnew=true;
  search:FormGroup;
  constructor(private userService:UserService,private formBuilder:FormBuilder, private router: Router) {this.currentUser=JSON.parse(localStorage.getItem('currentUser')) }

  ngOnInit() {
    this.asssupplier();

    
  this.search = this.formBuilder.group({
    search: ['', Validators.required],
      
  });

  }


  //load asssign supplierr
  asssupplier(){
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    this.userService.loadasssupplier(form)
    .pipe(first())
    .subscribe(
      data => {
        this.loadingnew=false;
        this.assignsupp=data['results'];
      }
    );
  }

  assnext(){
    var invite_ids = new Array();
    $('.sellerinv').each(function(){
      if ($(this).prop('checked')) {
          var invite_id = $(this).val(); 
          invite_ids.push(invite_id);
        
      }

  });

  if(invite_ids.length!=0){
    swal({
      
      buttons: {
       
        roll: {
          text: "view products that are not yet assigned to any supplier",
          value: "true",
        },
        roll1: {
          text: "view products that are already assigned to some other supplier",
          value: "false1",
        },
        roll2: {
          text: "Cancel",
          value: false,
        },
      },
          }).then((value) => {
            //console.log(value);
            if (value=='true') {
              let navigationExtras: NavigationExtras = {
                queryParams: {
                    "status":"0",
                    "seller":invite_ids,
                }
            };
              this.router.navigate(['/assign-seller-product-variant'], navigationExtras);     
              
            }
            else if(value=='false1'){
              let navigationExtras: NavigationExtras = {
                queryParams: {
                    "status":"1",
                    "seller":invite_ids,
                }
            };
              this.router.navigate(['/assign-seller-product-variant'], navigationExtras);
  }
  else{

  }
});
  }
  else{
    swal('Please select one Supplier', '', 'error');
  }
}
}
