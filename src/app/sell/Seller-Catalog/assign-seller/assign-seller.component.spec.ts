import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignSellerComponent } from './assign-seller.component';

describe('AssignSellerComponent', () => {
  let component: AssignSellerComponent;
  let fixture: ComponentFixture<AssignSellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignSellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
