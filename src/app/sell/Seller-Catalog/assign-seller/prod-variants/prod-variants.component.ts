import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService, PagerService } from 'src/app/_services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { User } from 'src/app/_models';
declare var $:any;
declare var swal:any;
@Component({
  selector: 'app-prod-variants',
  templateUrl: './prod-variants.component.html',
  styleUrls: ['./prod-variants.component.css']
})
export class ProdVariantsComponent implements OnInit {
  status:any;
  seller:any;
  search:FormGroup;
  currentUser:User;
  products:any;
  total: number;
  p: number = 1;
  loadingnew=true;
  per_page:any;
  constructor(private route: ActivatedRoute, private formBuilder:FormBuilder,private pagerService: PagerService, private userService: UserService,) { 
    this.route.queryParams.subscribe(params => {
      this.seller=params['seller']
      this.status=params['status']
      console.log(this.seller);
      console.log(this.status);
      
     });
     this.currentUser=JSON.parse(localStorage.getItem('currentUser'))
  }

    //Pagination
  allItems: any[]=[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];


  ngOnInit() {
    this.loadprodstatone();
    this.search = this.formBuilder.group({
      search: ['', Validators.required],
        
    });
    this.setPage(1);

    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });
  }

  loadprodstatone(page: number = 1){
    var form = new FormData();
        form.append('userid', this.currentUser.id);
        
        this.userService.prodvariants(form, this.seller,this.status)
        .pipe(first())
        .subscribe(
          data => {
            this.loadingnew=false;
            this.products=data['products']
            //console.log(this.checkcatdata.length);
          this.total = data['products_count'];
          this.per_page=20;
          this.pager = this.pagerService.getPager(this.total, page, this.per_page);
 
          }
        );


      
  }
  setPage(page: number) {

    if (page < 1 || page > this.pager.total) {
        return;
    }
  
    this.loadprodstatone(page);
  
  }


  notifyseller(){
    var invite_ids = new Array();
   
    $('.sellerinv').each(function(){
        if ($(this).prop('checked')) {
            var invite_id = $(this).val(); 
            invite_ids.push(invite_id);
          
        }

    });
   
    if(invite_ids.length!=0){

    }
    else{
      swal("Please select atleast one product", "","error");
    }
  }
}
