import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdVariantsComponent } from './prod-variants.component';

describe('ProdVariantsComponent', () => {
  let component: ProdVariantsComponent;
  let fixture: ComponentFixture<ProdVariantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdVariantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdVariantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
