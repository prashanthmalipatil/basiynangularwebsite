import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { UserService } from 'src/app/_services';
import { Router, NavigationExtras } from '@angular/router';
import { first } from 'rxjs/operators';
declare var swal:any;
@Component({
  selector: 'app-customer-catalog-request',
  templateUrl: './customer-catalog-request.component.html',
  styleUrls: ['./customer-catalog-request.component.css']
})
export class CustomerCatalogRequestComponent implements OnInit {
  currentUser:User;
  customerinfo:any[];
  custinfo:any[];
  constructor(private userService:UserService, private router: Router ) { 
    this.currentUser=JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.loadcustomer();

  }

  loadcustomer(){
    var form = new FormData();
    form.append('userid', this.currentUser.id);
    this.userService.sharecatalogrequest(form)
    .pipe(first())
    .subscribe(
      data => {
       
       // this.catalogdata=data['pages'];
        if(status='true'){
          this.customerinfo=data['buyers'];
          console.log(this.customerinfo);
        }
        else{
          swal('No products Found')
        }
        
    });

  }

  charecatalog(shared){
    this.custinfo=shared;
    console.log(shared);
    swal({
      
      buttons: {
       
        roll: {
          text: "Share all Products",
          value: "true",
        },
        roll1: {
          text: "Share few products",
          value: "false1",
        },
        roll2: {
          text: "Cancel",
          value: false,
        },
      },
          }).then((value) => {
            //console.log(value);
            if (value=='true') {
             // console.log('true')
                      var form = new FormData();
                      form.append('userid', this.currentUser.id);
                      form.append('buyer_id',this.custinfo['buyer_id'] );
                      form.append('user_match_id',this.custinfo['user_match_id'] )
                      this.userService.shareallproducts(form)
                      .pipe(first())
                      .subscribe(
                        data => {
                          console.log(data['status']);
                        // this.catalogdata=data['pages'];
                          if(data['status']==true){
                           swal(data['msg'],'','success')
                           this.loadcustomer();
                          }
                          else{
                            swal(data['msg']);
                          }
                          
                      });
            }
            else if(value=='false1'){
             // console.log('false')
             let navigationExtras: NavigationExtras = {
              queryParams: {
                  "buyer_id":this.custinfo['buyer_id'],
                  "user_match_id":this.custinfo['user_match_id'],
              }
          };
         
              this.router.navigate(['/seller-sharable-products'], navigationExtras)
              //this.router.navigate(['/seller-sharable-products'])
            }
            else{
              //console.log('else');
              this.router.navigate(['/seller-share-catalog']);
            }
      
        
        });

  }

}
