import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerCatalogRequestComponent } from './customer-catalog-request.component';

describe('CustomerCatalogRequestComponent', () => {
  let component: CustomerCatalogRequestComponent;
  let fixture: ComponentFixture<CustomerCatalogRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerCatalogRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerCatalogRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
