import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerVariantSettingComponent } from './seller-variant-setting.component';

describe('SellerVariantSettingComponent', () => {
  let component: SellerVariantSettingComponent;
  let fixture: ComponentFixture<SellerVariantSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerVariantSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerVariantSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
