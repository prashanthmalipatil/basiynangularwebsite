import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerManageVariantComponent } from './seller-manage-variant.component';

describe('SellerManageVariantComponent', () => {
  let component: SellerManageVariantComponent;
  let fixture: ComponentFixture<SellerManageVariantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerManageVariantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerManageVariantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
