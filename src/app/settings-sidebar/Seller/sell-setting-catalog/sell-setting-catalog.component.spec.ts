import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellSettingCatalogComponent } from './sell-setting-catalog.component';

describe('SellSettingCatalogComponent', () => {
  let component: SellSettingCatalogComponent;
  let fixture: ComponentFixture<SellSettingCatalogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellSettingCatalogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellSettingCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
