import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellSettingOtherComponent } from './sell-setting-other.component';

describe('SellSettingOtherComponent', () => {
  let component: SellSettingOtherComponent;
  let fixture: ComponentFixture<SellSettingOtherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellSettingOtherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellSettingOtherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
