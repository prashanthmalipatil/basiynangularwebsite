import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellSettingOrderComponent } from './sell-setting-order.component';

describe('SellSettingOrderComponent', () => {
  let component: SellSettingOrderComponent;
  let fixture: ComponentFixture<SellSettingOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellSettingOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellSettingOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
