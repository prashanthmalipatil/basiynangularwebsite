import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuySettingCatalogComponent } from './buy-setting-catalog.component';

describe('BuySettingCatalogComponent', () => {
  let component: BuySettingCatalogComponent;
  let fixture: ComponentFixture<BuySettingCatalogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuySettingCatalogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuySettingCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
