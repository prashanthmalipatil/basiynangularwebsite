import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuySettingOrderComponent } from './buy-setting-order.component';

describe('BuySettingOrderComponent', () => {
  let component: BuySettingOrderComponent;
  let fixture: ComponentFixture<BuySettingOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuySettingOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuySettingOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
