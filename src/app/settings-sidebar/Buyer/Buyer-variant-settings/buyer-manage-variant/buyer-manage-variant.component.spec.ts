import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerManageVariantComponent } from './buyer-manage-variant.component';

describe('BuyerManageVariantComponent', () => {
  let component: BuyerManageVariantComponent;
  let fixture: ComponentFixture<BuyerManageVariantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerManageVariantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerManageVariantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
