import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyVariantSettingComponent } from './buy-variant-setting.component';

describe('BuyVariantSettingComponent', () => {
  let component: BuyVariantSettingComponent;
  let fixture: ComponentFixture<BuyVariantSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyVariantSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyVariantSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
