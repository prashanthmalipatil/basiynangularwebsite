import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerVariantSettingComponent } from './buyer-variant-setting.component';

describe('BuyerVariantSettingComponent', () => {
  let component: BuyerVariantSettingComponent;
  let fixture: ComponentFixture<BuyerVariantSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerVariantSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerVariantSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
