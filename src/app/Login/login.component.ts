import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, AbstractControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationService, UserService } from "../_services";
import { first } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { result } from "underscore";
import { User } from "../_models";

declare var $ :any;
declare var swal:any; 

@Component({
    templateUrl: 'login.component.html',
    styleUrls:['login.component.css']
})
 export class Logincomponent implements OnInit{
    loginForm: FormGroup;
    Forgotpasss:FormGroup;
    otpform:FormGroup;
    Passfields:FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    userdetails:any=[];
    result:string;
    currentUser: User;
    userid = '';
     constructor(private formBuilder: FormBuilder,private userService: UserService, private router: Router,private authenticationService: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute,)
     {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
     }

     ngOnInit(){
        this.loginForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.Forgotpasss= this.formBuilder.group({
            mobile:['', Validators.required],
        })

        this.otpform= this.formBuilder.group({
            otp:['', Validators.required],
        })

        this.Passfields= this.formBuilder.group({
            password:['', Validators.required],
            confpassword:['', Validators.required],
        })

        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'dashboard';

       
       
     }
     get f() { return this.loginForm.controls; }
     get g() { return this.Forgotpasss.controls; }
     get h() { return this.otpform.controls; }
     get i() { return this.Passfields.controls; }

     navigate(){
         //console.log("came to nav")
        this.router.navigate([this.returnUrl]);
     }
    


    onSubmit() {
       
        this.submitted = true;
      
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            //console.log(this.loginForm.controls);
            return;
        }

        this.loading = true;
        //console.log(this.loginForm.controls);
        this.authenticationService.login(this.f.email.value, this.f.password.value)
            .pipe(first())
            
            .subscribe(
               
                    data => {
                        console.log(data.status);
                        if(data.status == 'true'){
                        
                        console.log('data');
                        this.toastr.success('success');
                        // this.router.navigate(['/dashboard']);
                        this.navigate();
                            
                        }
                        else{
                            this.toastr.error(data.result);
                            //console.log(data.status);
                             //this.toastr.success(data.status);
                             console.log('false')
                        }
                        
                      
                    });
  
    }



    forgotpas(){
        $("#ex1").modal();
    }

    //mobile no submit
    
    onSubmitpass(){
        this.submitted = true;
      
        // // stop here if form is invalid
         if (this.Forgotpasss.invalid) {
        //     //console.log(this.loginForm.controls);
             return;
         }

         this.loading = true;
            
        var formotp = new FormData();
        formotp.append('mobile', this.Forgotpasss.value.mobile);
        

        $.ajax({
            url:'https://dev.basiyn.com/forget-password.php',
            method:'POST',
            dataType:'JSON',
            processData:false,
            contentType:false,
            mimeType:'multipart/formdata',
            crossDomain:false,
            data:formotp,
            success:function(res){
               if(res.status=='true'){
                  //localStorage.setItem('currentUser', JSON.stringify(res));
                  $("#userid").val(res.user_id);
                  $("#ex1").modal('hide');
                  $('#otp').modal('show');
               }else{
                   swal('Otp Coudnt be sent');
               }
            }
           
        })

        console.log(this.userid);


        
    }

    //otp submit
    onSubmitotp(){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.submitted = true;
        
        // // stop here if form is invalid
         if (this.otpform.invalid) {
        //     //console.log(this.loginForm.controls);
             return;
         }

         this.loading = true;
         
         
        var formotp = new FormData();
        
        console.log(this.userdetails);
        var userid = $("#userid").val();
        formotp.append('id', userid);
        formotp.append('otp', this.otpform.value.otp);
        

        $.ajax({
            url:'https://dev.basiyn.com/verify-otp.php',
            method:'POST',
            dataType:'JSON',
            processData:false,
            contentType:false,
            mimeType:'multipart/formdata',
            crossDomain:false,
            data:formotp,
            success:function(res){
               if(res.result=='true'){
                  // alert('hi yes');
                  //console.log(this.currentUser.user_id);
                   $("#otp").modal('hide');
                    $('#newpass').modal('show')
               }else{
                console.log(this.currentUser.user_id);
                   swal('Please enter correct OTP');
               }
            }
           
        })
        
    }


    //pass and conf pass

    onSubmitnewpass(){
        this.submitted = true;
      
        // // stop here if form is invalid
         if (this.Passfields.invalid) {
        //     //console.log(this.loginForm.controls);
             return;
         }
         if(this.Passfields.value.password !=this.Passfields.value.confpassword){
            swal('Both Fields shld match')
         }
         else if( this.Passfields.value.password ==this.Passfields.value.confpassword){
           // alert('both fields matched')
            this.loading = true;
            console.log(this.userdetails);
            var userid = $("#userid").val();
           var formotp = new FormData();
           formotp.append('id',userid);
           formotp.append('password', this.Passfields.value.password);

           $.ajax({
            url:'https://dev.basiyn.com/reset-password.php',
            method:'POST',
            dataType:'JSON',
            processData:false,
            contentType:false,
            mimeType:'multipart/formdata',
            crossDomain:false,
            data:formotp,
            success:function(res){
               if(res.result=='true'){
                   swal('Password Updated successfully');
                  $('#newpass').modal('hide')
               }else{
                swal('Password coudnt be updated');
               }
            }
           
        })

         }
        
    }
 }